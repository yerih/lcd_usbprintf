/***************************************************************************
Archivo: ILI9341_STM32_Driver.c
Autor  : Yerih Iturriago
Descrip: libreria para control de las LCD.
****************************************************************************/


/* Includes ------------------------------------------------------------------*/
#include "ILI9341_STM32_Driver.h"

uint8_t g_FSMCpinMode; //1 input, 0 output

/* Global Variables ------------------------------------------------------------------*/
extern st_fontDef_t st_fontDefault;
extern st_LCD_setup st_LCD;


/***********************************************
function: 	ILI9341_init
input:		N/A
output:		N/A
descr:		inicia modulo.
************************************************/
void ILI9341_init(void)
{
   ILI9341_sendCommand(ILI9341_RESET); // software reset comand
   HAL_Delay(100);
   ILI9341_sendCommand (ILI9341_DISPLAY_OFF); // display off
   //------------power control------------------------------
   ILI9341_sendCommand (ILI9341_POWER1); // power control
   ILI9341_sendData   (0x26); // GVDD = 4.75v
   ILI9341_sendCommand (ILI9341_POWER2); // power control
   ILI9341_sendData   (0x11); // AVDD=VCIx2, VGH=VCIx7, VGL=-VCIx3
   //--------------VCOM-------------------------------------
   ILI9341_sendCommand (ILI9341_VCOM1); // vcom control
   ILI9341_sendData   (0x35); // Set the VCOMH voltage (0x35 = 4.025v)
   ILI9341_sendData   (0x3e); // Set the VCOML voltage (0x3E = -0.950v)
   ILI9341_sendCommand (ILI9341_VCOM2); // vcom control
   ILI9341_sendData   (0xbe); // 0x94 (0xBE = nVM: 1, VCOMH: VMH�2, VCOML: VML�2)
	 
   //------------memory access control------------------------
   ILI9341_sendCommand (ILI9341_MAC); // memory access control
	 //ILI9341_sendData   (0x48); // 0048 my,mx,mv,ml,BGR,mh,0.0 (mirrors)
   #if Dspl_Rotation_0_degr
   ILI9341_sendData(0x48); //0????????
   #elif Dspl_Rotation_90_degr
   ILI9341_sendData(0x28); //90????????
   #elif Dspl_Rotation_180_degr
   ILI9341_sendData(0x88); //180????????
   #elif Dspl_Rotation_270_degr
   ILI9341_sendData(0xE8); //270????????
   #endif	   
	 
   ILI9341_sendCommand (ILI9341_PIXEL_FORMAT); // pixel format set
   ILI9341_sendData   (0x55); // 16bit /pixel
	
	//Fram_Rate_119Hz	
   ILI9341_sendCommand(ILI9341_FRC);
   ILI9341_sendData(0);
	 
   //-------------ddram ----------------------------
   ILI9341_setRotation(SCREEN_HORIZONTAL_1);
	 
   ILI9341_sendCommand (ILI9341_TEARING_OFF); // tearing effect off
   //ILI9341_sendCommand(ILI9341_TEARING_ON); // tearing effect on
   //ILI9341_sendCommand(ILI9341_DISPLAY_INVERSION); // display inversion
   //ILI9341_sendCommand(ILI9341_TEARING_ON); // tearing effect on
   //ILI9341_sendData(1);
   ILI9341_sendCommand(ILI9341_Entry_Mode_Set); // entry mode set
   // Deep Standby Mode: OFF
   // Set the output level of gate driver G1~G320: Normal display
   // Low voltage detection: Disable
   ILI9341_sendData   (0x07); 
   //-----------------display------------------------
   ILI9341_sendCommand (ILI9341_DFC); // display function control
   //Set the scan mode in non-display area
   //Determine source/VCOM output in a non-display area in the partial display mode
   ILI9341_sendData   (0x0a);
   //Select whether the liquid crystal type is normally white type or normally black type
   //Sets the direction of scan by the gate driver in the range determined by SCN and NL
   //Select the shift direction of outputs from the source driver
   //Sets the gate driver pin arrangement in combination with the GS bit to select the optimal scan mode for the module
   //Specify the scan cycle interval of gate driver in non-display area when PTG to select interval scan
   ILI9341_sendData   (0x82);
   // Sets the number of lines to drive the LCD at an interval of 8 lines
   ILI9341_sendData   (0x27); 
   ILI9341_sendData   (0x00); // clock divisor
	 
   ILI9341_sendCommand (ILI9341_SLEEP_OUT); // sleep out
   HAL_Delay(100);
   ILI9341_sendCommand (ILI9341_DISPLAY_ON); // display on
   HAL_Delay(100);
   ILI9341_sendCommand (ILI9341_GRAM); // memory write
   HAL_Delay(5);
}


/***********************************************
function: 	ILI9341_sendCommand
input:		mode: cmd:  comando.
output:		N/A
descr:		envia comando a la LCD.
************************************************/
void ILI9341_sendCommand(uint16_t cmd)
{
 	HAL_GPIO_WritePin(LCD_RS_PORT, LCD_RS_PIN, GPIO_PIN_RESET);	
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_RD_PORT, LCD_RD_PIN, GPIO_PIN_SET);
	*(__IO uint16_t *)(0x60000000) = cmd;
	HAL_GPIO_WritePin(LCD_WR_PORT, LCD_WR_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_WR_PORT, LCD_WR_PIN, GPIO_PIN_SET);
}

/***********************************************
function: 	ILI9341_sendData
input:		mode: data:  data de cmd.
output:		N/A
descr:		envia data a la LCD.
************************************************/
void ILI9341_sendData(uint16_t data) 
{ 
	HAL_GPIO_WritePin(LCD_RS_PORT, LCD_RS_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_RD_PORT, LCD_RD_PIN, GPIO_PIN_SET);
	*(__IO uint16_t *)(0x60080000) = data;	
	HAL_GPIO_WritePin(LCD_WR_PORT, LCD_WR_PIN, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_WR_PORT, LCD_WR_PIN, GPIO_PIN_SET);
}


/***********************************************
function: 	ILI9341_DrawPixel
input:		x1, y1: coordenadas de columna.
			x2, y2: coordenadas de pagina.
output:		N/A
descr:		setea el cursor en la memoria.
************************************************/
void ILI9341_SetCursorPosition(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
  ILI9341_sendCommand(ILI9341_COLUMN_ADDR);	
  ILI9341_sendData(x1>>8);
  ILI9341_sendData(x1 & 0xFF);	
  ILI9341_sendData(x2>>8);	
  ILI9341_sendData(x2 & 0xFF);	
				
  ILI9341_sendCommand(ILI9341_PAGE_ADDR);		
  ILI9341_sendData(y1>>8);	
  ILI9341_sendData(y1 & 0xFF);		
  ILI9341_sendData(y2>>8);
  ILI9341_sendData(y2 & 0xFF);		
  ILI9341_sendCommand(ILI9341_GRAM);
}

/***********************************************
function: 	ILI9341_DrawPixel
input:		x, y: coordenadas de pixel.
			color: del pixel.
output:		N/A
descr:		pinta un pixel en la LCD.
************************************************/
void ILI9341_DrawPixel(uint16_t x, uint16_t y, uint16_t color)
{
	ILI9341_SetCursorPosition(x, y, x, y); 
	ILI9341_sendData(color);
}

void ILI9341_SPI_Init(void)
{
	MX_GPIO_Init();												//GPIO INIT
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);	//CS OFF
}

/*Send data (char) to LCD*/
void ILI9341_SPI_Send(unsigned char SPI_Data)
{
	//HAL_SPI_Transmit(HSPI_INSTANCE, &SPI_Data, 1, 1);
}


/***********************************************
function: GPIO_FSMC_ChangePinMode
input:		mode: TRUE/1:  si es input
								FALSE/0: si es output
output:		N/A
descr:		cambia el modo entrada o salida para
					el puerto FSMC data D0-D15.
************************************************/
void ILI9341_GPIO_ChangePinMode(unsigned char mode)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Pines que se alteraran Puerto E INPUT/OUTPUT
  PE7   ------> FSMC_D4
  PE8   ------> FSMC_D5
  PE9   ------> FSMC_D6
  PE10  ------> FSMC_D7
  PE11  ------> FSMC_D8
  PE12  ------> FSMC_D9
  PE13  ------> FSMC_D10
  PE14  ------> FSMC_D11
  PE15  ------> FSMC_D12
	*/
	GPIO_InitStruct.Pin = GPIO_PIN_7|
								
	GPIO_PIN_8|
												GPIO_PIN_9|
												GPIO_PIN_10|
												GPIO_PIN_11|
												GPIO_PIN_12|
												GPIO_PIN_13|
												GPIO_PIN_14|
												GPIO_PIN_15;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;
	
	if(mode)
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT/*GPIO_MODE_AF_PP*/;
	else
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP/*GPIO_MODE_AF_PP*/;

	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);																	//Puerto E
	
	/* Pines que se alteraran Puerto D INPUT/OUTPUT
	PD0   ------> FSMC_D2
	PD1   ------> FSMC_D3
	PD8   ------> FSMC_D13
  PD9   ------> FSMC_D14
  PD10  ------> FSMC_D15
	PD14  ------> FSMC_D0
  PD15  ------> FSMC_D1*/
  GPIO_InitStruct.Pin = GPIO_PIN_0|		
												GPIO_PIN_1|
												GPIO_PIN_8|
												GPIO_PIN_9|
												GPIO_PIN_10|
												GPIO_PIN_14|
												GPIO_PIN_15;
												
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;
	if(mode)
	{
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT/*GPIO_MODE_AF_PP*/;
		g_FSMCpinMode = 1;
	}
	else
	{
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP/*GPIO_MODE_AF_PP*/;
		g_FSMCpinMode = 0;
	}
	
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);															//Puerto D
}




/***********************************************
function: ILI9341_GPIO_verifyPinMode
input:		N/A
output:		TRUE/1:  si es input
					FALSE/0: si es output
descr:		verifica el modo entrada o salida del
					puerto FSMC data D0-D15.
************************************************/
uint8_t ILI9341_GPIO_verifyPinMode(void)
{
	return g_FSMCpinMode;
}



/***********************************************
function: ILI9341_FSMC_readConvertData
input:		buff: donde se almacenara la 
					info de [D15-D0].

					len:	tmno de la info.

output:		N/A
descr:		lee el puerto FSMC [D15-D0].
************************************************/
void ILI9341_FSMC_readConvertData(uint16_t *buff, uint16_t len)
{
	uint16_t aux = 0;
	
	if(!ILI9341_GPIO_verifyPinMode())
	{
		ILI9341_GPIO_ChangePinMode(INPUT_MODE);
	}
	
	for(int j = 0; j <= len; ++j)
	{
			buff[j] = 0;

			for(int i = 15; i >= 0; --i)
			{
				switch(i)
				{
					case 15:
								aux = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_10);
								buff[j] |= aux << 15;
								break;
					case 14:
								aux = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_9);
								buff[j] |= aux << 14;
								break;
					case 13:
								aux = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_8);
								buff[j] |= aux << 13;					
								break;
					case 12:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_15);
								buff[j] |= aux << 12;
								break;
					case 11:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_14);
								buff[j] |= aux << 11;
								break;
					case 10:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_13);
								buff[j] |= aux << 10;
								break;
					case 9:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_12);
								buff[j] |= aux << 9;
								break;
					case 8:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_11);
								buff[j] |= aux << 8;
								break;
					case 7:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_10);
								buff[j] |= aux << 7;
								break;
					case 6:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_9);
								buff[j] |= aux << 6;
								break;
					case 5:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_8);
								buff[j] |= aux << 5;
								break;
					case 4:
								aux = HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_7);
								buff[j] |= aux << 4;
								break;
					case 3:
								aux = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_1);
								buff[j] |= aux << 3;
								break;
					case 2:
								aux = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_0);
								buff[j] |= aux << 2;
								break;
					case 1:
								aux = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_15);
								buff[j] |= aux << 1;
								break;
					case 0:
								aux = HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14);
								buff[j] |= aux;
								break;
				}
			}
	}
	/* Pines que se alteraran Puerto D INPUT/OUTPUT
	FSMC_D15 -----> PD10
	FSMC_D14 -----> PD9 
	FSMC_D13 -----> PD8 
	FSMC_D12 -----> PE15  
	FSMC_D11 -----> PE14 
	FSMC_D10 -----> PE13 
	FSMC_D9 ------> PE12  
	FSMC_D8 ------> PE11 
	FSMC_D7 ------> PE10
	FSMC_D6 ------> PE9  
	FSMC_D5 ------> PE8 
	FSMC_D4 ------> PE7 
	FSMC_D3 ------> PD1  
	FSMC_D2 ------> PD0
	FSMC_D1 ------> PD15	
	FSMC_D0 ------> PD14*/
  
	
	
}

/***********************************************
function: ILI9341_FSMC_convertData
input:		value: valor para [D15-D0]
output:		N/A
descr:		escribe en el puerto FSMC D15-D0
					el valor de entrada si el puerto esta
					en modo output.
************************************************/
void ILI9341_FSMC_writeConvertData(uint16_t value)
{
	uint16_t buf = value;
	if(ILI9341_GPIO_verifyPinMode())
	{
		ILI9341_GPIO_ChangePinMode(OUTPUT_MODE);
	}
	
	for(int i = 0; i < 16; ++i)
	{
		buf = value;
		buf >>= i;
		buf &= 1;
		
		switch(i)
		{
			case 0:
						HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, (GPIO_PinState)buf);
						break;
			case 1:
						HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, (GPIO_PinState)buf);
						break;
			case 2:
						HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, (GPIO_PinState)buf);
						break;
			case 3:
						HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, (GPIO_PinState)buf);
						break;
			case 4:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7, (GPIO_PinState)buf);
						break;
			case 5:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, (GPIO_PinState)buf);
						break;
			case 6:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, (GPIO_PinState)buf);
						break;
			case 7:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, (GPIO_PinState)buf);
						break;
			case 8:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, (GPIO_PinState)buf);
						break;
			case 9:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, (GPIO_PinState)buf);
						break;
			case 10:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, (GPIO_PinState)buf);
						break;
			case 11:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_14, (GPIO_PinState)buf);
						break;
			case 12:
						HAL_GPIO_WritePin(GPIOE, GPIO_PIN_15, (GPIO_PinState)buf);
						break;
			case 13:
						HAL_GPIO_WritePin(GPIOD, GPIO_PIN_8, (GPIO_PinState)buf);
						break;
			case 14:
						HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, (GPIO_PinState)buf);
						break;
			case 15:
						HAL_GPIO_WritePin(GPIOD, GPIO_PIN_10, (GPIO_PinState)buf);
						break;
		}
	}		
}


/***********************************************
function: 	ILI9341_WriteMemContinue
input:		x, y: coordenadas.
			size: tmno o final del 
				  bloque de memoria a escribir.
			color: color del pixel.
output:		N/A
descr:		pinta un pixel.
************************************************/
void ILI9341_WriteMemContinue(uint16_t x, uint16_t y, uint32_t size, uint16_t color)
{
	ILI9341_SetCursorPosition(x, y, LCD_WIDTH-1, LCD_HEIGHT-1); 
	ILI9341_sendCommand(WRMEMC); //Write memory continue
	while (size)
	{
		size--;
        ILI9341_sendData(color);
	}
}

/***********************************************
function: 	ILI9341_printImage
input:		x, y: coordenadas.
			size: tmno de la imagen
			img : apuntador hacia el bmp.
output:		N/A
descr:		pinta una imagen en la pantalla.
************************************************/
void ILI9341_printImage(uint16_t x, uint16_t y, uint32_t size, uint16_t* img)
{
	uint32_t i;
	
	ILI9341_SetCursorPosition(x, y, LCD_HEIGHT-1, LCD_WIDTH-1); 
	ILI9341_sendCommand(WRMEMC);
	for(i = 0; i < size; ++i)
	{
		ILI9341_sendData(img[i]);
	}
	ILI9341_sendCommand(NOP);
}

/***********************************************
function: 	ILI9341_fillScreen
input:		Colour: del pixel
output:		N/A
descr:		pinta toda la pantalla de un color.
************************************************/
void ILI9341_fillScreen(uint16_t Colour)
{
	ILI9341_WriteMemContinue(0, 0, (uint32_t)LCD_SIZE_HeightxWidth, Colour);
}



/**************************************************
function: 	st_LCD_init
input	:	N/A
output	:	N/A
descr	:	escribe un caracter en la pantalla.
***************************************************/
void st_LCD_init(void)
{
	LCD_clr();
	fonts_init();
	st_LCD.x = 2;
	st_LCD.y = 2;
	st_LCD.background = WHITE;
	st_LCD.foreground = BLACK;
	st_LCD.fontBackground_flag = 0;
	st_LCD.font = font_7x10;
}

/********************************************************
function: 	lcd_printf
input	:	fmt: formato de caracteres.
			...: lista de variables.
output	:	N/A
descr	:	printf para LCD. La funcion usa la letra
			que contenga la estructura st_LCD.
			44 caracteres en total, si font = font_7x10
*********************************************************/
void lcd_printf(char *fmt, ...)
{
	char buff[200];
	memset(buff, 0, sizeof(buff));
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buff, fmt, ap);
	va_end(ap);
	ILI9341_puts(buff);
}



/**************************************************
function: 	LCD_clr
input	:	N/A
output	:	N/A
descr	:	borra pantalla.
***************************************************/
void LCD_clr(void)
{
	ILI9341_fillScreen(WHITE);
}

/**************************************************
function: 	ILI9341_Putc
input:		c: caracter a imprimir.
output:		N/A
descr:		escribe un caracter en la pantalla.
***************************************************/
void ILI9341_putc(char c)
{
	uint32_t i;
	uint32_t j;
	uint32_t b;
	
	if ((st_LCD.x + st_LCD.font.fontWidth) > LCD_WIDTH - 1) 
	{
		st_LCD.y += st_LCD.font.fontHeight;
		st_LCD.x = 2;
	}
	
	if((st_LCD.y + st_LCD.font.fontHeight) > LCD_HEIGHT - 1)
	{
		st_LCD.y = 2;
		st_LCD.x = 2;
	}
	
	for (i = 0; i < st_LCD.font.fontHeight; i++) 
	{
		b = st_LCD.font.data[(c - 32) * st_LCD.font.fontHeight + i];
		for (j = 0; j < st_LCD.font.fontWidth; j++)
		{
			if ((b << j) & 0x8000) 
			{
				ILI9341_DrawPixel(st_LCD.x + j, (st_LCD.y + i), st_LCD.foreground);
			} 
			else 
			{
				if(st_LCD.fontBackground_flag)
					ILI9341_DrawPixel(st_LCD.x + j, (st_LCD.y + i), st_LCD.background);
			}
		}
	}
	st_LCD.x += st_LCD.font.fontWidth;
}





void ILI9341_puts(char *str)
{	
	while (*str) 
	{
		if (*str == '\n')
		{
			st_LCD.y += st_LCD.font.fontHeight + 1;
			st_LCD.x = 2;
			str++;
			continue;
		} 
		else if (*str == '\r')
		{
			st_LCD.x = 2;
			str++;
			continue;
		}
		ILI9341_putc(*(str++));
	}
}







/*HARDWARE RESET*/
void ILI9341_Reset(void)
{
	HAL_GPIO_WritePin(LCD_CS_PORT, LCD_CS_PIN, GPIO_PIN_RESET);
	HAL_Delay(200);
}

/**************************************************
function: 	ILI9341_Set_Rotation
input:		Rotation: usar enum SCREEN_ROTATION.
			
output:		N/A
descr:		Establece sentido de la pantalla.
***************************************************/
void ILI9341_setRotation(uint8_t Rotation)
{
	ILI9341_sendCommand(0x36);
	switch(Rotation) 
	{
			case SCREEN_VERTICAL_1:
				ILI9341_sendData(0x40|0x08);
				st_LCD.width  = LCD_HEIGHT;
				st_LCD.height = LCD_WIDTH;
				break;
			case SCREEN_HORIZONTAL_1:
				ILI9341_sendData(0x20|0x08);
				st_LCD.width  = LCD_WIDTH;
				st_LCD.height = LCD_HEIGHT;
				break;
			case SCREEN_VERTICAL_2:
				ILI9341_sendData(0x80|0x08);
				st_LCD.width  = LCD_HEIGHT;
				st_LCD.height = LCD_WIDTH;
				break;
			case SCREEN_HORIZONTAL_2:
				ILI9341_sendData(0x40|0x80|0x20|0x08);
				st_LCD.width  = LCD_WIDTH;
				st_LCD.height = LCD_HEIGHT;
				break;
			default:
				break;
	}
}

/*Enable LCD display*/
void ILI9341_Enable(void)
{
HAL_GPIO_WritePin(LCD_RST_PORT, LCD_RST_PIN, GPIO_PIN_SET);
}


void ILI9341_Draw_Rectangle(uint16_t X, uint16_t Y, uint16_t Width, uint16_t Height, uint16_t Colour)
{
	if((X >=LCD_WIDTH) || (Y >=LCD_HEIGHT)) return;	
	if((X+Width-1)>=LCD_WIDTH)
	{
		Width=LCD_WIDTH-X;
	}
	if((Y+Height-1)>=LCD_HEIGHT)
	{
		Height=LCD_HEIGHT-Y;
	}
	ILI9341_SetCursorPosition(X, Y, X+Width-1, Y+Height-1);
//(Colour, Height*Width);
}

//DRAW LINE FROM X,Y LOCATION to X+Width,Y LOCATION
void ILI9341_Draw_Horizontal_Line(uint16_t X, uint16_t Y, uint16_t Width, uint16_t Colour)
{
	if((X >=LCD_WIDTH) || (Y >=LCD_HEIGHT)) return;	
	if((X+Width-1)>=LCD_WIDTH)
	{
		Width=LCD_WIDTH-X;
	}
	ILI9341_SetCursorPosition(X, Y, X+Width-1, Y);
//ILI9341_Draw_Colour_Burst(Colour, Width);
}

//DRAW LINE FROM X,Y LOCATION to X,Y+Height LOCATION
void ILI9341_Draw_Vertical_Line(uint16_t X, uint16_t Y, uint16_t Height, uint16_t Colour)
{
	if((X >=LCD_WIDTH) || (Y >=LCD_HEIGHT)) return;
	if((Y+Height-1)>=LCD_HEIGHT)
	{
		Height=LCD_HEIGHT-Y;
	}
	ILI9341_SetCursorPosition(X, Y, X, Y+Height-1);
}

