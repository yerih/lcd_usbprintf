/***************************************************************************
Archivo: ILI9341_STM32_Driver.h
Autor  : Yerih Iturriago
Descrip: libreria driver para controlador de LCD ILI9341.
****************************************************************************/


#ifndef ILI9341_STM32_DRIVER_H_
#define ILI9341_STM32_DRIVER_H_

#include "global.h"
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "spi.h"
#include "gpio.h"
#include "fsmc.h"
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "fonts.h"
#include "colors.h"
#include "utils.h"


#define ILI9341_SCREEN_HEIGHT 	240
#define LCD_HEIGHT 				240  
#define ILI9341_SCREEN_WIDTH 	320
#define LCD_WIDTH 				320

//SPI INSTANCE
#define HSPI_INSTANCE			&hspi2

//ILI9341 definitions
#ifndef COMMAND_LIST_ILI9343
#define COMMAND_LIST_ILI9343
//Commands
#define ILI9341_RESET					0x0001
#define ILI9341_SLEEP_OUT				0x0011
#define ILI9341_GAMMA					0x0026
#define ILI9341_DISPLAY_OFF				0x0028
#define ILI9341_DISPLAY_ON				0x0029
#define ILI9341_COLUMN_ADDR				0x002A
#define ILI9341_PAGE_ADDR				0x002B
#define ILI9341_GRAM					0x002C
#define ILI9341_TEARING_OFF				0x0034
#define ILI9341_TEARING_ON				0x0035
#define ILI9341_DISPLAY_INVERSION		0x00b4
#define ILI9341_MAC			        	0x0036
#define ILI9341_PIXEL_FORMAT    		0x003A
#define ILI9341_WDB			    		0x0051
#define ILI9341_WCD				    	0x0053
#define ILI9341_RGB_INTERFACE   		0x00B0
#define ILI9341_FRC						0x00B1
#define ILI9341_FRC_PM					0x00B3
#define ILI9341_BPC						0x00B5
#define ILI9341_DFC				 		0x00B6
#define ILI9341_Entry_Mode_Set			0x00B7
#define ILI9341_POWER1					0x00C0
#define ILI9341_POWER2					0x00C1
#define ILI9341_VCOM1					0x00C5
#define ILI9341_VCOM2					0x00C7
#define ILI9341_POWERA					0x00CB
#define ILI9341_POWERB					0x00CF
#define ILI9341_PGAMMA					0x00E0
#define ILI9341_NGAMMA					0x00E1
#define ILI9341_DTCA					0x00E8
#define ILI9341_DTCB					0x00EA
#define ILI9341_POWER_SEQ				0x00ED
#define ILI9341_3GAMMA_EN				0x00F2
#define ILI9341_INTERFACE				0x00F6
#define ILI9341_PRC				   	  	0x00F7
#define ILI9341_VERTICAL_SCROLL 		0x0033 

#endif

#define LCD_SIZE_HeightxWidth 			76800
#define INPUT_MODE						 1	
#define OUTPUT_MODE						 0

// COMMAND LIST
#define	NOTCMD							0xff	//Not cmd
#define NOP								0x00	//No operation
#define SWRESET							0x01	//Software reset
#define RDDIDIF							0x04	//Read display identification information
#define RDDST							0x09	//Read display status
#define RDDPM							0x0A	//Read display Power mode
#define RDDMADCTL						0x0B	//Read display Memory access control
#define RDDPIXF							0x0C	//Read display Pixel format
#define RDDIM							0x0D	//Read display Image mode
#define RDDSM							0x0E	//Read display signal format
#define RDDSDR							0x0F	//Read display Self diagnostic resultformat
#define SPLIN							0x10	//Sleep mode enter
#define SLPOUT							0x11	//Sleep mode out
#define PTLON							0x12	//Partial mode on
#define NORON							0x13	//normal display mode ON
#define DINVOFF							0x20	//Display inversion OFF
#define DINVON							0x21	//Display inversion ON
#define GAMSET							0x26	//Gamma set
#define DISPOFF							0x28	//Display OFF
#define DISPON							0x29	//display ON
#define CASET							0x2A	//Column Address set
#define PASET							0x2B	//Sleep mode out
#define RAMWR							0x2C	//Memory Write	
#define RGBSET							0x2D	//Color Set
#define RAMRD							0x2E	//Memory Read
#define PLTAR							0x2E	//Partial area
#define VSCRDEF							0x33	//Vertical scrolling definition
#define TEOFF							0x34	//Tearing effect line OFF
#define TEON							0x35	//Tearing effect line ON
#define MADCTL							0x36	//Memory access control

#define COLMOD							0x3A	//Pixel format set
#define WRMEMC							0x3C	//Write memory continue
#define RDMEMC							0x3E	//Read memory continue
#define WRDISBV							0x51	//Write display brightness
#define RDDISBV							0x52	//Read display brightness
#define WRCTRLD							0x53	//Write control display
#define RDCTRLD							0x54	//Write control display
//falta Rdcabc
#define WCABC							0x5E	//Write CABC Minimum Brightness
#define RCABC							0x5F	//Write CABC Minimum Brightness
#define BKCTRL1							0xB8	//Read display status
//faltan mas
#define RDID1							0xDA	//Read manufacturer ID
#define RDID2							0xDB	//Read version module/driver ID
#define RDID3							0xDC	//Read module/driver ID
//faltan





#ifndef FSMC_GPIO
#define FSMC_GPIO
  /** FSMC GPIO Configuration  
  PE7   ------> FSMC_D4
  PE8   ------> FSMC_D5
  PE9   ------> FSMC_D6
  PE10   ------> FSMC_D7
  PE11   ------> FSMC_D8
  PE12   ------> FSMC_D9
  PE13   ------> FSMC_D10
  PE14   ------> FSMC_D11
  PE15   ------> FSMC_D12
  PD8   ------> FSMC_D13
  PD9   ------> FSMC_D14
  PD10   ------> FSMC_D15
  PD13   ------> FSMC_A18
  PD14   ------> FSMC_D0
  PD15   ------> FSMC_D1
  PD0   ------> FSMC_D2
  PD1   ------> FSMC_D3
  PD4   ------> FSMC_NOE
  PD5   ------> FSMC_NWE
  PD7   ------> FSMC_NE1
	*/
	
/* Definiciones FSMC */
#define FSMC_D0						GPIO_PIN_14
#define FSMC_D1						GPIO_PIN_15
#define FSMC_D2						GPIO_PIN_0
#define FSMC_D3						GPIO_PIN_1
#define FSMC_D4						GPIO_PIN_7
#define FSMC_D5						GPIO_PIN_8
#define FSMC_D6						GPIO_PIN_9
#define FSMC_D7						GPIO_PIN_10
#define FSMC_D8						GPIO_PIN_11
#define FSMC_D9						GPIO_PIN_12
#define FSMC_D10					GPIO_PIN_13
#define FSMC_D11					GPIO_PIN_14
#define FSMC_D12					GPIO_PIN_15
#define FSMC_D13					GPIO_PIN_8
#define FSMC_D14					GPIO_PIN_9
#define FSMC_D15					GPIO_PIN_10
#define FSMC_A18					GPIO_PIN_13
#define FSMC_NOE					GPIO_PIN_4
#define FSMC_NWE					GPIO_PIN_5
#define FSMC_NE1					GPIO_PIN_7

/* definiciones LCD */
#define	LCD_RST_PORT			GPIOA
#define	RST_PIN						GPIO_PIN_7
#define	LCD_RST_PIN				RST_PIN

#define LCD_RD_PORT				GPIOD
#define RD_PIN						FSMC_NOE
#define LCD_RD_PIN				FSMC_NOE

#define LCD_CS_PORT				GPIOD
#define CS_PIN						FSMC_NE1
#define CS_Pin						FSMC_NE1
#define LCD_CS_PIN				FSMC_NE1

#define LCD_RW_PORT				GPIOD
#define LCD_WR_PORT				GPIOD
#define RW_PIN						FSMC_NWE
#define LCD_RW_PIN				FSMC_NWE
#define WR_PIN						FSMC_NWE
#define LCD_WR_PIN				FSMC_NWE

#define LCD_RS_PORT				GPIOD
#define RS_PIN						FSMC_A18
#define RS_Pin						FSMC_A18
#define LCD_RS_PIN				FSMC_A18

#endif

/**SPI2 GPIO Configuration    
PB12     ------> SPI2_NSS
PB13     ------> SPI2_SCK
PB14     ------> SPI2_MISO
PB15     ------> SPI2_MOSI 
*/
#define SPI2_NSS					GPIO_PIN_12
#define SPI2_SCK					GPIO_PIN_13
#define SPI2_MISO					GPIO_PIN_14
#define SPI2_MOSI 				GPIO_PIN_15

enum SCREEN_ROTATION
{
	SCREEN_VERTICAL_1 = 0,
	SCREEN_HORIZONTAL_1,	
	SCREEN_VERTICAL_2,		
	SCREEN_HORIZONTAL_2			
};

/*---------------------------------------------------------------- Structs ------------------------------------------*/





/***********************************************
function: 	ILI9341_init
input:		N/A
output:		N/A
descr:		inicia modulo.
************************************************/
void ILI9341_init(void);
	
/***********************************************
function: 	ILI9341SendCommand
input:		mode: cmd:  comando.
output:		N/A
descr:		envia comando a la LCD.
************************************************/
void ILI9341_sendCommand(uint16_t cmd);

/***********************************************
function: 	ILI9341SendData
input:		mode: data:  data de cmd.
output:		N/A
descr:		envia data a la LCD.
************************************************/
void ILI9341_sendData(uint16_t data); 

/***********************************************
function: 	ILI9341_DrawPixel
input:		x, y: coordenadas.
			color: color del pixel.
output:		N/A
descr:		pinta un pixel.
************************************************/
void ILI9341_DrawPixel(uint16_t x, uint16_t y, uint16_t color);

/***********************************************
function: 	ILI9341_WriteMemContinue
input:		x, y: coordenadas.
			size: tmno o final del 
				  bloque de memoria a escribir.
			color: color del pixel.
output:		N/A
descr:		pinta un pixel.
************************************************/
void ILI9341_WriteMemContinue(uint16_t x, uint16_t y, uint32_t size, uint16_t color);

/***********************************************
function: 	ILI9341_fillScreen
input:		Colour: del pixel
output:		N/A
descr:		pinta toda la pantalla de un color.
************************************************/
void ILI9341_fillScreen(uint16_t Colour);

/***********************************************
function: 	ILI9341_printImage
input:		x, y: coordenadas.
			size: tmno de la imagen
			img : apuntador hacia el bmp.
output:		N/A
descr:		pinta una imagen en la pantalla.
************************************************/
void ILI9341_printImage(uint16_t x, uint16_t y, uint32_t size, uint16_t* img);

/**************************************************
function: 	st_LCD_init
input	:	N/A
output	:	N/A
descr	:	escribe un caracter en la pantalla.
***************************************************/
void st_LCD_init(void);

/**************************************************
function: 	LCD_clr
input	:	N/A
output	:	N/A
descr	:	borra pantalla.
***************************************************/
void LCD_clr(void);

/**************************************************
function: 	ILI9341_Putc
input:		c: caracter a imprimir.
output:		N/A
descr:		escribe un caracter en la pantalla.
***************************************************/
void ILI9341_putc(char c);

/**************************************************
function: 	ILI9341_puts
input:		*str  : string.
output:		N/A
descr:		escribe un string en la pantalla.
***************************************************/
void ILI9341_puts(char *str);

/**************************************************
function: 	ILI9341_Set_Rotation
input:		Rotation: usar enum SCREEN_ROTATION.
			
output:		N/A
descr:		Establece sentido de la pantalla.
***************************************************/
void ILI9341_setRotation(uint8_t Rotation);


/**************************************************
function: 	lcd_printf
input	:	fmt: formato de caracteres.
			...: lista de variables.
output	:	N/A
descr	:	printf para LCD. La funcion usa la letra
			que contenga la estructura st_LCD.
***************************************************/
void lcd_printf(char *fmt, ...);


void ILI9341_SPI_Init(void);
void ILI9341_SPI_Send(unsigned char SPI_Data);
void ILI9341_GPIO_ChangePinMode(unsigned char mode);
uint8_t ILI9341_GPIO_verifyPinMode(void);
void ILI9341_FSMC_readConvertData(uint16_t *buff, uint16_t len);
void ILI9341_FSMC_writeConvertData(uint16_t value);
void ILI9341_Reset(void);
void ILI9341_Enable(void);
void ILI9341_Draw_Rectangle(uint16_t X, uint16_t Y, uint16_t Width, uint16_t Height, uint16_t Colour);
void ILI9341_Draw_Horizontal_Line(uint16_t X, uint16_t Y, uint16_t Width, uint16_t Colour);
void ILI9341_Draw_Vertical_Line(uint16_t X, uint16_t Y, uint16_t Height, uint16_t Colour);
	
#endif

