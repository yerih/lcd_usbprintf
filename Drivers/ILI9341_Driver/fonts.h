 /**
 * Fonts library
 */

#ifndef FONTS_H_
#define FONTS_H_

#include <stdint.h>
#include "colors.h"


/**************************************************
typedef: 	struct st_fontDef_t
campos:		fontWidth : ancho de fuente.
			fontHeight: ancho de fuente.
			data	  : apuntador al arreglo de
			la fuente.
			
descr:		se utiliza para controlar las fuentes.
aplicacion:	Si se desea cambiar el tipo de fuente.
***************************************************/
typedef struct
{
	uint8_t fontWidth;
	uint8_t fontHeight;
	const uint16_t *data;
}st_fontDef_t;

/**************************************************
function: 	fonts_init
input:		N/A
output:		N/A
descr:		inicializa fuentes.
***************************************************/
void fonts_init(void);


#endif

