/***************************************************************************
Archivo: SD_OS.h
Autor  : Yerih Iturriago
Descrip: libreria de sistema operativo para control de SD.
****************************************************************************/




#ifndef SD_OS_H_
#define SD_OS_H_


//#include "global.h"
#include "ILI9341_STM32_Driver.h"
#include "fatfs.h"
#include "stm32f4xx_hal_sd.h"
#include "audioPlay.h"


//--------------------------------------------------------------------- Typedef, def, enum ---------------------------//



//---------------------------------------------------------------------------- Globales ------------------------------//

extern uint8_t g_showDebug;
extern uint8_t g_SDmntState; //SD mount State. 1: montaje de SD. 0: desmontaje.


/******************************************************************
function: 	SD_mount
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_mount(void);

/******************************************************************
function: 	SD_mount
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_unmount(void);

/******************************************************************
function: 	SD_openFile
input	:	mode: WRITE: crea el archivo y lo edita.
				  READ : lee el archivo.
			
output	:	error: si hubo algun error retorna 1.
descr	:	Abre un archivo de la SD segun el modo. Se debe 
			realizar montaje de SD con SD_mount. Se debe cerrar el 
			archivo luego de usarlo.
*******************************************************************/
uint8_t SD_openFile(char* fileName, uint8_t mode);


/******************************************************************
function: 	SD_closeFile
input	:	N/A
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Cierra el archivo de la SD.
*******************************************************************/
uint8_t SD_closeFile(FIL *SDFile);

/***********************************************************************
function: 	SD_writeFile
input	:	fileName: nombre del archivo con su extension.
			buffer  : direccion donde se almacenará la data leida.
			len		: tmno del buffer o de la data. 
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD en modo FA_READ o FA_OPEN_EXISTING.
************************************************************************/
uint8_t SD_writeFile(char* fileName, void* buffer, uint32_t len);

/***********************************************************************
function: 	SD_readFile
input	:	fileName: nombre del archivo con su extension.
			buffer  : direccion donde se almacenará la data leida.
			len		: tmno del buffer o de la data. 
			*lseek  : apuntador hacia el cursor u offset de R/W del 
					  archivo.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD en modo FA_READ o FA_OPEN_EXISTING 
			y lo cierra.
************************************************************************/
uint8_t SD_readFile(char* fileName, void* buffer, uint32_t len, uint32_t* lseek);


/***********************************************************************
function: 	SD_lseek
input	:	fileName    : nombre del archivo con su extension.
			lseek       : cursor u offset de R/W del archivo.
			mountAndOpen: 1: realiza montaje y apertura de archivo.
						  0: no realiza montaje ni apertura.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	establece el lseek o cursor de R/W del archivo.
************************************************************************/
uint8_t SD_lseek(char* fileName, uint32_t lseek, uint8_t mountAndOpen);


/***********************************************************************
function: 	SD_read
input	:	fileName    : nombre del archivo con su extension.
			len			: longitud o tmno en bytes de la lectura.
			*lseek      : apuntador hacia el cursor u offset de R/W del archivo.
			mountAndOpen: 1: realiza montaje y apertura de archivo.
						  0: no realiza montaje ni apertura.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
************************************************************************/
uint8_t SD_OS_read(char* fileName, void* buffer, uint32_t len, uint32_t* lseek);



/***********************************************************************
function: 	SD_OS_openFilePad
input	:	pad: apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Recibe la direccion de un pad y abre su respectivo archivo.
************************************************************************/
uint8_t SD_OS_openFilePad(st_PAD *pad, uint8_t mode);


/***********************************************************************
function: 	SD_OS_readFilePad
input	:	pad: apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Recibe la direccion de un pad y abre su respectivo archivo.
************************************************************************/
uint8_t SD_OS_readFilePad(st_PAD *pad, uint32_t len);




/***********************************************************************
function: 	SD_OS_closeFilePad
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	cierra el archivo del pad respectivo.
************************************************************************/
uint8_t SD_OS_closeFilePad(st_PAD *pad);


/***********************************************************************
function: 	SD_OS_changeDir
input	:	path : direccion en string del directorio.
output	:	si hubo algun error retorna >0. De lo contrario 0.
descr	:	cambia de directorio actual.
************************************************************************/
uint8_t SD_OS_changeDir(const TCHAR* path);


/***********************************************************************
function: 	SD_OS_currentDir
input	:	N/A
output	:	string con el nombre del directorio actual.
descr	:	devuelve string con el nombre del directorio actual.
************************************************************************/
char* SD_OS_currentDir(void);




/****************************************************************************
function: 	SD_OS_openDir
input	:	path: ruta del directorio. Se debe tomar en cuenta el directorio
				  raiz.
output	:	string con el nombre del directorio actual.
descr	:	devuelve string con el nombre del directorio actual.
*****************************************************************************/
uint8_t SD_OS_openDir(const TCHAR* path);


/****************************************************************************
function: 	SD_OS_padLseekReset
input	:	pad: direccion de memoria del pad.
output	:	N/A
descr	:	resetea el lseek del archivo del pad al byte 44.
*****************************************************************************/
void SD_OS_padLseekReset(st_PAD* pad);


#endif

