/***************************************************************************
Archivo: audioplay.h
Autor  : Yerih Iturriago
Descrip: libreria para control de sonido.
****************************************************************************/


#include "stm32f4xx_hal.h" 
#include <stdio.h>
#include <string.h>
#include <stdint.h> 
#include <stdarg.h>
#include "audioplay_config.h"
#include "SD_OS.h"
#include "global_def.h"

//#define ONLY_SNARE
//#define WITH_KICK
//#define WITH_HITHAT

#ifndef AUDIOPLAY_H_
#define AUDIOPLAY_H_


//#include "stm32f4xx_hal.h" 
//#include <stdio.h>
//#include <string.h>
//#include <stdint.h> 
//#include <stdarg.h>
//#include "audioplay_config.h"
//#include "SD_OS.h"
//#include "global_def.h"


/*-------------------------------------------------------  Definiciones ------------------------------------------------*/

//------------------------------ Audio



enum 
{
	WAV_FORMAT = 0,
	MP3_FORMAT,
	AAC_FORMAT,
	WMA_FORMAT,
};


enum fmtNameByIntensity
{
	FMT_HYPHEN_NUMBER = 0,
	FMT_ONLY_NUMBER,
};


/* Estructura de estado de audio */   
typedef enum num_Audio_state
{
	AUDIO_INACTV = 0,

} AUDIO_State;



typedef struct st_WAVE_Format
{
	uint32_t ChunkID; 		/* 0 */
	uint32_t FileSize; 		/* 4 */
	uint32_t FileFormat; 	/* 8 */
	uint32_t SubChunk1ID; 	/* 12 */
	uint32_t SubChunk1Size; /* 16 */  
	uint16_t AudioFormat; 	/* 20 */
	uint16_t NbrChannels; 	/* 22 */  
	uint32_t SampleRate; 	/* 24 */
	uint32_t ByteRate; 		/* 28 */
	uint16_t BlockAlign; 	/* 32 */  
	uint16_t BitPerSample; 	/* 34 */  
	uint32_t SubChunk2ID; 	/* 36 */  
	uint32_t SubChunk2Size; /* 40 */
	
} WAVE_format_typedef;

typedef struct st_AUDIO_FILE
{
	uint8_t 				fileID;
	uint8_t 				fileFormat;
	char 	   			    path[50];
	char 	   			    pathDir[50];
	uint32_t	   			duration;
	uint32_t			 	bytesRead;
	uint32_t				lseek;
	WAVE_format_typedef     fileHeaderInfo;
	
} AUDIO_file_st_typedef;

typedef struct st_AUDIO_PAD
{
	AUDIO_file_st_typedef	file;
	DMA_I2S_FORMATBIT     	*buffer;
	uint8_t 			  	intensidad;
	uint8_t				  	actv;
	float				  	vol;
	st_SD_typedef		  	SD;
	uint8_t					tcnt;
	uint8_t					hit;
} st_PAD;

/*----------------------------------------------------------------- Globales -----------------------------------------*/

extern DMA_I2S_FORMATBIT DMA_buffer_M0[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT DMA_buffer_M1[DMA_I2S_BUFFER_SIZE];


//Buffer para pads
extern DMA_I2S_FORMATBIT audioBuffer_MIX[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT audioBuffer_SNARE[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT audioBuffer_KICK[DMA_I2S_BUFFER_SIZE];
#ifndef ONLY_SNARE
extern DMA_I2S_FORMATBIT audioBuffer_KICK[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT audioBuffer_HITHAT[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT audioBuffer_TOM1[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT audioBuffer_TOM2[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT audioBuffer_TOM3[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT audioBuffer_CYMBAL_L[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT audioBuffer_CYMBAL_R[DMA_I2S_BUFFER_SIZE];
#endif

//----------------------------------------------- PADs
extern st_PAD pad_snare;

#ifdef WITH_KICK
extern st_PAD pad_kick;
#endif

#ifdef WITH_HITHAT
extern st_PAD pad_hithat;
#endif

#ifndef ONLY_SNARE
extern st_PAD pad_kick;
extern st_PAD pad_hithat;
extern st_PAD pad_tom1;
extern st_PAD pad_tom2;
extern st_PAD pad_tom3;
extern st_PAD pad_cymbalL;
extern st_PAD pad_cymbalR;
#endif

//----------------------------------------------- ARCHIVO/FILES
//extern AUDIO_file_st_typedef audioFile;



/*----------------------------------------------------------------- Funciones -----------------------------------------*/
/******************************************************************
function: 	audioPlay_printFileInfo
input	:	audioFile:	contiene la informacion del header del 
						archivo.
output	:	TRUE : 		Error.
			FALSE: 		Success.
descr	:	Abre el archivo dependiendo del tipo de formato.
			Lee su encabezado y lo guarda en la variable
			tipo estructura AUDIO_file_st_typedef.
*******************************************************************/
void audioPlay_printFileInfo(AUDIO_file_st_typedef *audioFile);



/******************************************************************
function: 	AudioPlay_GetFileInfoFromSD
input	:	audioFile: variable del tipo estructura que contiene la
			info del archivo.
output	:	TRUE : Error.
			FALSE: Success.
descr	:	Abre el archivo dependiendo del tipo de formato.
			Lee su encabezado y lo guarda en la variable
			tipo estructura AUDIO_file_st_typedef.
			Antes de invocar esta funcion. Debe haberse seteado 
			el campo "pad" de la estructura.
*******************************************************************/
uint8_t audioPlay_GetFileInfoFromSD(AUDIO_file_st_typedef *audioFile);


/******************************************************************
function: 	AudioPlay_GetFileInfoFromSD
input	:	audioFile: variable del tipo estructura que contiene la
			info del archivo.
output	:	TRUE : Error.
			FALSE: Success.
descr	:	Abre el archivo dependiendo del tipo de formato.
			Lee su encabezado y lo guarda en la variable
			tipo estructura AUDIO_file_st_typedef.
			Antes de invocar esta funcion. Debe haberse seteado 
			el campo "pad" de la estructura.
*******************************************************************/
uint8_t audioPlay_loadPadBuff(void);

/******************************************************************
function: 	audioPlay_initPAD
input	:	N/A
output	:	TRUE : Error.
			FALSE: Success.
descr	:	inicializa estructura de los pads.
*******************************************************************/
uint8_t audioPlay_initPAD(uint8_t showInfoFile);


/******************************************************************
function: 	audioPlay_loadMainBuff
input	:	N/A
output	:	>0: Error. Verificar este valor con enum_PAD y se 
			obtendra el pad que genero error.
			FALSE: Success.
descr	:	carga data nueva en los buffer principales M0 y M1.
			Si M0 esta en uso, carga M1. Aplica tambien para el 
			caso reciproco.
*******************************************************************/
uint8_t audioPlay_loadMainBuff(void);


/******************************************************************
function: 	audioPlay_mix
input	:	N/A
output	:	>0: Error. Verificar este valor con enum_PAD y se 
			obtendra el pad que genero error.
			FALSE: Success.
descr	:	realiza la mezcla de muestras.
*******************************************************************/
uint8_t audioPlay_mix(void);




/******************************************************************
function: 	audioPlay_padCurrentTime
input	:	*pad = pad del que se desea obtener info.
output	:	tiempo (seg) reproducido.
descr	:	divide el lseek por el bit rate.
*******************************************************************/
float audioPlay_padCurrentTime(st_PAD *pad);



/******************************************************************
function: 	audioPlay_setVolumePads
input	:	N/A
output	:	N/A
descr	:	setea el volumen de cada pad.
*******************************************************************/
void audioPlay_defaultVolumePads(void);


/******************************************************************
function: 	audioPlay_openPadFile
input	:	N/A
output	:	error: retorna < 0 (negativo) si el problema es con la 
				   SD.
				   retorna el numero correspondiente a la etiqueta 
				   enum_PAD.
descr	:	abre los archivos respectivos de cada PAD.
*******************************************************************/
uint8_t audioPlay_openPadsFile(void);


/******************************************************************
function: 	audioPlay_actvPads
input	:	N/A
output	:	N/A
descr	:	activa todos los pads.
*******************************************************************/
void audioPlay_actvPads(void);



/******************************************************************
function: 	audioPlay_clnBuff
input	:	N/A
output	:	N/A
descr	:	activa todos los pads.
*******************************************************************/
void audioPlay_clnBuff(void);




/******************************************************************
function: 	audioPlay_fadeOut
input	:	 a: direccion del sample para dar inicio del fade out.
output	:	N/A.
descr	:	aplica fade out a las ultimas 44 samples.
*******************************************************************/
void audioPlay_fadeOut(DMA_I2S_FORMATBIT *a, DMA_I2S_FORMATBIT len);



/******************************************************************
function: 	audioPlay_memset0_stpad
input	:	N/A
output	:	N/A
descr	:	hace un memset en 0 a la estructura de los pads.
******************************************************************/
void audioPlay_memset0_stpad(void);




/******************************************************************
function: 	audioPlay_verifyPadHit
input	:	padLabel: etiqueta correspondiente al pad a verificar.
output	:	si fue golpeado ese pad.
descr	:	segun verifica si el pad ha sido golpeado.
******************************************************************/
uint8_t audioPlay_verifyPadHit(uint8_t padLabel);


/***********************************************************************************
function: 	audioPlay_setDirAndFile
input	:	N/A
output	:	N/A
descr	:	setea el directorio y el archivo segun el formato.
************************************************************************************/
void audioPlay_setDirAndFile(void);



/***********************************************************************************
function: 	audioPlay_setDirPad
input	:	padLabel: etiqueta correspondiente al pad.
			dirMode:  si es el directorio /default o el configurado por el usuario.
output	:	N/A
descr	:	guarda la ruta del directorio en la estructura st_PAD segun el modo
			de directorio. Si esta configurado para usar directorio /default o el
			configurado por el usuario.
************************************************************************************/
void audioPlay_setDirPad(uint8_t padLabel, uint8_t dirMode);





/****************************************************************************
function: 	audioPlay_setFileByIntensity
input	:	padLabel: etiqueta correspondiente al pad.
			fmtName:  para los distintos formatos de nombre del archivo. 
			Ver enum fmtNameByIntensity.
output	:	N/A
descr	:	guarda en el campo pathDir
*****************************************************************************/
void audioPlay_setFileByIntensity(uint8_t padLabel, uint8_t fmtName);




/************************************************************************************
function: 	audioPlay_setFmtName
input	:	fmt:		formato del nombre del archivo. Ver enum fmtNameByIntensity.
			dst:		direccion de memoria donde se guardara la ruta.
			intensity:  intensidad del hit. Ver st_PAD.
output	:	N/A
descr	:	setea el nombre del archivo segun el formato.
**************************************************************************************/
void audioPlay_setFmtName(uint8_t fmt, char* dst, uint8_t intensity);



/**************************************************************************************
function: 	audioPlay_openPadFileByHit
input	:	N/A
output	:	N/A
descr	:	verifica si el campo hit de un pad esta en TRUE. si es asi, setea el 
			directorio segun la intensidad del golpe y abre el archivo correspondiente,
			obtiene los metadatos y se coloca el lseek = 44. Setea en cero el campo hit
***************************************************************************************/
void audioPlay_openPadFileByHit(void);




/***************************************************************************************
function: 	audioPlay_openPadFiles
input	:	padLabel: etiqueta correspondiente al pad.
output	:	retorna error segun enum FRESULT.
descr	:	cierra y abre el archivo segun la ruta del pad (seteada antes con setDirPad
			y setFileByIntensity). Obtiene los metadata y sete el lseek = 44;
****************************************************************************************/
uint8_t audioPlay_openPadFiles(uint8_t padLabel);



#endif //AUDIOPLAY_H_


