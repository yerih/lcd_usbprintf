/***************************************************************************
Archivo: buttons_OS.h
Autor  : Yerih Iturriago
Descrip: libreria de sistema operativo para buttons_OS.
****************************************************************************/


#ifndef BUTTONS_OS_H_
#define BUTTONS_OS_H_

#include "global.h"
#include "gpio.h"

enum enum_PIN
{
	PIN_0 = 0,
	PIN_1,
	PIN_2,
	PIN_3,
	PIN_4,
	PIN_5,
	PIN_6,
	PIN_7,
	PIN_8,
	PIN_9,
	PIN_10,
	PIN_11,
	PIN_12,
	PIN_13,
	PIN_14,
	PIN_15,
};

typedef enum 
{
	//GPIOA
	PA0 = 1,
	PA1,
	PA2,
	PA3,
	PA4,
	PA5,
	PA6,
	PA7,
	PA8,
	PA9,
	PA10,
	PA11,
	PA12,
	PA13,
	PA14,
	PA15,
	
	//GPIOB
	PB0,
	PB1,
	PB2,
	PB3,
	PB4,
	PB5,
	PB6,
	PB7,
	PB8,
	PB9,
	PB10,
	PB11,
	PB12,
	PB13,
	PB14,
	PB15,
	
	//GPIOC
	PC0,
	PC1,
	PC2,
	PC3,
	PC4,
	PC5,
	PC6,
	PC7,
	PC8,
	PC9,
	PC10,
	PC11,
	PC12,
	PC13,
	PC14,
	PC15,
	
	//GPIOD
	PD0,
	PD1,
	PD2,
	PD3,
	PD4,
	PD5,
	PD6,
	PD7,
	PD8,
	PD9,
	PD10,
	PD11,
	PD12,
	PD13,
	PD14,
	PD15,
	
	//GPIOE
	PE0,
	PE1,
	PE2,
	PE3,
	PE4,
	PE5,
	PE6,
	PE7,
	PE8,
	PE9,
	PE10,
	PE11,
	PE12,
	PE13,
	PE14,
	PE15,
}enum_BUTTONS;


//------------------------------------------------------- directivas de definicion
#define		K0		PE3
#define		K1		PE4
//colocar las restantes aqui.

#define LIMITE_BOTONES		12

//------------------------------------------------------- Variables
extern uint8_t availButtons[80];
extern uint8_t createdButtons[LIMITE_BOTONES];			//guarda la etiqueta del boton creado.
extern uint8_t pressedButtons[LIMITE_BOTONES];			//guarda la etiqueta del boton presionado.
extern uint8_t iBut;									//indice del arreglo de botones presionados pressedButtons[].


//------------------------------------------------------- Prototipos de Funciones 


/*****************************************************************
function: 	buttons_initAvailButtons
input:		N/A
output:		N/A
descr:		inicializa el arreglo availButtons con la informacion
			correspondiente.
*******************************************************************/
void buttons_initAvailButtons(void);


/*****************************************************************
function: 	buttons_OS_labelToStr
input:		label: etiqueta del pin segun enum_BUTTONS
output:		el string de la etiqueta.
descr:		retorna el string de la etiqueta.
*******************************************************************/
char* buttons_OS_labelToStr(enum_BUTTONS label);




/*****************************************************************
function: 	buttons_OS_labelToPort
input:		label:	etiqueta segun enum_BUTTONS.
output:		numero de pin correspondiente segun la etiqueta.
descr:		Traduce la etiqueta de pines (PA0, PE13, PC4, etc)
			y devuelve el nro. de pin correspondiente.
*******************************************************************/
uint8_t buttons_OS_labelToNpin(enum_BUTTONS label);



/*****************************************************************
function: 	buttons_OS_labelToPort
input:		label:	etiqueta segun enum_BUTTONS.
output:		puerto correspondiente a la etiqueta.
descr:		Traduce la etiqueta de pines (PA0, PE13, PC4, etc)
			y devuelve la direccion del puerto correspondiente.
*******************************************************************/
GPIO_TypeDef* buttons_OS_labelToPort(uint16_t label);




/*****************************************************************
function: 	buttons_OS_labelToPort
input:		label:	etiqueta segun enum_BUTTONS.
output:		numero de pin correspondiente segun la etiqueta.
descr:		Traduce la etiqueta de pines (PA0, PE13, PC4, etc)
			y devuelve el nro. de pin correspondiente.
			retorna 0xff si es una entrada incorrecta.
*******************************************************************/
uint16_t buttons_OS_labelToPin(enum_BUTTONS label);





/*****************************************************************
function: 	buttons_OS_isAvail
input:		pin:      nro de pin (1, 2, 3, 4, 5, 6, 7...14, 15).
			GPIO:	  el puerto respectivo.
output:		TRUE: ocupado. FALSE: disponible.
descr:		Verifica si un pin esta disponible.
*******************************************************************/
uint8_t buttons_OS_isAvail(uint8_t pin, GPIO_TypeDef* GPIOx);




/*****************************************************************
function: 	buttons_OS_createButton
input:		pin:      segun la etiqueta de la tarjeta. Se utiliza
					  enum_BUTTONS. Ejemplo: PA0
			pullMode: GPIO_PULLDOWN, GPIO_NOPULL, etc.
output:		TRUE: si se creo el boton. FALSE: caso contrario.
descr:		Crea botones.
			Verifica si el pin esta ocupado. Si lo esta, retorna 0.
			Si esta vacio, crea el boton.
*******************************************************************/
uint8_t buttons_OS_createButton(enum_BUTTONS pin, uint8_t pullMode);





/*****************************************************************
function: 	buttons_OS_scanAvailButtons	
input:		a[]: arreglo q indica los pines que estan diponibles y 
				 cuales no.
descr:		escanea los pines y guarda en un arreglo cuales estan 
			disponibles u ocupados.
			Disponibles = 1; Ocupados = 0;
*******************************************************************/
void buttons_OS_scanAvailButtons(uint8_t* a);




/*****************************************************************
function: 	buttons_OS_printAvailButtons	
input:		a[]: arreglo q indica los pines que estan diponibles y 
				 cuales no.
descr:		imprime el arreglo de pines disponibles y ocupados.
*******************************************************************/
void buttons_OS_printAvailButtons(uint8_t *a);



/*****************************************************************
function: 	buttons_OS_isPress	
input:		N/A
output:		N/A
descr:		TRUE: tecla presionada. FALSE: nada se presiono.
			Version 1: valida solo para botones K0 y K1.
*******************************************************************/
uint8_t buttons_OS_isPress(void);



/*****************************************************************
function: 	buttons_OS_getKey	
input:		N/A
output:		Boton presionado.
descr:		retorna el valor de la etiqueta del boton presionado.
*******************************************************************/
uint8_t buttons_OS_getKey(void);



/*******************************************************************
function: 	buttons_OS_verifyIOpin
input:		label
output:		
			GPIO_MODE_INPUT      = Input Floating Mode.
			GPIO_MODE_OUTPUT_PP  = Output Push Pull Mode.
			GPIO_MODE_OUTPUT_OD  = Output Open Drain Mode.
			GPIO_MODE_AF_PP      = Alter. Function Push Pull Mode.
			GPIO_MODE_AF_OD      = No se retorna.
descr:		retorna el modo en que esta configurado el pin.
*********************************************************************/
uint8_t buttons_OS_verifyIOpin(enum_BUTTONS label);



/*******************************************************************
function: 	buttons_OS_strIOpin
input:		label
output:		INPUT      = Input Floating Mode.
			OUTPUT_PP  = Output Push Pull Mode.
			OUTPUT_OD  = Output Open Drain Mode.
			AF_PP      = Alter. Function Push Pull Mode.
descr:		retorna el modo en que esta configurado el pin en forma 
			de str.
*********************************************************************/
char* buttons_OS_strIOpin(enum_BUTTONS label);




/*******************************************************************
function: 	buttons_OS_pinToLabel
input:		pin:   nro. de pin
			GPIOx: puerto correspondiente al pin.
output:		etiqueta correspondiente al pin segun enum_BUTTONS.
descr:		retorna etiqueta correspondiente al pin.
*********************************************************************/
enum_BUTTONS buttons_OS_pinToLabel(uint8_t pin, GPIO_TypeDef* GPIOx);






/*******************************************************************
function: 	buttons_OS_labelAvailPin
input:		label:   etiqueta segun enum_BUTTONS.
output:		TRUE: ocupado. FALSE: disponible.
descr:		verifica disponibilidad del pin.
*********************************************************************/
uint8_t buttons_OS_labelAvailPin(enum_BUTTONS label);




/*******************************************************************
function: 	buttons_OS_updateCreatedButtons
input:		label: etiqueta del pin donde esta el boton.
output:		TRUE: sin espacio para botones. FALSE: exito.
descr:		guarda en createdButtons[] la etiqueta del pin creado.
*********************************************************************/
uint8_t buttons_OS_updateCreatedButtons(enum_BUTTONS label);




/*******************************************************************
function: 	buttons_OS_updateCreatedButtons
input:		label: etiqueta del pin donde esta el boton.
output:		TRUE: ocupado. FALSE: disponible.
descr:		pregunta si un boton esta creado.
*********************************************************************/
uint8_t buttons_OS_scanCreatedButtons(enum_BUTTONS label);




/*******************************************************************
function: 	buttons_OS_clearBuffButtons
input:		N/A.
output:		N/A.
descr:		limpia el buffer de botones.
*********************************************************************/
void buttons_OS_clearBuffButtons(void);


/*******************************************************************
function: 	buttons_OS_readKey
input:		N/A.
output:		etiqueta del boton oprimido
descr:		permanece en un loop hasta que se presione un boton
			y devuelve la etiqueta del boton oprimido. Limpia el
			buffer.
*********************************************************************/
uint8_t buttons_OS_readKey(void);



/*******************************************************************
function: 	buttons_OS_readKey
input:		N/A.
output:		etiqueta del boton oprimido
descr:		permanece en un loop hasta que no se presione un boton
			y devuelve la etiqueta del boton oprimido. Limpia el
			buffer.
*********************************************************************/
uint8_t buttons_OS_keyPress(void);


/*******************************************************************
function: 	buttons_isPress
input:		N/A.
output:		etiqueta del boton oprimido
descr:		devuelve la etiqueta del boton oprimido.
*********************************************************************/
uint8_t buttons_isPress(void);



/*******************************************************************
function: 	buttons_OS_loopPress
input:		label: etiqueta del boton que se esta presionando.
output:		N/A.
descr:		permanece en un loop hasta que no se presione el boton
*********************************************************************/
void buttons_OS_loopPress(enum_BUTTONS label);




/*******************************************************************
function: 	buttons_OS_readButt
input:		call: funcion a ser ejecutada si se presiona la tecla 
				  Recibe un parametro key: tecla al ser presionada
output:		N/A
descr:		Ejecuta la funcion call cuando es presionada el boton
			key. Limpia el buffer.
*********************************************************************/
void buttons_OS_readButt(void(call)(uint8_t*));




/*******************************************************************
function: 	buttons_OS_readButtTim
input:		call: funcion a ser ejecutada si se presiona la tecla 
				  Recibe un parametro key: tecla al ser presionada
output:		N/A
descr:		Ejecuta la funcion call cuando es presionada el boton
			key. Permanece en un loop hasta que no se presione un 
			boton. Limpia el buffer.
*********************************************************************/
void buttons_OS_readButtLoop(void(call)(uint8_t*));



#endif


