/******************************************************************************
  * File Name          : call.c
  * Description        : Herramientas para codificar (debugs, etc).
  * Author			   : Yerih Iturriago.
  ******************************************************************************/
  
  
  
#ifndef CALL_H_
#define CALL_H_

#include "global.h"
  
  
/*******************************************************************
function: 	call_buttonsDebug
input:		key: boton o etiqueta del boton a ser presionado para
			ejecutar esta funcion.
output:		N/A.
descr:		enciende el LED_D2 e imprime en pantalla la etiqueta del
			boton.
*********************************************************************/
void call_buttonsDebug(uint8_t* key);



/*******************************************************************
function: 	call_buttonsDebug
input:		key: boton o etiqueta del boton a ser presionado para
			ejecutar esta funcion.
output:		N/A.
descr:		Reproduce el archivo desde el principio. Resetea el lseek
*********************************************************************/
void call_playPad(uint8_t* key);
  
  
  
/*******************************************************************
function: 	call_keyIsPressed
input:		key: boton o etiqueta del boton a ser presionado para
			ejecutar esta funcion.
output:		N/A.
descr:		Reproduce el archivo desde el principio. Resetea el 
			lseek.
			Enciende LED_X dependiendo del boton pulsado.
			imprime el pad correspondiente.
*********************************************************************/
void call_keyIsPressed(uint8_t* key);  
  

/*******************************************************************
function: 	call_irqTest
input:		GPIO_Pin: pin de interrupcion.
output:		N/A.
descr:		para testeo de Interrupciones externas. Enciende LED D2,
			imprime texto en pantalla.
*********************************************************************/
void call_irqTest(uint16_t GPIO_Pin);



/*******************************************************************
function: 	call_adcGetPadIntensity
input:		GPIO_Pin: pin de interrupcion.
output:		N/A.
descr:		obtiene valor del adc correspondiente al pad.
*********************************************************************/
int call_adcGetPadIntensity(uint16_t GPIO_Pin);


/*******************************************************************
function: 	call_changeADCchannel
input:		adcChannel
output:		retorna el valor del ADC
descr:		cambia el canal del ADC segun el pad y retorna el valor
			leido.
*********************************************************************/
void call_changeADCchannel(uint32_t channel);




/***********************************************************************
function: 	call_getADCvalue
input:		value: direccion donde sera almacenado el valor leido
output:		retorna FALSE si es exitoso, de lo contrario ver HAL_RETURN
descr:		cambia el canal del ADC segun el pad y retorna el valor
			leido.
*************************************************************************/
int call_getADCvalue(uint8_t* value);




/*******************************************************************
function: 	call_timerADCmanage
input:		N/A
output:		N/A
descr:		obtiene valor del adc correspondiente al pad y verifica
			el contador de cada pad.
*********************************************************************/
void call_timerADCmanage(void);



/***********************************************************************
function: 	call_incrementPadCntHit
input:		GPIO_Pin: pin correspondiente a la interrupcion y al pad.
output:		N/A
descr:		incrementa el contador del pad segun el pin correspondiente
			y setea pad.hit en TRUE.
************************************************************************/
void call_incrementPadCntHit(uint16_t GPIO_Pin);






/*******************************************************************************
function: 	call_adcGetIntensity
input:		channel: canal del pad correspondiente.
			value:   direccion donde sera almacenado el valor del ADC.
output:		FALSE si es exitoso, de lo contrario ver HAL_RETURN.
descr:		obtiene el valor del adc segun el canal en que se encuentre el pad.
********************************************************************************/
int call_adcGetIntensity(uint32_t channel, uint8_t* value);




  
#endif



