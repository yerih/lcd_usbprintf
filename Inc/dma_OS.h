/***************************************************************************
Archivo: DMA_I2S_OS.h
Autor  : Yerih Iturriago
Descrip: libreria para control de protocolo I2S a traves del DMA.
****************************************************************************/


#ifndef _DMA_OS_H_
#define _DMA_OS_H_

#include "global.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_def.h"
#include "stm32f4xx_hal_dma.h"


/* ---------------------------------------------- Definiciones --------------------------------------------------------- */


									
/* ------------------------------------------------ Globales ----------------------------------------------------------- */

//Generales
extern st_flagGestor st_flag;

//I2S
extern uint16_t sinewave16bit[256];
extern I2S_HandleTypeDef hi2s2;


//DMA
extern DMA_HandleTypeDef hdma_memtomem_dma2_stream0;

//buffers DMA
#ifdef AUDIOPLAY_H_
extern DMA_I2S_FORMATBIT DMA_buffer_M0[DMA_I2S_BUFFER_SIZE];
extern DMA_I2S_FORMATBIT DMA_buffer_M1[DMA_I2S_BUFFER_SIZE];
#endif

/* ------------------------------------------------ Funciones ----------------------------------------------------------- */

/**
  * @brief  Set the DMA Transfer parameter.
  * @param  hdma       pointer to a DMA_HandleTypeDef structure that contains
  *                     the configuration information for the specified DMA Stream.  
  * @param  SrcAddress The source memory Buffer address
  * @param  DstAddress The destination memory Buffer address
  * @param  DataLength The length of data to be transferred from source to destination
  * @retval HAL status
  */
void DMA_MultiBufferSetConfig(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength);

/******************************************************************
function: 	DMA_OS_dmaI2S_init
input	:	N/A.
output	:	N/A.
descr	:	
			- inicializa y configura el DMA para I2S.
			- inicializa y configura el DMA para SDIO.
			- 
*******************************************************************/
void DMA_OS_dmaI2S_init(void);


/******************************************************************
function: 	DMA_OS_MultibufferCallbackM0M1
input	:	N/A.
output	:	N/A.
descr	:	
			- inicializa y configura el DMA para I2S.
			- inicializa y configura el DMA para SDIO.
			- 
*******************************************************************/
void DMA_OS_MultibufferCallbackM0M1(struct __DMA_HandleTypeDef *hdma);


/******************************************************************
function: 	TxHalfCpltCallback
input	:	hdma pointer to a __DMA_HandleTypeDef structure that 
			contains the configuration information for I2S module.
output	:	N/A.
descr	:	Tx Transfer Half completed callbacks
*******************************************************************/
void DMA_I2S_TxHalfCpltCallback(struct __DMA_HandleTypeDef *hdma);

/******************************************************************
function: 	DMA_I2S_ErrorCallback
input	:	hdma pointer to a __DMA_HandleTypeDef structure that 
			contains the configuration information for I2S module.
output	:	N/A.
descr	:	registra en el apuntador si hubo algun error.
*******************************************************************/
void DMA_I2S_ErrorCallback(DMA_HandleTypeDef *hdma);

/******************************************************************
function: 	DMA_I2S_printInfoDMAconfig
input	:	N/A.
output	:	N/A.
descr	:	Imprime en pantalla la info del DMA I2S.
*******************************************************************/
void DMA_I2S_printInfoDMAconfig(void);



/******************************************************************
function: 	DMA_I2S_clnBuff
input	:	N/A.
output	:	N/A.
descr	:	Imprime en pantalla la info del DMA I2S.
*******************************************************************/
void DMA_I2S_clnBuff(void);

/******************************************************************
function: 	call_DMA2_OS_memIncEnable
input	:	hdma: stream usado en DMA2.
output	:	N/A.
descr	:	Reestablece la configuracion del stream DMA2 en modo 
			m2m.
*******************************************************************/
void call_DMA2_OS_memIncEnable(DMA_HandleTypeDef* hdma);



/******************************************************************
function: 	DMA2_OS_memcpy
input	:	SrcAddress: memoria fuente.
			DstAddress: memoria destino.
			len:		tmno de los datos a enviar.
output	:	N/A.
descr	:	Transfiere datos de memoria a memoria con interrupcion.
*******************************************************************/
void DMA2_OS_memcpy(void* DstAddress, void* SrcAddress, uint32_t len);


/******************************************************************
function: 	DMA2_OS_memset
input	:	SrcAddress: memoria fuente.
			DstAddress: memoria destino.
			szElmnt:	tmno de cada elemento.
			sz:			tmno de los datos a enviar.
output	:	N/A.
descr	:	Transfiere datos de memoria a memoria con interrupcion.
*******************************************************************/
void DMA2_OS_memset(void* dst, uint32_t val, uint8_t szElmnt, uint32_t sz);





#endif


