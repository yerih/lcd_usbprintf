/***************************************************************************
Archivo: global.h
Autor  : Yerih Iturriago
Descrip: libreria de sistema operativo que contiene variables globales.
****************************************************************************/




#include "stm32f4xx_hal.h"
#include "rng.h"
#include "spi.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"
#include "fsmc.h"
#include "ILI9341_STM32_Driver.h"
#include "fonts.h"
#include "SD_OS.h"
#include "audioplay_config.h"
#include "audioplay.h"
#include "w25qxx.h"
#include "w25qxxConfig.h"
#include "sdio.h"
#include "fatfs.h"
#include "i2s.h"
#include "dma.h"
#include "audio_arrays.h"
#include "dma_OS.h"
#include "buttons_OS.h"
#include "call.h"
#include "timer_OS.h"
#include "adc.h"
#include "tdd.h"

#ifndef _UTILS_H_
#include "utils.h"
#endif




#ifndef _GLOBAL_H_
#define _GLOBAL_H_

/*--------------------------------------------------------------- Definiciones ------------------------------------- */

//------------------------------ Generales
#define DEBUG
#define DEFAULT				0xff



//------------------------------ flash MEM
#define HEADER_HASH			'#'
#define HEADER_VERTBAR		'|'
#define FOOTER_HASH			'#'
#define FOOTER_VERTBAR		'|'
#define MEM_VACIA			0xFFffFFff
#define TRUE				1
#define FALSE				0

//------------------------------ Audio
#define BUSY_STATE			2


/*---------------------------------- TypeDef, Enum ----------------------------------------------------------------- */

//------------------------------------------------------------------- FlashMEM W25QXX
enum
{
     BLOCK = 0,
     SECT,
     PAG,
     BYTES,
	 ADDRESS
};

//------------------------------------------------------------------- Audio
enum
{
	M0 = 0,
	M1,
};

enum enum_PAD
{
	SNARE = 1,
	KICK,
	HITHAT,
	TOM1,
	TOM2,
	TOM3,
	CYMBAL_L,
	CYMBAL_R
};

//------------------------------------------------------------------- Imagen
typedef struct st_fileImgSt
{
	uint16_t width;
	uint16_t height;
	uint32_t size;
	uint16_t bitxPixel;
	char headerFooter[2];
	char *pathImg;
	const unsigned char *imgInArray;
}st_fileImg;

//------------------------------------------------------------------- LCD
typedef struct st_LCD_setupSt
{
	uint16_t x;
	uint16_t y;
	uint16_t width;
	uint16_t height;
	st_fontDef_t font;
	uint8_t  fontBackground_flag;
	uint16_t foreground;
	uint16_t background;
} st_LCD_setup;

//------------------------------------------------------------------- FlagsGestor
typedef struct st_flagGestorSt
{
	uint8_t dma_M0M1;	//TRUE/1 : avisa que M0 esta en uso. FALSE/0 : que M1 esta en uso.
	uint8_t dma_SDRX;	//TRUE/1 : avisa que la lectura de la SD esta lista. De lo contrario, FALSE/0.
	uint8_t dma_SDTX;	//TRUE/1 : avisa que la escritura en la SD esta lista. De lo contrario, FALSE/0.
}st_flagGestor;

//------------------------------------------------------------------- SD





//------------------------------------------------------------------- Setting
enum DIRECTORY_MODE
{
	DIR_DEFAULT = 0,
	DIR_DEMO,
	DIR_USER,
};

#define DIR_PATH_DEFAULT	"/audiolib/default/"
#define DIR_PATH_ROOT		"/"


extern DIR SDDir;

/************************************ Variables Globales ***********************************************************************/

/*---------------------------------- LCD ----------------------------------------------------------------- */

/**********************************************************
					font_7x10
descripcion: fuente de tmno 7x10.

					font_11x18
descripcion: fuente de tmno 11x18.

					font_16x26
descripcion: fuente de tmno 16x26.

					st_fontDefault
descripcion: variable del tipo "st_fonDef_t" que contiene
			 el tipo de fuente LCD.
**********************************************************/
extern st_fontDef_t font_7x10;
extern st_fontDef_t font_11x18;
extern st_fontDef_t font_16x26;
extern st_fontDef_t st_fontDefault;

/**********************************************************
					st_LCD
					
descripcion: variable del tipo "st_LCD_setup" que controla
			 el cursor de la LCD, fuente, colorForeGr, 
			 colorText, ancho y alto de la LCD.
**********************************************************/
extern st_LCD_setup st_LCD;


/*---------------------------------- SD ----------------------------------------------------------------- */
/**********************************************************
					SDFatFS
descripcion: File system object for SD logical drive.

					SDPath
descripcion: SD logical drive path.

					SDFile
descripcion: File object for SD			
**********************************************************/
extern FATFS SDFatFS;    
extern char SDPath[4];   
extern FIL SDFile; 
extern UINT g_SDerrorByte;
extern uint8_t retSD;    		/* Return value for SD */
extern uint8_t g_SDmntState; //SD mount State. 1: montaje de SD. 0: desmontaje.

/*---------------------------------- Imagen ----------------------------------------------------------------- */
/**********************************************************
					st_fileImgVar
					
descripcion: variable del tipo st_fileImg para tomar
			 los datos de una imagen. Tmno, dimensiones.
**********************************************************/
extern st_fileImg st_fileImgVar;



/*---------------------------------- MEM W25QXX ------------------------------------------------------------- */
/*
					mem_Available
					
descripcion: arreglo con informacion de espacio
			 disponible en memoria.
						
mem_Available[BLOCK]  : bloques disponibles.
mem_Available[SECTOR] : sector disponibles.
mem_Available[PAG]    : pag disponibles.
mem_Available[BYTES]  : bytes disponibles.
mem_Available[ADDRESS]: address disponible. valor por 
						default 0xffFFffFF;
*/
extern uint32_t mem_Available[5];
extern uint32_t **g_filePtr;


/*----------------------------------------------  Generales  ---------------------------------------------------------*/
extern uint8_t g_showDebug;
extern uint8_t g_dmai2sStop; 		//realiza stop si es > 0; 
extern st_flagGestor st_flag;
extern uint8_t g_demoAudio;		//Para el seteo de configuraciones demo o default.

/*----------------------------------------------  Audio      ---------------------------------------------------------*/
extern uint8_t g_demoAudio;


//DMA I2S buffers
extern I2S_HandleTypeDef hi2s2;


//------------------------------------------------------------- Timers
extern uint8_t g_tcnt;


#endif





