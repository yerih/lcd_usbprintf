/***************************************************************************
Archivo: global_def.h
Autor  : Yerih Iturriago
Descrip: libreria de sistema operativo que contiene definiciones globales.
****************************************************************************/

#ifndef GLOBAL_DEF_H_
#define GLOBAL_DEF_H_

#include "fatfs.h"

typedef struct st_SD_fatfs
{
	uint8_t ret;     /* Return value for SD */
	char    path[4]; /* SD logical drive path */
	FATFS   fatFS;   /* File system object for SD logical drive */
	FIL     file;    /* File object for SD */
	
}st_SD_typedef;


#endif







