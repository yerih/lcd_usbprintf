/********************************************************************************
  * @file           : main.h
  * @brief          : Main program header
  *******************************************************************************/
  
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/
//---------------------- IRQ
#define IRQ_SNARE_Pin 			GPIO_PIN_9
#define IRQ_SNARE_GPIO_Port 	GPIOA
#define IRQ_SNARE_EXTI_IRQn 	EXTI9_5_IRQn

#define IRQ_KICK_Pin 			GPIO_PIN_8
#define IRQ_KICK_GPIO_Port 		GPIOB
#define IRQ_KICK_EXTI_IRQn 		EXTI9_5_IRQn

#define IRQ_HITHAT_Pin 			GPIO_PIN_4
#define IRQ_HITHAT_GPIO_Port 	GPIOC
#define IRQ_HITHAT_EXTI_IRQn 	EXTI4_IRQn


#define IRQ_TOM1_Pin 			GPIO_PIN_10
#define IRQ_TOM1_GPIO_Port 		GPIOB
#define IRQ_TOM1_EXTI_IRQn 		EXTI15_10_IRQn

#define IRQ_TOM2_Pin 			GPIO_PIN_0
#define IRQ_TOM2_GPIO_Port 		GPIOE
#define IRQ_TOM2_EXTI_IRQn 		EXTI0_IRQn

#define IRQ_TOM3_Pin 			GPIO_PIN_1
#define IRQ_TOM3_GPIO_Port 		GPIOE
#define IRQ_TOM3_EXTI_IRQn 		EXTI1_IRQn

#define IRQ_CYMBAL_L_Pin 		GPIO_PIN_5
#define IRQ_CYMBAL_L_GPIO_Port 	GPIOE
#define IRQ_CYMBAL_L_EXTI_IRQn 	EXTI9_5_IRQn

#define IRQ_CYMBAL_R_Pin 		GPIO_PIN_6
#define IRQ_CYMBAL_R_GPIO_Port 	GPIOE
#define IRQ_CYMBAL_R_EXTI_IRQn 	EXTI9_5_IRQn



//------------------- ADC definiciones
#define ADC_SNARE_Pin 			GPIO_PIN_2
#define ADC_SNARE_GPIO_Port 	GPIOA
#define ADC_SNARE_CHANNEL		ADC_CHANNEL_2

#define ADC_KICK_Pin 			GPIO_PIN_3
#define ADC_KICK_GPIO_Port 		GPIOA
#define ADC_KICK_CHANNEL		ADC_CHANNEL_3

#define ADC_HITHAT_Pin 			GPIO_PIN_4
#define ADC_HITHAT_GPIO_Port 	GPIOA
#define ADC_HITHAT_CHANNEL		ADC_CHANNEL_4

#define ADC_TOM1_Pin 			GPIO_PIN_5
#define ADC_TOM1_GPIO_Port 		GPIOA
#define ADC_TOM1_CHANNEL		ADC_CHANNEL_5

#define ADC_TOM2_Pin 			GPIO_PIN_6
#define ADC_TOM2_GPIO_Port 		GPIOA
#define ADC_TOM2_CHANNEL		ADC_CHANNEL_6

#define ADC_TOM3_Pin 			GPIO_PIN_7
#define ADC_TOM3_GPIO_Port 		GPIOA
#define ADC_TOM3_CHANNEL		ADC_CHANNEL_7

#define ADC_CYMBAL_L_Pin 		GPIO_PIN_5
#define ADC_CYMBAL_L_GPIO_Port 	GPIOC
#define ADC_CYMBAL_L_CHANNEL	ADC_CHANNEL_15

#define ADC_CYMBAL_R_Pin 		GPIO_PIN_0
#define ADC_CYMBAL_R_GPIO_Port 	GPIOB
#define ADC_CYMBAL_R_CHANNEL	ADC_CHANNEL_8




#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
