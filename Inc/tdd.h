/***************************************************************************
Archivo: tdd.h
Autor  : Yerih Iturriago
Descrip: funciones para pruebas unitarias de funciones.
****************************************************************************/



#ifndef TDD_H_
#define TDD_H_


#include "global.h"

/*************************************************************************
function: 	tdd_audioPlay_setDirAndFile
input	:	padLabel: etiqueta correspondiente al pad.
			demo:	  setea el tipo de configuracion. TRUE: setea
			los directorios en default. FALSE: directorios segun 
			configuracion de usuario.
output	:	N/A
descr	:	observa los directorios segun la configuracion en demo y user.
**************************************************************************/
void tdd_audioPlay_setDirAndFile(uint8_t padLabel, uint8_t demo);


/*************************************************************************
function: 	tdd_SD_testPathFile
input	:	path:	string con la ruta del archivo a testear.
output	:	N/A
descr	:	imprime en pantalla el resultado o msj de error del archivo de
			la ruta.
**************************************************************************/
void tdd_SD_testPathFile(char* path);







#endif

