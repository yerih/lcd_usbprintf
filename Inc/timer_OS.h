/******************************************************************************
  * File Name          : timer_OS.c
  * Autor			   : Yerih Iturriago.
  * Description        : operaciones con timers.
  ******************************************************************************/

#ifndef _TIMER_OS_H_
#define _TIMER_OS_H_

#include "global.h"


/*******************************************************************
function: 	HAL_TIM_PeriodElapsedCallback
input:		htim: timer vinculado.
output:		N/A.
descr:		funcion de interrupcion o callback. 
			Desabilita el timer. 
			Apaga el LED D3.
*********************************************************************/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);



/*******************************************************************
function: 	timer_OS_enableTimerMs
input:		htim: timer vinculado al pad o boton.
			ms  : milisegundos.
output:		N/A.
descr:		setea el timer indicado para el conteo de ms.
*********************************************************************/
void timer_OS_enableTimerMs(TIM_HandleTypeDef *htim, uint32_t ms);



/*******************************************************************
function: 	timer_OS_padPlayTime
input:		key : etiqueta del boton asociado.
output:		TRUE, si no es el momento de reproducir.
descr:		Verifica si el timer vinculado a la tecla termino el 
			conteo y lo vuelve a activar si es afirmativo.
*********************************************************************/
uint8_t timer_OS_verifyPlayTime(TIM_HandleTypeDef *htim); //TODO:(st_PAD* pad)




















#endif

