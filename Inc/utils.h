/******************************************************************************
  * File Name          : utils.h
  * Description        : Herramientas para codificar (debugs, etc).
  * Author			   : Yerih Iturriago.
  ******************************************************************************/

#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include "usbd_cdc_if.h"
#include "global.h"


#define LED_D2		  0
#define LED_D3		  1
#define LED_TOGGLE_D3 HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_7)
#define LED_LOW_D3    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET)
#define LED_HIGH_D3   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET)
#define LED_LOW_D2    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET)
#define LED_HIGH_D2   HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET)
#define LED_TOGGLE_D2 HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_6)
#define LED_STATE_D2  HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_6);
#define LED_STATE_D3  HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7);

//extern uint32_t mem_Available[4];


/*****************************************************************
function: 	utils_LED_init
input:		N/A
output:		N/A
descr:		Inicializa LEDs.
*******************************************************************/
void utils_LED_init(void);

/*****************************************************************
function: 	utils_USB_printf
input:		formato: string conteniendo formato (%s, %d, etc)
			(...)  : lista de variables.
output:		retorna USBD_OK = 0, USBD_BUSY = 1, USBD_FAIL = 2.
descr:		es como un printf via usb.
*******************************************************************/
uint8_t utils_USB_printf(const char *formato, ...);


/*****************************************************************
function: 	utils_LEDsequence
input:		seq: numero de secuencia.
output:		N/A
descr:		Hace una seq de blinks con el LED
*******************************************************************/
void utils_LEDsequence(uint8_t seq, uint8_t);


/**************************************************
function: 	utils_debug
input	:	fmt: formato de caracteres.
			...: lista de variables.
output	:	N/A
descr	:	hace un lcd_clr, y un lcd_printf para
			debuguear.
			No cambia el tipo de letra.
***************************************************/
void utils_debug(char *fmt, ...);


/*****************************************************************
function: 	utils_w25q16_printStruct
input	:	N/A
output	:	N/A
descr	:	imprime el valor de los campos de la estructura que
			controla la flash mem w25q16.
*****************************************************************/
void utils_w25q16_printStruct(void);


/*****************************************************************
function: 	utils_MEMdistribution
input	:	fsize : tmno de arreglo o archivo
			*array: arreglo que contiene el numero de
					estructura de bloques, sectores, pag y bytes
					a ser escritos en la flash mem.
output	:	N/A
descr	:	Calcula la distribucion de la memoria en estructuras 
			de memoria. Retorna la cantidad de cada estructuras
			en un arreglo. 
			array[0] = nro de bloques
			array[1] = nro de sectores
			array[2] = nro de paginas
			array[3] = nro de bytes
*****************************************************************/
void utils_MEMdistribution(uint32_t fsize, uint32_t *array);

/******************************************************************
function: 	utils_w25q16_saveImgArray
input	:	*st_fileImgVar: direccion de la estructura que contiene
							la informacion de la imagen.
output	:	N/A
descr	:	guarda un dato en forma de archivo. Se le agrega un
			HEADER o encabezado.
			'#' y '|': 2 bytes, para comienzo de archivo.
			'ID'	 : 3 bytes, contiene la direcci�n en memoria.
			'SIZE'	 : 3 bytes, contiene el tamno del archivo.
			DATA	 : n bytes, la informacion del archivo.
			'|' y '#': 2 bytes, para final de archivo.
*******************************************************************/
void utils_w25q16_saveImgArray(st_fileImg *st_fileImgVar);


/******************************************************************
function: 	st_memAvailable_init
input	:	N/A
output	:	N/A
descr	:	inicializa memoria
*******************************************************************/
void st_memAvailable_init(void);

/******************************************************************
function: 	utils_flashMEM_init
input	:	erase: >0 : borrar. 0 : no borra.
output	:	TRUE : hubo un error.
			FALSE: proceso exitoso.
descr	:	
*******************************************************************/
uint8_t utils_flashMEM_init(uint8_t erase);


/******************************************************************
function: 	utils_flashMEM_init
input	:	N/A
output	:	TRUE : hubo un error.
			FALSE: proceso exitoso.
descr	:	
*******************************************************************/
void utils_dumpMemory(uint8_t *buffer, uint32_t size);
	
	
/******************************************************************
function: 	utils_writeMEMnTimes
input	:	N/A
output	:	N/A
descr	:	escribe en la memoria 0x56, n veces.
*******************************************************************/
void utils_writeMEMnTimes(uint32_t n);


/******************************************************************
function: 	utils_w25q16_scanMEM
input	:	show = 1 (TRUE) muestra mensajes en pantalla
			show = 0 (FALSE) es obvio.
output	:	N/A
descr	:	escanea la memoria flash y detecta el byte disponible
			o vacio (0xff) y guarda esa direccion de memoria.
*******************************************************************/
void utils_w25q16_slowScanMEM(uint8_t show);


/******************************************************************
function: 	utils_w25q16_IDfileList
input	:	fileCount: contador de archivos. Debe ser > 0.
			id       : ID o direccion del archivo.
			size     : tmno del archivo.

output	:	N/A
descr	:	guarda en un doble apuntador (g_filePtr) un lista de 
			direcciones o ID y tmno de los archivos contenidos en 
			la flashsMEM. La primera fila del apuntador contiene
			g_filePtr[0][0] = contador de archivos encontrados.	
			g_filePtr[0][1] = peso en memoria.
*******************************************************************/
void utils_w25q16_IDfileList(uint32_t fileCount, uint32_t id, uint32_t size);

/******************************************************************
function: 	utils_w25q16_scanMEM
input	:	show = 1 (TRUE) muestra mensajes en pantalla
			show = 0 (FALSE) es obvio.
output	:	N/A
descr	:	escanea la memoria flash y detecta el byte disponible
			o vacio (0xff) y guarda esa direccion de memoria.
*******************************************************************/
void utils_w25q16_scanMEM(uint8_t show);


/******************************************************************
function: 	utils_w25q16_IDfileList
input	:	fileCount: contador de archivos. Debe ser > 0.
			id       : ID o direccion del archivo.
			size     : tmno del archivo.

output	:	N/A
descr	:	guarda en un doble apuntador (g_filePtr) un lista de 
			direcciones o ID y tmno de los archivos contenidos en 
			la flashsMEM. La primera fila del apuntador contiene
			g_filePtr[0][0] = contador de archivos encontrados.	
			g_filePtr[0][1] = peso en memoria.
*******************************************************************/
void utils_w25q16_IDfilePrint(void);


/******************************************************************
function: 	utils_w25q16_saveFileInBytes
input	:	fileCount: contador de archivos. Debe ser > 0.
			id       : ID o direccion del archivo.
			size     : tmno del archivo.

output	:	N/A
descr	:	escribe en la flashMEM el archivo de memoria sin 
			utilizar estructuras de memoria; solo bytes.
*******************************************************************/
void utils_w25q16_saveFileInBytes(st_fileImg *st_fileImgVar);


/******************************************************************
function: 	memset32
input	:	dest : direccion de destino.
			value: valor a ser seteado.
			size : tmno del paquete (bytes).

output	:	N/A
descr	:	memset de 32 bit.
*******************************************************************/
void memset32( void * dest, uint32_t value, uintptr_t size);



/******************************************************************
function: 	utils_changeByteOrder32
input	:	buffer: buffer a modificar.
			size  : tmno del paquete medido en unidad de 32bit 
					o 4bytes.
output	:	N/A
descr	:	cambia el orden de los bytes. Ejemplo: a = 0xaabbccdd
			al invocar funcion a = ddccbbaa. Valido solo para 32bit
*******************************************************************/
void utils_changeByteOrder32(uint32_t *buffer, uint32_t size);



/******************************************************************
function: 	utils_printClr
input	:	fmt: string de texto.
			va_list: lista de variables.
output	:	N/A
descr	:	imprime texto y cuando llega al final de la pantalla
			la limpia y continua escribiendo.
*******************************************************************/
void utils_printClr(char* fmt, ...);



/******************************************************************
function: 	utils_printArray
input	:	a:			 arreglo a imprimir.
			sizeElement: tmno de un elemento.
			len:		 tmno total del arreglo.
output	:	N/A
descr	:	imprime el arreglo en enteros.
*******************************************************************/
void utils_printArray(void* a, uint32_t sizeElement, uint32_t len);




/********************************************************************
function: 	utils_printArray
input	:	GPIO_Pin: pin solicitado.
output	:	string con el nombre del pad
descr	:	retorna el string con el nombre del pad asociado al pin.
*********************************************************************/
char* utils_getIRQlabel(uint16_t GPIO_Pin);



/********************************************************************
function: 	utils_printArray
input	:	GPIO_Pin: pin solicitado.
output	:	string con el nombre del pad
descr	:	retorna el puerto del pad asociado al pin.
*********************************************************************/
GPIO_TypeDef* utils_getIRQPort(uint16_t GPIO_Pin);




/******************************************************************
function: 	utils_padPointer
input	:	padLabel: etiqueta correspondiente al pad.
output	:	apuntador al pad correspodiente a la etiqueta.
descr	:	apunta al pad correspondiente a padLabel.
*******************************************************************/
st_PAD* utils_padPointer(uint8_t padLabel);




/******************************************************************
function: 	utils_hitPad
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
void utils_hitPad(uint8_t padLabel);




/******************************************************************
function: 	utils_hitPadReset
input	:	padLabel: etiqueta correspondiente al pad.
output	:	N/A
descr	:	resetea el campo hit de la estructura st_PAD.
*******************************************************************/
void utils_hitPadReset(uint8_t padLabel);


/******************************************************************
function: 	utils_fresult
input	:	frLabel: referido al enum FRESULT.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_fresult(uint8_t frLabel);


#endif
