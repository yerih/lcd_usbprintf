/***************************************************************************
Archivo: SD_OS.c
Autor  : Yerih Iturriago
Descrip: libreria de sistema operativo para control de SD.
****************************************************************************/




#include "SD_OS.h"



/******************************************************************
function: 	SD_mount
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_mount(void)
{
	if(f_mount(&SDFatFS, SDPath, 1) == FR_OK)
	{
		if(g_showDebug)
			lcd_printf("SD: Acceso concedido\n");
		g_SDmntState = 1;
		return 0;
	}
	if(g_showDebug)
			lcd_printf("SD: error de montaje\n");
	g_SDmntState = 0;
	return 1;
}


/******************************************************************
function: 	SD_mount
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
uint8_t SD_unmount(void)
{
	if(f_mount(NULL, SDPath, 1) == FR_OK)
	{
		if(g_showDebug)
			lcd_printf("SD: desmontada\n");
		g_SDmntState = 0;
		return 0;
	}
	if(g_showDebug)
			lcd_printf("SD: error de desmonte\n");
	return 1;
}


/******************************************************************
function: 	SD_openFile
input	:	mode: WRITE: crea el archivo y lo edita.
				  READ : lee el archivo.
			
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD segun el modo. Se debe 
			realizar montaje de SD con SD_mount. Se debe cerrar el 
			archivo luego de usarlo.
*******************************************************************/
uint8_t SD_openFile(char* fileName, uint8_t mode)
{
	uint8_t ret;
	if( (ret = f_open(&SDFile, fileName, mode)) != FR_OK)
	{
		if(g_showDebug)
			utils_printClr("%s, %s, line %d\n", utils_fresult(ret), __FILE__, __LINE__);
		return ret;
	}
	return 0;
}

/******************************************************************
function: 	SD_closeFile
input	:	SDFile: apuntador hacia el archivo SD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Cierra el archivo FIL de la SD.
*******************************************************************/
uint8_t SD_closeFile(FIL *SDFile)
{
	uint8_t ret = 0;
	if( (ret = f_close(SDFile)) != FR_OK)
		return ret;
	return ret;
}


/***********************************************************************
function: 	SD_writeFile
input	:	fileName: nombre del archivo con su extension.
			buffer  : direccion donde se almacenará la data leida.
			len		: tmno del buffer o de la data. 
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD en modo FA_READ o FA_OPEN_EXISTING.
************************************************************************/
uint8_t SD_writeFile(char* fileName, void* buffer, uint32_t len)
{
	uint32_t bytesRead = 0;
	if(SD_mount() == 0)
	{
		if(f_open(&SDFile,fileName, FA_CREATE_ALWAYS | FA_WRITE) == 0)
		{
			if(g_showDebug)
				lcd_printf("SD: Open success in Write mode\n");
			
			if(f_write(&SDFile, buffer, len, &bytesRead) == FR_OK)
			{
				if(g_showDebug)
					lcd_printf("SD: se escribieron %d bytes\n", bytesRead);	
			}
			else
			{
				if(g_showDebug)
					lcd_printf("SD: Error. Se escribieron %d bytes\n", bytesRead);
			}
		}
		else
		{
			lcd_printf("SD: Open failed\n");
		}
		f_close(&SDFile);
		SD_unmount();
		return 0;
	}
		
	else
	{
		SD_unmount();
		return 1;
	}
}


/***********************************************************************
function: 	SD_readFile
input	:	fileName: nombre del archivo con su extension.
			buffer  : direccion donde se almacenará la data leida.
			len		: tmno del buffer o de la data. 
			*lseek  : apuntador hacia el cursor u offset de R/W del 
					  archivo.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	Abre un archivo de la SD en modo FA_READ o FA_OPEN_EXISTING 
			y lo cierra.
************************************************************************/
uint8_t SD_readFile(char* fileName, void* buffer, uint32_t len, uint32_t* lseek)
{
	uint32_t a;
	f_lseek(&SDFile, *lseek);
	f_read(&SDFile, buffer, len, &a);
	(*lseek) += a;
	if(g_showDebug)
	{
		lcd_printf("SD: Se leyeron: %d bytes\n", a);
	}
	return 0;
}


/***********************************************************************
function: 	SD_lseek
input	:	fileName    : nombre del archivo con su extension.
			lseek       : cursor u offset de R/W del archivo.
			mountAndOpen: 1: realiza montaje y apertura de archivo.
						  0: no realiza montaje ni apertura.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	establece el lseek o cursor de R/W del archivo.
************************************************************************/
uint8_t SD_lseek(char* fileName, uint32_t lseek, uint8_t mountAndOpen)
{
	if(g_showDebug)
		utils_debug("SD_lseek debug\n");
	
	if(mountAndOpen)
	{
		if(!SD_mount())
		{
			if(SD_openFile(fileName, FA_OPEN_EXISTING))
			{
				if(g_showDebug)
					lcd_printf("SD: No se abre archivo\n");
				SD_closeFile(&SDFile);
				SD_unmount();
				return 1;
			}
		}
		else
		{
			if(g_showDebug)
				lcd_printf("SD: Acceso denegado\n");
			SD_unmount();
			return 1;
		}
	}
	
	f_lseek(&SDFile, lseek);
	return 0;
}

/***********************************************************************
function: 	SD_read
input	:	fileName    : nombre del archivo con su extension.
			len			: longitud o tmno en bytes de la lectura.
			*lseek      : apuntador hacia el cursor u offset de R/W del archivo.
			mountAndOpen: 1: realiza montaje y apertura de archivo.
						  0: no realiza montaje ni apertura.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
************************************************************************/
uint8_t SD_OS_read(char* fileName, void* buffer, uint32_t len, uint32_t* lseek)
{
	uint32_t a;
	f_lseek(&SDFile, *lseek);
	if(f_read(&SDFile, buffer, len, &a) != FR_OK)
	{
		if(g_showDebug)
			utils_debug("\nf_read fallo, line %d\n", __LINE__);
		return 1;
	}
	(*lseek) += a;
	return 0;
}


/***********************************************************************
function: 	SD_OS_openFilePad
input	:	pad : apuntador del tipo de estructura PAD.
			mode: mode de apertura. FA_READ, FA_WRITE, FA_CREATE, etc.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Recibe la direccion de un pad y abre su respectivo archivo.
			Guarda la estructura FIL (SDFile).
************************************************************************/
uint8_t SD_OS_openFilePad(st_PAD *pad, uint8_t mode)
{	
	uint8_t ret = 0;
	ret = SD_openFile((pad->file).path, mode);
	if(ret != FR_OK)
	{		
		if(g_showDebug)
			utils_debug("SD: error abriendo archivo pad, line %d\n", __LINE__);
		return ret;
	}
	(pad->SD).file  = SDFile;
	(pad->SD).fatFS = SDFatFS;
	memcpy((pad->SD).path, SDPath, sizeof(SDPath));
	return ret;
}


/***********************************************************************
function: 	SD_OS_readFilePad
input	:	pad: apuntador del tipo de estructura PAD.
			len: nro de bytes a leer. generalmente se usa sizeof.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	lee la SD sin abrir ni hacer montaje ni desmontaje.
			Recibe la direccion de un pad y abre su respectivo archivo.
			guarda la posicion del cursor del archivo lseek.
			Aplica fade out a las ultimas 44 samples del archivo.
************************************************************************/
uint8_t SD_OS_readFilePad(st_PAD *pad, uint32_t len)
{
	uint32_t a; //bytes leidos
	uint32_t r = (pad->file).fileHeaderInfo.FileSize - (pad->file).lseek;
	float t = audioPlay_padCurrentTime(pad);
	
	(r < len) ? (len = r): (0);
	f_lseek(&((pad->SD).file), pad->file.lseek);
	if(f_read(&((pad->SD).file), pad->buffer, len, &a) != FR_OK)
	{
		return 1;
	}
	pad->file.lseek += a;
	if(t >= (float)4.99)
	{
		DMA2_OS_memset(pad->buffer, 0, 1, sizeof(DMA_buffer_M0));
	}
	return 0;
}


/***********************************************************************
function: 	SD_OS_closeFilePad
input	:	pad : apuntador del tipo de estructura PAD.
output	:	si hubo algun error retorna 1. De lo contrario 0.
descr	:	cierra el archivo del pad respectivo.
************************************************************************/
uint8_t SD_OS_closeFilePad(st_PAD *pad)
{
	uint8_t ret = SD_closeFile(&(pad->SD.file));
	pad->SD.fatFS = SDFatFS;
	pad->SD.file  = SDFile;
	memcpy(pad->SD.path, SDPath, sizeof(SDPath));
	if(ret != FR_OK)
		if(g_showDebug)
			utils_debug("SD: Error cerrando archivo %d", ret);
	return ret;
}


/***********************************************************************
function: 	SD_OS_changeDir
input	:	path : direccion en string del directorio.
output	:	si hubo algun error retorna >0. De lo contrario 0.
descr	:	cambia de directorio actual.
************************************************************************/
uint8_t SD_OS_changeDir(const TCHAR* path)
{
	return f_chdir(path);
}



/***********************************************************************
function: 	SD_OS_currentDir
input	:	N/A
output	:	string con el nombre del directorio actual.
descr	:	devuelve string con el nombre del directorio actual.
************************************************************************/
char* SD_OS_currentDir(void)
{
	char* str;
	char str1[50];
	memset(str1, 0, sizeof(str1));
	f_getcwd(str1, 50);
	str = str1;
	return str;
}



/****************************************************************************
function: 	SD_OS_openDir
input	:	path: ruta del directorio. Se debe tomar en cuenta el directorio
				  raiz.
output	:	string con el nombre del directorio actual.
descr	:	devuelve string con el nombre del directorio actual.
*****************************************************************************/
uint8_t SD_OS_openDir(const TCHAR* path)
{
	return f_opendir(&SDDir, path);
}




/****************************************************************************
function: 	SD_OS_padLseekReset
input	:	pad: direccion de memoria del pad.
output	:	N/A
descr	:	resetea el lseek del archivo del pad al byte 44.
*****************************************************************************/
void SD_OS_padLseekReset(st_PAD* pad)
{
	pad->file.lseek = 44;
}














