/***************************************************************************
Archivo: audioplay.c
Autor  : Yerih Iturriago
Descrip: libreria para control de sonido.
****************************************************************************/


#include "audioplay.h"

/*------------------------------------------------------------------- Definiciones ------------------------------------*/
/* Comentar si se desean todos los pads */
//#define ONLY_SNARE
//#define WITH_KICK
//#define WITH_HITHAT

/*--------------------------------------------------------------------- Globales --------------------------------------*/
//------------------------------------------- Buffers
//Principales
DMA_I2S_FORMATBIT DMA_buffer_M0[DMA_I2S_BUFFER_SIZE];
DMA_I2S_FORMATBIT DMA_buffer_M1[DMA_I2S_BUFFER_SIZE];

//Buffer para pads
DMA_I2S_FORMATBIT audioBuffer_MIX[DMA_I2S_BUFFER_SIZE];
DMA_I2S_FORMATBIT audioBuffer_SNARE[DMA_I2S_BUFFER_SIZE];
#ifdef WITH_KICK
DMA_I2S_FORMATBIT audioBuffer_KICK[DMA_I2S_BUFFER_SIZE];
#endif
#ifdef WITH_HITHAT
DMA_I2S_FORMATBIT audioBuffer_HITHAT[DMA_I2S_BUFFER_SIZE];
#endif
#ifndef ONLY_SNARE
DMA_I2S_FORMATBIT audioBuffer_KICK[DMA_I2S_BUFFER_SIZE];
DMA_I2S_FORMATBIT audioBuffer_HITHAT[DMA_I2S_BUFFER_SIZE];
DMA_I2S_FORMATBIT audioBuffer_TOM1[DMA_I2S_BUFFER_SIZE];
DMA_I2S_FORMATBIT audioBuffer_TOM2[DMA_I2S_BUFFER_SIZE];
DMA_I2S_FORMATBIT audioBuffer_TOM3[DMA_I2S_BUFFER_SIZE];
DMA_I2S_FORMATBIT audioBuffer_CYMBAL_L[DMA_I2S_BUFFER_SIZE];
DMA_I2S_FORMATBIT audioBuffer_CYMBAL_R[DMA_I2S_BUFFER_SIZE];
#endif


//----------------------------------------------- PADs
st_PAD pad_snare;

#ifdef WITH_KICK
st_PAD pad_kick;
#endif

#ifdef WITH_HITHAT
st_PAD pad_hithat;
#endif

#ifndef ONLY_SNARE
st_PAD pad_kick;
st_PAD pad_hithat;
st_PAD pad_tom1;
st_PAD pad_tom2;
st_PAD pad_tom3;
st_PAD pad_cymbalL;
st_PAD pad_cymbalR;
#endif

//------------------------------------------- Archivo/Files


/******************************************************************
function: 	audioPlay_printFileInfo
input	:	audioFile:	contiene la informacion del header del 
						archivo.
output	:	TRUE : 		Error.
			FALSE: 		Success.
descr	:	Abre el archivo dependiendo del tipo de formato.
			Lee su encabezado y lo guarda en la variable
			tipo estructura AUDIO_file_st_typedef.
*******************************************************************/
void audioPlay_printFileInfo(AUDIO_file_st_typedef *audioFile)
{
	if(g_showDebug)
	{
		lcd_printf("     WAVE File Header\n");
		lcd_printf("ChunkID       = %d\n",        audioFile->fileHeaderInfo.ChunkID);
		lcd_printf("FileSize      = %d bytes\n",  audioFile->fileHeaderInfo.FileSize);
		lcd_printf("SampleRate    = %d hz\n",    audioFile->fileHeaderInfo.SampleRate);
		lcd_printf("BitPerSample  = %d\n",        audioFile->fileHeaderInfo.BitPerSample);
		lcd_printf("ByteRate      = %d byte/s\n", audioFile->fileHeaderInfo.ByteRate);
		lcd_printf("FileFormat    = 0x%x\n",      (audioFile->fileHeaderInfo.FileFormat));
		lcd_printf("NbrChannels   = %d\n",        audioFile->fileHeaderInfo.NbrChannels);
		lcd_printf("duration      = %d seg\n",    audioFile->duration);
		lcd_printf("SubChunk1ID   = %d\n",        audioFile->fileHeaderInfo.SubChunk1ID);
		lcd_printf("SubChunk1Size = %d\n",        audioFile->fileHeaderInfo.SubChunk1Size);
		lcd_printf("AudioFormat   = %d\n",        audioFile->fileHeaderInfo.AudioFormat);
		lcd_printf("BlockAlign    = %d\n",        audioFile->fileHeaderInfo.BlockAlign);
		lcd_printf("SubChunk2ID   = %d\n",        audioFile->fileHeaderInfo.SubChunk2ID);
		lcd_printf("SubChunk2Size = %d\n",        audioFile->fileHeaderInfo.SubChunk2Size);
		lcd_printf("\n--------------- presione tecla -------------\n"); //44 caracteres en total (sin contar el salto de linea).
		while(!buttons_OS_isPress());
	}
}


/******************************************************************
function: 	AudioPlay_GetFileInfoFromSD
input	:	audioFile: variable del tipo estructura que contiene la
			info del archivo.
output	:	TRUE : Error.
			FALSE: Success.
descr	:	Abre el archivo dependiendo del tipo de formato.
			Lee su encabezado y lo guarda en la variable
			tipo estructura AUDIO_file_st_typedef.
			Antes de invocar esta funcion. Debe haberse seteado 
			el campo "pad" de la estructura.
*******************************************************************/
uint8_t audioPlay_GetFileInfoFromSD(AUDIO_file_st_typedef *audioFile)
{
	uint8_t ret  = 0;
	uint8_t show = 0;
	
	//Para no mostrar el debug de SD_readFile
	if(g_showDebug)
	{
		g_showDebug = 0;
		show = 1;
	}
	ret = SD_openFile(audioFile->path, FA_READ);
	if(ret != FR_OK)
		return ret;
	ret = SD_readFile(audioFile->path, &(audioFile->fileHeaderInfo), WAV_HEADER_SIZE, &(audioFile->lseek));
	if(ret != FR_OK)
		return ret;
	audioFile->duration = ((audioFile->fileHeaderInfo).FileSize)/((audioFile->fileHeaderInfo).ByteRate);
	if(show)
		g_showDebug = 1;
	audioPlay_printFileInfo(audioFile);
	g_showDebug = 0;
	return ret;
}


/******************************************************************
function: 	AudioPlay_GetFileInfoFromSD
input	:	audioFile: variable del tipo estructura que contiene la
			info del archivo.
output	:	>0   : Error. El valor retornado indicara el pad que
				   genero el error.
			FALSE: Success.
descr	:	carga data nueva al buffer de cada pad ACTIVO. 
			pad activo es igual a decir "pad_name.actv = 1"
			Cuando se termina de reproducir el sonido del pad,
			este es desactivado automaticamente.
NOTA    :	debe haberse abierto los archivos correspondientes a 
			cada pad.
*******************************************************************/
uint8_t audioPlay_loadPadBuff(void)
{
	if(pad_snare.actv)
	{
		if( SD_OS_readFilePad(&pad_snare, sizeof(DMA_buffer_M0)) )
			return SNARE;
		if((pad_snare.file.lseek) >= (pad_snare.file.fileHeaderInfo.FileSize))
		{
			pad_snare.file.lseek = 44;
			pad_snare.actv = 0;
		}
	}
	
#ifdef WITH_KICK
	if(pad_kick.actv)
	{
		if( SD_OS_readFilePad(&pad_kick, sizeof(DMA_buffer_M0)) )
			return KICK;
		if((pad_kick.file.lseek) >= (pad_kick.file.fileHeaderInfo.FileSize))
		{
			pad_kick.file.lseek = 44;
			pad_kick.actv = 0;
		}
	}
#endif
#ifdef WITH_HITHAT
	if(pad_hithat.actv)
	{
		if( SD_OS_readFilePad(&pad_hithat, sizeof(DMA_buffer_M0)) )
		{
			return HITHAT;
		}
		if((pad_hithat.file.lseek) >= (pad_hithat.file.fileHeaderInfo.FileSize))
		{
			pad_hithat.file.lseek = 44;
			pad_hithat.actv = 0;
		}
	}
#endif
	
#ifndef ONLY_SNARE
	if(pad_kick.actv)
	{
		if( SD_OS_readFilePad(&pad_kick, sizeof(DMA_buffer_M0)) )
			return KICK;
		if((pad_kick.file.lseek) >= (pad_kick.file.fileHeaderInfo.FileSize))
		{
			pad_kick.file.lseek = 44;
			pad_kick.actv = 0;
		}
	}
	
	if(pad_hithat.actv)
	{
		if( SD_OS_readFilePad(&pad_hithat, sizeof(DMA_buffer_M0)) )
		{
			return HITHAT;
		}
		if((pad_hithat.file.lseek) >= (pad_hithat.file.fileHeaderInfo.FileSize))
		{
			pad_hithat.file.lseek = 44;
			pad_hithat.actv = 0;
		}
	}
	
	if(pad_tom1.actv)
	{
		if( SD_OS_readFilePad(&pad_tom1, sizeof(DMA_buffer_M0)) )
			return TOM1;
		if((pad_tom1.file.lseek) >= (pad_tom1.file.fileHeaderInfo.FileSize))
		{
			pad_tom1.file.lseek = 44;
			pad_tom1.actv = 0;
		}
	}
	
	if(pad_tom2.actv)
	{
		if( SD_OS_readFilePad(&pad_tom2, sizeof(DMA_buffer_M0)) )
			return TOM2;
		if((pad_tom2.file.lseek) >= (pad_tom2.file.fileHeaderInfo.FileSize))
		{
			pad_tom2.file.lseek = 44;
			pad_tom2.actv = 0;
		}
	}
	
	if(pad_tom3.actv)
	{
		if( SD_OS_readFilePad(&pad_tom3, sizeof(DMA_buffer_M0)) )
			return TOM3;
		if((pad_tom3.file.lseek) >= (pad_tom3.file.fileHeaderInfo.FileSize))
		{
			pad_tom3.file.lseek = 44;
			pad_tom3.actv = 0;
		}
	}
	
	if(pad_cymbalL.actv)
	{
		if( SD_OS_readFilePad(&pad_cymbalL, sizeof(DMA_buffer_M0)) )
			return CYMBAL_L;
		if((pad_cymbalL.file.lseek) >= (pad_cymbalL.file.fileHeaderInfo.FileSize))
		{
			pad_cymbalL.file.lseek = 44;
			pad_cymbalL.actv = 0;
		}
	}
	
	if(pad_cymbalR.actv)
	{
		if( SD_OS_readFilePad(&pad_cymbalR, sizeof(DMA_buffer_M0)) )
			return CYMBAL_R;
		if((pad_cymbalR.file.lseek) >= (pad_cymbalR.file.fileHeaderInfo.FileSize))
		{
			pad_cymbalR.file.lseek = 44;
			pad_cymbalR.actv = 0;
		}
	}
#endif
	return 0;
}

/******************************************************************
function: 	audioPlay_initPAD
input	:	showInfoFile : imprime la info del archivo del pad 
			seleccionado.
output	:	>0: Error. Verificar este valor con enum_PAD y se 
			obtendra el pad que genero error.
			FALSE: Success.
descr	:	inicializa estructura de los pads.
*******************************************************************/
uint8_t audioPlay_initPAD(uint8_t showInfoFile)
{
	uint8_t ret = 0;
	audioPlay_memset0_stpad();
	/* Seteo de las estructuras del archivo de cada pad */
	
	//--------------------------------------------- SNARE
	pad_snare.buffer     	  = audioBuffer_SNARE;
	pad_snare.file.fileFormat = WAV_FORMAT;
	pad_snare.file.lseek	  = 0;
	sprintf(pad_snare.file.path, "%s", "snare.wav");
	
	if(showInfoFile == SNARE)
		g_showDebug = 1;
	
	if( audioPlay_GetFileInfoFromSD(&(pad_snare.file)) )
		return SNARE;
	g_showDebug = 0;
	
	
#ifdef WITH_KICK
	//--------------------------------------------- KICK (solo con snare)
	pad_kick.buffer			  = audioBuffer_KICK;
	pad_kick.file.fileFormat  = WAV_FORMAT;
	pad_kick.file.lseek		  = 0;
	//pad_kick.file.path		  = "kick.wav";
	sprintf(pad_kick.file.path, "%s", "kick.wav");
	
	if(showInfoFile == KICK)
		g_showDebug = 1;
	
	if( audioPlay_GetFileInfoFromSD(&(pad_kick.file)) )
		return KICK;
	g_showDebug = 0;
#endif
	
#ifdef WITH_HITHAT
	//--------------------------------------------- HITHAT (solo con snare)
	pad_hithat.buffer		   = audioBuffer_HITHAT;
	pad_hithat.file.fileFormat = WAV_FORMAT;
	pad_hithat.file.lseek	   = 0;
	pad_hithat.file.path	   = "hithat.wav";
	sprintf(pad_hithat.file.path, "%s", "hithat.wav");
	
	if(showInfoFile == HITHAT)
		g_showDebug = 1;
		
	if( audioPlay_GetFileInfoFromSD(&(pad_hithat.file)) )
		return HITHAT;
	g_showDebug = 0;
#endif
	
	
#ifndef ONLY_SNARE
	
	//--------------------------------------------- KICK
	pad_kick.buffer			  = audioBuffer_KICK;
	pad_kick.file.fileFormat  = WAV_FORMAT;
	pad_kick.file.lseek		  = 0;
	sprintf(pad_kick.file.path, "%s", "kick.wav");
	
	if(showInfoFile == KICK)
		g_showDebug = 1;
	
	if( audioPlay_GetFileInfoFromSD(&(pad_kick.file)) )
		return KICK;
	g_showDebug = 0;
	
	//--------------------------------------------- HITHAT
	pad_hithat.buffer		   = audioBuffer_HITHAT;
	pad_hithat.file.fileFormat = WAV_FORMAT;
	pad_hithat.file.lseek	   = 0;
	//pad_hithat.file.path	   = "hithat.wav";
	sprintf(pad_hithat.file.path, "%s", "hithat.wav");
	
	if(showInfoFile == HITHAT)
		g_showDebug = 1;
		
	if( audioPlay_GetFileInfoFromSD(&(pad_hithat.file)) )
		return HITHAT;
	g_showDebug = 0;
	
	//--------------------------------------------- TOM1
	pad_tom1.buffer			   = audioBuffer_TOM1;
	pad_tom1.file.fileFormat   = WAV_FORMAT;
	pad_tom1.file.lseek	       = 0;
	//pad_tom1.file.path		   = "tom1.wav";
	sprintf(pad_tom1.file.path, "%s", "tom1.wav");
	
	if(showInfoFile == TOM1)
		g_showDebug = 1;
		
	if( audioPlay_GetFileInfoFromSD(&(pad_tom1.file)) )
		return TOM1;
	g_showDebug = 0;
	
	//--------------------------------------------- TOM2
	pad_tom2.buffer 		   = audioBuffer_TOM2;
	pad_tom2.file.fileFormat   = WAV_FORMAT;
	pad_tom2.file.lseek		   = 0;
	//pad_tom2.file.path		   = "tom2.wav";
	sprintf(pad_tom2.file.path, "%s", "tom2.wav");
	
	if(showInfoFile == TOM2)
		g_showDebug = 1;
		
	if( audioPlay_GetFileInfoFromSD(&(pad_tom2.file)) )
		return TOM2;
	g_showDebug = 0;
	
	//--------------------------------------------- TOM3
	pad_tom3.buffer			   = audioBuffer_TOM3;
	pad_tom3.file.fileFormat   = WAV_FORMAT;
	pad_tom3.file.lseek	 	   = 0;
	//pad_tom3.file.path		   = "tom3.wav";
	sprintf(pad_tom3.file.path, "%s", "tom3.wav");
	
	if(showInfoFile == TOM3)
		g_showDebug = 1;
		
	if( audioPlay_GetFileInfoFromSD(&(pad_tom3.file)) )
		return TOM3;
	g_showDebug = 0;
	
	//--------------------------------------------- CYMBAL_L
	pad_cymbalL.buffer 			= audioBuffer_CYMBAL_L;
	pad_cymbalL.file.fileFormat = WAV_FORMAT;
	pad_cymbalL.file.lseek 		= 0;
	//pad_cymbalL.file.path		= "cymbalL.wav";
	sprintf(pad_cymbalL.file.path, "%s", "cymbalL.wav");
	
	if(showInfoFile == CYMBAL_L)
		g_showDebug = 1;
		
	if( audioPlay_GetFileInfoFromSD(&(pad_cymbalL.file)) )
		return CYMBAL_L;
	g_showDebug = 0;
	
	//--------------------------------------------- CYMBAL_R
	pad_cymbalR.buffer 			= audioBuffer_CYMBAL_R;
	pad_cymbalR.file.fileFormat = WAV_FORMAT;
	pad_cymbalR.file.lseek 		= 0;
	//pad_cymbalR.file.path		= "cymbalR.wav";
	sprintf(pad_cymbalR.file.path, "%s", "cymbalR.wav");
	
	if(showInfoFile == CYMBAL_R)
		g_showDebug = 1;
		
	if( audioPlay_GetFileInfoFromSD(&(pad_cymbalR.file)) )
		return CYMBAL_R;
	g_showDebug = 0;
#endif
	
	ret = audioPlay_openPadsFile(); 		//Abre los respectivos archivos de cada pad.
	if(ret)	
		return ret;
	
	//audioPlay_actvPads();					//Activa cada pad.
	audioPlay_defaultVolumePads();			//setea el volumen por default.
	audioPlay_clnBuff();					//limpia buffers M0, M1 del DMA y los de los pads.
	ret = audioPlay_loadPadBuff();			//Realiza precarga de datos en cada buffer.
	if(ret)	
		return ret;
	
	return ret;
}


/******************************************************************
function: 	audioPlay_loadMainBuff
input	:	N/A
output	:	>0: Error. Verificar este valor con enum_PAD y se 
			obtendra el pad que genero error.
			FALSE: Success.
descr	:	carga data nueva en los buffer principales M0 y M1.
			Si M0 esta en uso, carga M1. Aplica tambien para el 
			caso reciproco.
*******************************************************************/
uint8_t audioPlay_loadMainBuff(void)
{
	if(st_flag.dma_M0M1 == 0)
	{
		audioPlay_openPadFileByHit();
		audioPlay_loadPadBuff();
		audioPlay_mix();
		DMA2_OS_memcpy(DMA_buffer_M0, audioBuffer_MIX, sizeof(DMA_buffer_M0));
		st_flag.dma_M0M1 = BUSY_STATE;
	}
	
	if(st_flag.dma_M0M1 == 1)
	{
		audioPlay_openPadFileByHit();
		audioPlay_loadPadBuff();
		audioPlay_mix();
		DMA2_OS_memcpy(DMA_buffer_M1, audioBuffer_MIX, sizeof(DMA_buffer_M0));
		st_flag.dma_M0M1 = BUSY_STATE;
	}
	return 0;
}


/******************************************************************
function: 	audioPlay_mix
input	:	N/A
output	:	>0: Error. Verificar este valor con enum_PAD y se 
			obtendra el pad que genero error.
			FALSE: Success.
descr	:	realiza la mezcla de muestras.
*******************************************************************/
uint8_t audioPlay_mix(void)
{
	DMA_I2S_FORMATBIT mix = 0;
	
	for(int i = 0; i < DMA_I2S_BUFFER_SIZE; i++)
	{
#ifdef	ONLY_SNARE
		mix = 
		  (pad_snare.actv)*(pad_snare.vol)*audioBuffer_SNARE[i]		
#ifdef	WITH_KICK
		+ (pad_kick.actv)*(pad_kick.vol)*audioBuffer_KICK[i]
#endif
#ifdef WITH_HITHAT
		+ (pad_hithat.actv)*(pad_hithat.vol)*audioBuffer_HITHAT[i]
#endif
		;
#else
		mix = 
		(pad_snare.actv)*(pad_snare.vol)*audioBuffer_SNARE[i]			+
		(pad_kick.actv)*(pad_kick.vol)*audioBuffer_KICK[i]				+
		(pad_hithat.actv)*(pad_hithat.vol)*audioBuffer_HITHAT[i]		+
		(pad_tom1.actv)*(pad_tom1.vol)*audioBuffer_TOM1[i]				+
		(pad_tom2.actv)*(pad_tom2.vol)*audioBuffer_TOM2[i]				+
		(pad_tom3.actv)*(pad_tom3.vol)*audioBuffer_TOM3[i]				+
		(pad_cymbalL.actv)*(pad_cymbalL.vol)*audioBuffer_CYMBAL_L[i]	+
		(pad_cymbalR.actv)*(pad_cymbalR.vol)*audioBuffer_CYMBAL_R[i];	
		
#endif
		audioBuffer_MIX[i] = mix;
	}
	return 0;
}





/******************************************************************
function: 	audioPlay_padCurrentTime
input	:	*pad = pad del que se desea obtener info.
output	:	tiempo (seg) reproducido.
descr	:	divide el lseek por el bit rate.
*******************************************************************/
float audioPlay_padCurrentTime(st_PAD *pad)
{
	return ( (float)( (pad->file).lseek )/( (float)((pad->file).fileHeaderInfo.ByteRate)) );
}


/******************************************************************
function: 	audioPlay_setVolumePads
input	:	N/A
output	:	N/A
descr	:	setea el volumen de cada pad.
*******************************************************************/
void audioPlay_defaultVolumePads(void)
{
	pad_snare.vol  	= 0.090;
#ifdef WITH_KICK
	pad_kick.vol   	= 0.300;
#endif
#ifdef WITH_HITHAT
	pad_hithat.vol  = 0.070;
#endif
	
#ifndef ONLY_SNARE
	pad_kick.vol   	= 0.300;
	pad_cymbalL.vol	= 0.050;
	pad_cymbalR.vol	= 0.050;
	pad_hithat.vol	= 0.070;
	pad_tom1.vol	= 0.200;
	pad_tom2.vol	= 0.200;
	pad_tom3.vol	= 0.200;
#endif
}

/******************************************************************
function: 	audioPlay_openPadFile
input	:	N/A
output	:	error: retorna < 0 (negativo) si el problema es con la 
				   SD.
				   retorna el numero correspondiente a la etiqueta 
				   enum_PAD.
descr	:	abre los archivos respectivos de cada PAD.
*******************************************************************/
uint8_t audioPlay_openPadsFile(void)
{
	if(SD_mount())
		return 0xff;
	if(SD_OS_openFilePad(&pad_snare,  FA_READ))
		return SNARE;
#ifdef WITH_KICK
	if(SD_OS_openFilePad(&pad_kick,   FA_READ))
		return KICK;
#endif
#ifdef WITH_HITHAT
	if(SD_OS_openFilePad(&pad_hithat, FA_READ))
		return HITHAT;
#endif
#ifndef ONLY_SNARE
	if(SD_OS_openFilePad(&pad_kick,  FA_READ))
		return KICK;
	if(SD_OS_openFilePad(&pad_hithat,  FA_READ))
		return HITHAT;
	if(SD_OS_openFilePad(&pad_tom1,  FA_READ))
		return TOM1;
	if(SD_OS_openFilePad(&pad_tom2,  FA_READ))
		return TOM2;
	if(SD_OS_openFilePad(&pad_tom3,  FA_READ))
		return TOM3;
	if(SD_OS_openFilePad(&pad_cymbalL,  FA_READ))
		return CYMBAL_L;
	if(SD_OS_openFilePad(&pad_cymbalR,  FA_READ))
		return CYMBAL_R;
#endif
	return 0;
}

/******************************************************************
function: 	audioPlay_actvPads
input	:	N/A
output	:	N/A
descr	:	activa todos los pads.
*******************************************************************/
void audioPlay_actvPads(void)
{
	pad_snare.actv 	  = 1;
	
#ifdef WITH_KICK
	pad_kick.actv  	  = 1;
#endif
	
#ifdef WITH_HITHAT
	pad_hithat.actv   = 1;
#endif

#ifndef ONLY_SNARE
	pad_kick.actv  	  = 1;
	pad_hithat.actv   = 1;
	pad_tom1.actv  	  = 1;
	pad_tom2.actv	  = 1;
	pad_tom3.actv  	  = 1;
	pad_cymbalL.actv  = 1;
	pad_cymbalR.actv  = 1;
#endif
}


/******************************************************************
function: 	audioPlay_clnBuff
input	:	N/A
output	:	N/A
descr	:	activa todos los pads.
*******************************************************************/
void audioPlay_clnBuff(void)
{
	DMA_I2S_clnBuff();
	memset(audioBuffer_SNARE,    0, sizeof(audioBuffer_SNARE));
	
#ifdef WITH_KICK	
	memset(audioBuffer_KICK,     0, sizeof(audioBuffer_KICK));
#endif
	
#ifdef WITH_HITHAT
	memset(audioBuffer_HITHAT,   0, sizeof(audioBuffer_HITHAT));
#endif
	
#ifndef ONLY_SNARE
	memset(audioBuffer_KICK,     0, sizeof(audioBuffer_KICK));
	memset(audioBuffer_HITHAT,   0, sizeof(audioBuffer_HITHAT));
	memset(audioBuffer_TOM1,     0, sizeof(audioBuffer_TOM1));
	memset(audioBuffer_TOM2,     0, sizeof(audioBuffer_TOM2));
	memset(audioBuffer_TOM3,     0, sizeof(audioBuffer_TOM3));
	memset(audioBuffer_CYMBAL_L, 0, sizeof(audioBuffer_CYMBAL_L));
	memset(audioBuffer_CYMBAL_R, 0, sizeof(audioBuffer_CYMBAL_R));
#endif
}







/******************************************************************
function: 	audioPlay_fadeOut
input	:	 a: direccion del sample para dar inicio del fade out.
output	:	N/A.
descr	:	aplica fade out a las ultimas 44 samples.
*******************************************************************/
void audioPlay_fadeOut(DMA_I2S_FORMATBIT *a, DMA_I2S_FORMATBIT pos)
{
    DMA_I2S_FORMATBIT i = 0;//0;
	while( (i < DMA_I2S_BUFFER_SIZE) && ((a[i] = -a[0]*i/pos + a[0]) != 0) )//((a[i] = -a[0]*i/44 + a[0]) != 0) )
		i++;
	while((i < DMA_I2S_BUFFER_SIZE))
		a[i++] = 0;
}










/******************************************************************
function: 	audioPlay_memset0_stpad
input	:	N/A
output	:	N/A
descr	:	hace un memset en 0 a la estructura de los pads.
******************************************************************/
void audioPlay_memset0_stpad(void)
{
	memset(&pad_snare, 0, sizeof(st_PAD));
#ifdef WITH_KICK	
	memset(&pad_kick, 0, sizeof(st_PAD));
#endif

#ifdef WITH_HITHAT	
	memset(&pad_hithat, 0, sizeof(st_PAD));
#endif
	
#ifndef ONLY_SNARE
	memset(&pad_kick, 0, sizeof(st_PAD));
	memset(&pad_hithat, 0, sizeof(st_PAD));
	memset(&pad_tom1, 0, sizeof(st_PAD));
	memset(&pad_tom2, 0, sizeof(st_PAD));
	memset(&pad_tom3, 0, sizeof(st_PAD));
	memset(&pad_cymbalL, 0, sizeof(st_PAD));
	memset(&pad_cymbalR, 0, sizeof(st_PAD));
#endif	
}





/******************************************************************
function: 	audioPlay_verifyPadHit
input	:	padLabel: etiqueta correspondiente al pad a verificar.
output	:	si fue golpeado ese pad.
descr	:	segun verifica si el pad ha sido golpeado.
******************************************************************/
uint8_t audioPlay_verifyPadHit(uint8_t padLabel)
{
	uint8_t r;
	switch(padLabel)
	{
		case SNARE:				r = pad_snare.hit; 	 break;
#ifdef  WITH_KICK
		case KICK:				r = pad_kick.hit; 	 break;
#endif
#ifdef  WITH_HITHAT
		case HITHAT:			r = pad_hithat.hit;  break;
#endif
#ifndef ONLY_SNARE
		case KICK:				r = pad_kick.hit; 	 break;
		case HITHAT:			r = pad_hithat.hit;  break;
		case TOM1:				r = pad_tom1.hit; 	 break;
		case TOM2:				r = pad_tom2.hit; 	 break;
		case TOM3:				r = pad_tom3.hit; 	 break;
		case CYMBAL_L:			r = pad_cymbalL.hit; break;
		case CYMBAL_R:			r = pad_cymbalR.hit; break;
#endif
		default:				r = 255; 			 break;
	}
	return r;
}



/***********************************************************************************
function: 	audioPlay_setDirAndFile
input	:	N/A
output	:	N/A
descr	:	setea el directorio y el archivo segun el formato.
************************************************************************************/
void audioPlay_setDirAndFile(void)
{
	uint8_t padLabel = SNARE;	  //porque SNARE = 0.
	while(padLabel <= CYMBAL_R)   //CYMBAL_R es el 8vo y ultimo pad.
	{
		if(audioPlay_verifyPadHit(padLabel))
		{
			audioPlay_setDirPad(padLabel, (g_demoAudio ? (DIR_DEFAULT):(DIR_USER)) );
			audioPlay_setFileByIntensity(padLabel, g_demoAudio ? (FMT_HYPHEN_NUMBER):(FMT_ONLY_NUMBER));
		}
		padLabel++;
	}
}


/***********************************************************************************
function: 	audioPlay_setDirPad
input	:	padLabel: etiqueta correspondiente al pad.
			dirMode:  si es el directorio /default o el configurado por el usuario.
output	:	N/A
descr	:	guarda la ruta del directorio en la estructura st_PAD segun el modo
			de directorio. Si esta configurado para usar directorio /default o el
			configurado por el usuario.
************************************************************************************/
void audioPlay_setDirPad(uint8_t padLabel, uint8_t dirMode)
{
	switch(padLabel)
	{
		case SNARE:				dirMode == DIR_DEFAULT || dirMode == DIR_DEMO ? 
								(sprintf(pad_snare.file.pathDir, "%s", "/audiolib/default/snare"))
								:(NULL/*TODO: leer la estructura de configuracion*/);
								break;
#ifdef  WITH_KICK
		case KICK:				r = pad_kick.hit; 	 break;
#endif
#ifdef  WITH_HITHAT
		case HITHAT:			r = pad_hithat.hit;  break;
#endif
#ifndef ONLY_SNARE
//		case KICK:				break;
//		case HITHAT:			break;
//		case TOM1:				break;
//		case TOM2:				break;
//		case TOM3:				break;
//		case CYMBAL_L:			break;
//		case CYMBAL_R:			break;
#endif
		default:				break;
	}
}

/****************************************************************************
function: 	audioPlay_setFileByIntensity
input	:	padLabel: etiqueta correspondiente al pad.
			fmtName:  para los distintos formatos de nombre del archivo. 
			Ver enum fmtNameByIntensity.
output	:	N/A
descr	:	guarda en el campo pathDir
*****************************************************************************/
void audioPlay_setFileByIntensity(uint8_t padLabel, uint8_t fmtName)
{
	char auxNumber[6];
	memset(auxNumber, 0, sizeof(auxNumber));
	
	switch(padLabel)
	{
		case SNARE:				audioPlay_setFmtName(fmtName, auxNumber, pad_snare.intensidad);
								//sprintf(path, "%s/snare%s.wav", pad_snare.file.pathDir, auxNumber);
								sprintf(pad_snare.file.path, "%s/snare%s.wav", pad_snare.file.pathDir, auxNumber);
								break;
#ifdef  WITH_KICK
		case KICK:				break;
#endif
#ifdef  WITH_HITHAT
		case HITHAT:			break;
#endif
#ifndef ONLY_SNARE
//		case KICK:				break;
//		case HITHAT:			break;
//		case TOM1:				break;
//		case TOM2:				break;
//		case TOM3:				break;
//		case CYMBAL_L:			break;
//		case CYMBAL_R:			break;
#endif
		default:				break;
	}	
}



/************************************************************************************
function: 	audioPlay_setFmtName
input	:	fmt:		formato del nombre del archivo. Ver enum fmtNameByIntensity.
			dst:		direccion de memoria donde se guardara la ruta.
			intensity:  intensidad del hit. Ver st_PAD.
output	:	N/A
descr	:	setea el nombre del archivo segun el formato.
**************************************************************************************/
void audioPlay_setFmtName(uint8_t fmt, char* dst, uint8_t intensity)
{
	switch(fmt)
	{
		case FMT_HYPHEN_NUMBER: intensity < 10 ? (sprintf(dst, "-0%d", intensity)):(sprintf(dst, "-%d", intensity)); break;
		case FMT_ONLY_NUMBER:   break;
		default: break;
	}
}



/**************************************************************************************
function: 	audioPlay_openPadFileByHit
input	:	N/A
output	:	N/A
descr	:	verifica si el campo hit de un pad esta en TRUE. si es asi, setea el 
			directorio segun la intensidad del golpe y abre el archivo correspondiente,
			obtiene los metadatos y se coloca el lseek = 44. Setea en cero el campo hit
***************************************************************************************/
void audioPlay_openPadFileByHit(void)
{
	st_PAD* pad = NULL;
	uint8_t padLabel = SNARE;	  //porque SNARE = 0.
	while(padLabel <= CYMBAL_R)   //CYMBAL_R es el 8vo y ultimo pad.
	{
		if(audioPlay_verifyPadHit(padLabel))
		{
			pad = utils_padPointer(padLabel);
			audioPlay_setDirPad(padLabel, (g_demoAudio ? (DIR_DEFAULT):(DIR_USER)) );
			audioPlay_setFileByIntensity(padLabel, g_demoAudio ? (FMT_HYPHEN_NUMBER):(FMT_ONLY_NUMBER));
			audioPlay_openPadFiles(padLabel);
			pad->hit = 0;
		}
		padLabel++;
	}
}


/***************************************************************************************
function: 	audioPlay_openPadFiles
input	:	padLabel: etiqueta correspondiente al pad.
output	:	retorna error segun enum FRESULT.
descr	:	cierra y abre el archivo segun la ruta del pad (seteada antes con setDirPad
			y setFileByIntensity). Obtiene los metadata y sete el lseek = 44;
****************************************************************************************/
uint8_t audioPlay_openPadFiles(uint8_t padLabel)
{
	st_PAD* pad = NULL;
	uint8_t ret;
	pad = utils_padPointer(padLabel);
	ret = SD_OS_closeFilePad(pad);
	if(ret != FR_OK)
		return ret;
	ret = SD_OS_openFilePad(pad, FA_READ);
	if(ret != FR_OK)
		return ret;
	pad->file.lseek = 0;
	audioPlay_GetFileInfoFromSD(&(pad->file));
	pad->file.lseek = 44;
	return ret;
}







