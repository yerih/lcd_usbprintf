/***************************************************************************
Archivo: buttons_OS.c
Autor  : Yerih Iturriago
Descrip: libreria de sistema operativo que contiene definiciones globales.
****************************************************************************/

#include "buttons_OS.h"





//------------------------------------------------------------------------------- Variables
uint8_t availButtons[80];						//para indicar si un pin esta ocupado.
uint8_t createdButtons[LIMITE_BOTONES];			//guarda la etiqueta del boton creado.
uint8_t pressedButtons[LIMITE_BOTONES];			//guarda la etiqueta del boton presionado.
uint8_t iBut = 0;								//indice del arreglo de botones presionados pressedButtons[].


//------------------------------------------------------------------------------- Funciones

/*****************************************************************
function: 	buttons_initAvailButtons
input:		N/A
output:		N/A
descr:		inicializa el arreglo availButtons con la informacion
			correspondiente.
*******************************************************************/
void buttons_initAvailButtons(void)
{
	memset(availButtons,   0, sizeof(availButtons));
	memset(createdButtons, 0, sizeof(createdButtons));
	memset(pressedButtons, 0, sizeof(pressedButtons));
	buttons_OS_updateCreatedButtons((enum_BUTTONS)0);
	buttons_OS_scanAvailButtons(availButtons);
}


/*****************************************************************
function: 	buttons_OS_labelToStr
input:		label: etiqueta del pin segun enum_BUTTONS
output:		el string de la etiqueta.
descr:		retorna el string de la etiqueta.
*******************************************************************/
char* buttons_OS_labelToStr(enum_BUTTONS label)
{
	char* ret = NULL;
	switch(label)
	{
		//PA
		case 1:	 ret = "PA0"; break;
		case 2:	 ret = "PA1"; break;
		case 3:	 ret = "PA2"; break;
		case 4:	 ret = "PA3"; break;
		case 5:	 ret = "PA4"; break;
		case 6:	 ret = "PA5"; break;
		case 7:	 ret = "PA6"; break;
		case 8:	 ret = "PA7"; break;
		case 9:	 ret = "PA8"; break;
		case 10: ret = "PA9"; break;
		case 11: ret = "PA10"; break;
		case 12: ret = "PA11"; break;
		case 13: ret = "PA12"; break;
		case 14: ret = "PA13"; break;
		case 15: ret = "PA14"; break;
		case 16: ret = "PA15"; break;
		
		//PB
		case 17: ret = "PB0"; break;
		case 18: ret = "PB1"; break;
		case 19: ret = "PB2"; break;
		case 20: ret = "PB3"; break;
		case 21: ret = "PB4"; break;
		case 22: ret = "PB5"; break;
		case 23: ret = "PB6"; break;
		case 24: ret = "PB7"; break;
		case 25: ret = "PB8"; break;
		case 26: ret = "PB9"; break;
		case 27: ret = "PB10"; break;
		case 28: ret = "PB11"; break;
		case 29: ret = "PB12"; break;
		case 30: ret = "PB13"; break;
		case 31: ret = "PB14"; break;
		case 32: ret = "PB15"; break;
		
		//PC
		case 33: ret = "PC0"; break;
		case 34: ret = "PC1"; break;
		case 35: ret = "PC2"; break;
		case 36: ret = "PC3"; break;
		case 37: ret = "PC4"; break;
		case 38: ret = "PC5"; break;
		case 39: ret = "PC6"; break;
		case 40: ret = "PC7"; break;
		case 41: ret = "PC8"; break;
		case 42: ret = "PC9"; break;
		case 43: ret = "PC10"; break;
		case 44: ret = "PC11"; break;
		case 45: ret = "PC12"; break;
		case 46: ret = "PC13"; break;
		case 47: ret = "PC14"; break;
		case 48: ret = "PC15"; break;
		
		//PD
		case 49: ret = "PD0"; break;
		case 50: ret = "PD1"; break;
		case 51: ret = "PD2"; break;
		case 52: ret = "PD3"; break;
		case 53: ret = "PD4"; break;
		case 54: ret = "PD5"; break;
		case 55: ret = "PD6"; break;
		case 56: ret = "PD7"; break;
		case 57: ret = "PD8"; break;
		case 58: ret = "PD9"; break;
		case 59: ret = "PD10"; break;
		case 60: ret = "PD11"; break;
		case 61: ret = "PD12"; break;
		case 62: ret = "PD13"; break;
		case 63: ret = "PD14"; break;
		case 64: ret = "PD15"; break;
		
		//PE
		case 65: ret = "PE0"; break;
		case 66: ret = "PE1"; break;
		case 67: ret = "PE2"; break;
		case 68: ret = "PE3"; break;
		case 69: ret = "PE4"; break;
		case 70: ret = "PE5"; break;
		case 71: ret = "PE6"; break;
		case 72: ret = "PE7"; break;
		case 73: ret = "PE8"; break;
		case 74: ret = "PE9"; break;
		case 75: ret = "PE10"; break;
		case 76: ret = "PE11"; break;
		case 77: ret = "PE12"; break;
		case 78: ret = "PE13"; break;
		case 79: ret = "PE14"; break;
		case 80: ret = "PE15"; break;
		
		default: ret = "unknown"; break;
	}
	return ret;
}

/*****************************************************************
function: 	buttons_OS_labelToPort
input:		label:	etiqueta segun enum_BUTTONS.
output:		numero de pin correspondiente segun la etiqueta.
descr:		Traduce la etiqueta de pines (PA0, PE13, PC4, etc)
			y devuelve el nro. de pin correspondiente.
			retorna 0xff si es una entrada incorrecta.
*******************************************************************/
uint8_t buttons_OS_labelToNpin(enum_BUTTONS label)
{
	if( label == PA0 || label == PB0 || label == PC0 || label == PD0 || label == PE0 )
		return 0;
	else if( label == PA1 || label == PB1 || label == PC1 || label == PD1 || label == PE1 )
		return 1;
	else if( label == PA2 || label == PB2 || label == PC2 || label == PD2 || label == PE2 )
		return 2;
	else if( label == PA3 || label == PB3 || label == PC3 || label == PD3 || label == PE3 )
		return 3;
	else if( label == PA4 || label == PB4 || label == PC4 || label == PD4 || label == PE4 )
		return 4;
	else if( label == PA5 || label == PB5 || label == PC5 || label == PD5 || label == PE5 )
		return 5;
	else if( label == PA6 || label == PB6 || label == PC6 || label == PD6 || label == PE6 )
		return 6;
	else if( label == PA7 || label == PB7 || label == PC7 || label == PD7 || label == PE7 )
		return 7;
	else if( label == PA8 || label == PB8 || label == PC8 || label == PD8 || label == PE8 )
		return 8;
	else if( label == PA9 || label == PB9 || label == PC9 || label == PD9 || label == PE9 )
		return 9;
	else if( label == PA10 || label == PB10 || label == PC10 || label == PD10 || label == PE10 )
		return 10;
	else if( label == PA11 || label == PB11 || label == PC11 || label == PD11 || label == PE11 )
		return 11;
	else if( label == PA12 || label == PB12 || label == PC12 || label == PD12 || label == PE12 )
		return 12;
	else if( label == PA13 || label == PB13 || label == PC13 || label == PD13 || label == PE13 )
		return 13;
	else if( label == PA14 || label == PB14 || label == PC14 || label == PD14 || label == PE14 )
		return 14;
	else if( label == PA15 || label == PB15 || label == PC15 || label == PD15 || label == PE15 )
		return 15;
	else
		return 0xff;
}


/*****************************************************************
function: 	buttons_OS_labelToPort
input:		label:	etiqueta segun enum_BUTTONS.
output:		puerto correspondiente a la etiqueta.
descr:		Traduce la etiqueta de pines (PA0, PE13, PC4, etc)
			y devuelve la direccion del puerto correspondiente.
*******************************************************************/
GPIO_TypeDef* buttons_OS_labelToPort(uint16_t label)
{
	GPIO_TypeDef* gpio = NULL;
	if( (label >= PA0)&&(label <= PA15) )
		gpio = GPIOA;
	else if( (label >= PB0)&&(label <= PB15) )
		gpio = GPIOB;
	else if( (label >= PC0)&&(label <= PC15) )
		gpio = GPIOC;
	else if( (label >= PD0)&&(label <= PD15) )
		gpio = GPIOD;
	else if( (label >= PE0)&&(label <= PE15) )
		gpio = GPIOE;
	
	return gpio;
}





/*****************************************************************
function: 	buttons_OS_labelToPin
input:		label:	etiqueta correspondiente a enum_BUTTONS.
output:		nro de pin en HEX, correspondiente a la etiqueta.
descr:		Traduce la etiqueta de pines (PA0, PE13, PC4, etc)
			y devuelve la direccion de pin correspondiente.
*******************************************************************/
uint16_t buttons_OS_labelToPin(enum_BUTTONS label)
{
	if( label == PA0 || label == PB0 || label == PC0 || label == PD0 || label == PE0 )
		return GPIO_PIN_0;
	else if( label == PA1 || label == PB1 || label == PC1 || label == PD1 || label == PE1 )
		return GPIO_PIN_1;
	else if( label == PA2 || label == PB2 || label == PC2 || label == PD2 || label == PE2 )
		return GPIO_PIN_2;
	else if( label == PA3 || label == PB3 || label == PC3 || label == PD3 || label == PE3 )
		return GPIO_PIN_3;
	else if( label == PA4 || label == PB4 || label == PC4 || label == PD4 || label == PE4 )
		return GPIO_PIN_4;
	else if( label == PA5 || label == PB5 || label == PC5 || label == PD5 || label == PE5 )
		return GPIO_PIN_5;
	else if( label == PA6 || label == PB6 || label == PC6 || label == PD6 || label == PE6 )
		return GPIO_PIN_6;
	else if( label == PA7 || label == PB7 || label == PC7 || label == PD7 || label == PE7 )
		return GPIO_PIN_7;
	else if( label == PA8 || label == PB8 || label == PC8 || label == PD8 || label == PE8 )
		return GPIO_PIN_8;
	else if( label == PA9 || label == PB9 || label == PC9 || label == PD9 || label == PE9 )
		return GPIO_PIN_9;
	else if( label == PA10 || label == PB10 || label == PC10 || label == PD10 || label == PE10 )
		return GPIO_PIN_10;
	else if( label == PA11 || label == PB11 || label == PC11 || label == PD11 || label == PE11 )
		return GPIO_PIN_11;
	else if( label == PA12 || label == PB12 || label == PC12 || label == PD12 || label == PE12 )
		return GPIO_PIN_12;
	else if( label == PA13 || label == PB13 || label == PC13 || label == PD13 || label == PE13 )
		return GPIO_PIN_13;
	else if( label == PA14 || label == PB14 || label == PC14 || label == PD14 || label == PE14 )
		return GPIO_PIN_14;
	else
		return GPIO_PIN_15;	
}



/*****************************************************************
function: 	buttons_OS_isAvail
input:		pin:      nro de pin (1, 2, 3, 4, 5, 6, 7...14, 15).
			GPIO:	  el puerto respectivo.
output:		TRUE: ocupado. FALSE: disponible.
descr:		Verifica si un pin esta disponible.
*******************************************************************/
uint8_t buttons_OS_isAvail(uint8_t pin, GPIO_TypeDef* GPIOx)
{
	uint8_t i     = 0; 			//Se usara como contador y retornador.
    uint32_t mask = 0xf;
    uint8_t reg   = 0;
	uint8_t IO    = buttons_OS_verifyIOpin( buttons_OS_pinToLabel(pin, GPIOx)) != GPIO_MODE_INPUT;  //verifica si es entrada u otro modo.

    if(pin > 7)
    {
        reg = 1;
        pin -= 8;
    }

    while(i++ < pin) 
        mask <<= 4;
    (mask & GPIOx->AFR[reg]) > 0 ? (i = 1) : (i = 0); //1: ocupado. 0: disponible.
    return (i | IO);
}


/*****************************************************************
function: 	buttons_OS_createButton
input:		pin:      segun la etiqueta de la tarjeta. Se utiliza
					  enum_BUTTONS. Ejemplo: PA0
			pullMode: GPIO_PULLDOWN, GPIO_NOPULL, etc. Si 
					  pullMode = DEFAULT, se aplicara PULL_UP.
output:		TRUE: si el pino esta ocupado. FALSE: crea el boton.
descr:		Crea botones.
			Verifica si el pin esta ocupado. Si lo esta, retorna 0.
			Si esta vacio, crea el boton. Los botones en DEFAULT
			estan conectados conectados a ground.
*******************************************************************/
uint8_t buttons_OS_createButton(enum_BUTTONS label, uint8_t pullMode)
{
	//pre
	if(buttons_OS_labelAvailPin(label))
		return 1;
	
	GPIO_InitTypeDef GPIO;
	GPIO_TypeDef* gpio = NULL;
	
	GPIO.Alternate = 0x00; 					//ninguna funcion alterna, solo input.
	GPIO.Mode      = GPIO_MODE_INPUT;
	GPIO.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO.Pin 	   = buttons_OS_labelToPin(label);
	gpio     	   = buttons_OS_labelToPort(label);
	(pullMode 	  == DEFAULT) ? (GPIO.Pull = GPIO_PULLUP) : (GPIO.Pull = pullMode);
	HAL_GPIO_Init(gpio, &GPIO);
	availButtons[label-1] = 1;  			//se actualiza la lista de pines disponibles u ocupados.
	buttons_OS_updateCreatedButtons(label);	//registra nuevo boton creado..
	return 0;
}



/*****************************************************************
function: 	buttons_OS_scanAvailButtons	
input:		a[]: arreglo q indica los pines que estan diponibles y 
				 cuales no.
descr:		escanea los pines y guarda en un arreglo cuales estan 
			disponibles u ocupados.
			Disponibles = 0; Ocupados = 1;
*******************************************************************/
void buttons_OS_scanAvailButtons(uint8_t* a)
{
	uint8_t i = 0; //cantidad total de pines (80 para stm32f407).
	
	while(i < 80)
	{
		if(i < 16)
			a[i] = buttons_OS_isAvail(i,    GPIOA);
		else if(i < 32)
			a[i] = buttons_OS_isAvail(i-16, GPIOB);
		else if(i < 48)
			a[i] = buttons_OS_isAvail(i-32, GPIOC);
		else if(i < 64)
			a[i] = buttons_OS_isAvail(i-48, GPIOD);
		else
			a[i] = buttons_OS_isAvail(i-64, GPIOE);
		i++;
	}
}



/*****************************************************************
function: 	buttons_OS_printAvailButtons	
input:		a[]: arreglo q indica los pines que estan diponibles y 
				 cuales no.
descr:		imprime el arreglo de pines disponibles y ocupados.
*******************************************************************/
void buttons_OS_printAvailButtons(uint8_t *a)
{
	uint8_t i = 0;
	uint8_t tempx = st_LCD.x;
	uint8_t tempy = st_LCD.y;
	st_LCD.x = 2;
	st_LCD.y = 3;
	
	LCD_clr();
	lcd_printf("Lista de pines disponibles\n");
	lcd_printf("Disponibles = 0, ocupados 1.\n");
	lcd_printf("---------------------------------------------\n");
	
	while(i < 16)
	{
		if(i < 10)
			lcd_printf("PA%d  = %d PB%d  = %d PC%d  = %d PD%d  = %d PE%d  = %d\n", 
						   i,  a[i],  i, a[i+16], i, a[i+32],i, a[i+48],i, a[i+64]);
		else
			lcd_printf("PA%d = %d PB%d = %d PC%d = %d PD%d = %d PE%d = %d\n", 
						   i,  a[i],  i, a[i+16], i, a[i+32],i, a[i+48],i, a[i+64]);
		i++;
	}
	
	st_LCD.x = tempx;
	st_LCD.y = tempy;
}


/*****************************************************************
function: 	buttons_OS_isPress	
input:		N/A
output:		TRUE: si un boton es presionado. FALSE: si no.
descr:		TRUE: tecla presionada. FALSE: nada se presiono.
			Version 1: verifica solo para botones K0 y K1.
			Version 2: guarda en pressedButtons[] los botones 
			presionados.
*******************************************************************/
uint8_t buttons_OS_isPress(void)
{
	static uint8_t i = 0;
	uint8_t j = 0;
	uint8_t p = 0;
	
	i >= LIMITE_BOTONES ? (i = 0): (i++);
	
	if( !HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3) )
		pressedButtons[i++] = PE3;
	if( !HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_4) )	
		pressedButtons[i++] = PE4;
	
	//TODO: falta el boton K_UP
	while(j < LIMITE_BOTONES && createdButtons[j] != 0 )
	{
		if( !HAL_GPIO_ReadPin(buttons_OS_labelToPort((enum_BUTTONS)createdButtons[j]), buttons_OS_labelToPin((enum_BUTTONS)createdButtons[j]) ) )
		{
			pressedButtons[i++] = createdButtons[j];
			p = 1;
		}
		j++;
	}
	
	j = 0;
	while(j < LIMITE_BOTONES && p == 0)	//Recorre el buffer de botones presionados.
	{
		if(pressedButtons[j++] > 0)
			p = 1;
	}
	
	//HAL_Delay(15);
	HAL_Delay(7);
	return p;
}



/*****************************************************************
function: 	buttons_OS_getKey	
input:		N/A
output:		Boton presionado.
descr:		retorna el valor de la etiqueta del boton presionado.
			0 o 0xff si no se oprimio boton.
*******************************************************************/
uint8_t buttons_OS_getKey(void)
{
	if(iBut >= LIMITE_BOTONES)
		iBut = 0;
	return (pressedButtons[iBut] != 0 ? (pressedButtons[iBut++]) : (0) );
}



/*******************************************************************
function: 	buttons_OS_verifyIOpin
input:		label
output:		
			GPIO_MODE_INPUT      = Input Floating Mode.
			GPIO_MODE_OUTPUT_PP  = Output Push Pull Mode.
			GPIO_MODE_OUTPUT_OD  = Output Open Drain Mode.
			GPIO_MODE_AF_PP      = Alter. Function Push Pull Mode.
			GPIO_MODE_AF_OD      = No se retorna.
descr:		retorna el modo en que esta configurado el pin.
*********************************************************************/
uint8_t buttons_OS_verifyIOpin(enum_BUTTONS label)
{
	//buttons_OS_labelToPin()
	GPIO_TypeDef* GPIOx = buttons_OS_labelToPort(label);
	uint16_t pin		= buttons_OS_labelToNpin(label);
	uint32_t moder      = GPIOx->MODER;
	while(pin-- > 0)
		moder >>= 2;
	return (moder & 3);
}

/*******************************************************************
function: 	buttons_OS_strIOpin
input:		label
output:		INPUT      = Input Floating Mode.
			OUTPUT_PP  = Output Push Pull Mode.
			OUTPUT_OD  = Output Open Drain Mode.
			AF_PP      = Alter. Function Push Pull Mode.
descr:		retorna el modo en que esta configurado el pin en forma 
			de str.
*********************************************************************/
char* buttons_OS_strIOpin(enum_BUTTONS label)
{
	uint8_t mode = buttons_OS_verifyIOpin(label);
	if(mode == GPIO_MODE_INPUT)
		return "INPUT";
	else if(mode == GPIO_MODE_OUTPUT_PP)
		return "OUTPUT_PP";
	else if(mode == GPIO_MODE_OUTPUT_OD)
		return "OUTPUT_OD";
	else if(mode == GPIO_MODE_AF_PP)
		return "AF_PP";
	else
		return "Unknow mode";
}

/*******************************************************************
function: 	buttons_OS_pinToLabel
input:		pin:   nro. de pin
			GPIOx: puerto correspondiente al pin.
output:		etiqueta correspondiente al pin segun enum_BUTTONS.
descr:		retorna etiqueta correspondiente al pin.
*********************************************************************/
enum_BUTTONS buttons_OS_pinToLabel(uint8_t pin, GPIO_TypeDef* GPIOx)
{
	if(GPIOx == GPIOA)
	{
		switch(pin)
		{
			case 0:	 pin = PA0; break;
			case 1:	 pin = PA1; break;
			case 2:	 pin = PA2; break;
			case 3:	 pin = PA3; break;
			case 4:	 pin = PA4; break;
			case 5:	 pin = PA5; break;
			case 6:	 pin = PA6; break;
			case 7:	 pin = PA7; break;
			case 8:	 pin = PA8; break;
			case 9:	 pin = PA9; break;
			case 10: pin = PA10; break;
			case 11: pin = PA11; break;
			case 12: pin = PA12; break;
			case 13: pin = PA13; break;
			case 14: pin = PA14; break;
			case 15: pin = PA15; break;
			default: break;
		}
	}
	else if(GPIOx == GPIOB)
	{
		switch(pin)
		{
			case 0:	 pin = PB0; break;
			case 1:	 pin = PB1; break;
			case 2:	 pin = PB2; break;
			case 3:	 pin = PB3; break;
			case 4:	 pin = PB4; break;
			case 5:	 pin = PB5; break;
			case 6:	 pin = PB6; break;
			case 7:	 pin = PB7; break;
			case 8:	 pin = PB8; break;
			case 9:	 pin = PB9; break;
			case 10: pin = PB10; break;
			case 11: pin = PB11; break;
			case 12: pin = PB12; break;
			case 13: pin = PB13; break;
			case 14: pin = PB14; break;
			case 15: pin = PB15; break;
			default: break;
		}
	}
	else if(GPIOx == GPIOC)
	{
		switch(pin)
		{
			case 0:	 pin = PC0; break;
			case 1:	 pin = PC1; break;
			case 2:	 pin = PC2; break;
			case 3:	 pin = PC3; break;
			case 4:	 pin = PC4; break;
			case 5:	 pin = PC5; break;
			case 6:	 pin = PC6; break;
			case 7:	 pin = PC7; break;
			case 8:	 pin = PC8; break;
			case 9:	 pin = PC9; break;
			case 10: pin = PC10; break;
			case 11: pin = PC11; break;
			case 12: pin = PC12; break;
			case 13: pin = PC13; break;
			case 14: pin = PC14; break;
			case 15: pin = PC15; break;
			default: break;
		}
	}
	else if(GPIOx == GPIOD)
	{
		switch(pin)
		{
			case 0:	 pin = PD0; break;
			case 1:	 pin = PD1; break;
			case 2:	 pin = PD2; break;
			case 3:	 pin = PD3; break;
			case 4:	 pin = PD4; break;
			case 5:	 pin = PD5; break;
			case 6:	 pin = PD6; break;
			case 7:	 pin = PD7; break;
			case 8:	 pin = PD8; break;
			case 9:	 pin = PD9; break;
			case 10: pin = PD10; break;
			case 11: pin = PD11; break;
			case 12: pin = PD12; break;
			case 13: pin = PD13; break;
			case 14: pin = PD14; break;
			case 15: pin = PD15; break;
			default: break;
		}
	}
	else if(GPIOx == GPIOE)
	{
		switch(pin)
		{
			case 0:	 pin = PE0; break;
			case 1:	 pin = PE1; break;
			case 2:	 pin = PE2; break;
			case 3:	 pin = PE3; break;
			case 4:	 pin = PE4; break;
			case 5:	 pin = PE5; break;
			case 6:	 pin = PE6; break;
			case 7:	 pin = PE7; break;
			case 8:	 pin = PE8; break;
			case 9:	 pin = PE9; break;
			case 10: pin = PE10; break;
			case 11: pin = PE11; break;
			case 12: pin = PE12; break;
			case 13: pin = PE13; break;
			case 14: pin = PE14; break;
			case 15: pin = PE15; break;
			default: break;
		}
	}
	else
		pin = 0;
	return (enum_BUTTONS)pin;
}



/*******************************************************************
function: 	buttons_OS_labelAvailPin
input:		label:   etiqueta segun enum_BUTTONS.
output:		TRUE: ocupado. FALSE: disponible.
descr:		verifica disponibilidad del pin.
*********************************************************************/
uint8_t buttons_OS_labelAvailPin(enum_BUTTONS label)
{
	return buttons_OS_isAvail(buttons_OS_labelToPin(label), buttons_OS_labelToPort(label));
}


/*******************************************************************
function: 	buttons_OS_updateCreatedButtons
input:		label: etiqueta del pin donde esta el boton.
output:		TRUE: sin espacio para botones. FALSE: exito.
descr:		guarda en createdButtons[] la etiqueta del pin creado.
*********************************************************************/
uint8_t buttons_OS_updateCreatedButtons(enum_BUTTONS label)
{
	uint8_t i = 0;
	
	if(buttons_OS_verifyIOpin(PE3) == GPIO_MODE_INPUT)
	{
		availButtons[PE3-1] = 1;
		createdButtons[i++] = K0;
	}
	
	if(buttons_OS_verifyIOpin(PE4) == GPIO_MODE_INPUT)
	{
		availButtons[PE4-1] = 1;
		createdButtons[i++] = K1;
	}
	
	//TODO: agregar boton K_UP
	
	if(label < 1 && label > 80) //Si la etiqueta es incorrecta retorna.
		return 1;
	while(createdButtons[i] != 0 && i < LIMITE_BOTONES && createdButtons[i] != label)
		i++;
	if(i == LIMITE_BOTONES && createdButtons[i] == label)
		return 1;
	createdButtons[i] = label;
	return 0;
}



/*******************************************************************
function: 	buttons_OS_scanCreatedButtons
input:		label: etiqueta del pin donde esta el boton.
output:		TRUE: ocupado. FALSE: disponible.
descr:		pregunta si un boton esta creado.
*********************************************************************/
uint8_t buttons_OS_scanCreatedButtons(enum_BUTTONS label)
{
	uint8_t i = 0;
	while(i < LIMITE_BOTONES && createdButtons[i] != label)
		i++;
	return i == LIMITE_BOTONES;
}


/*******************************************************************
function: 	buttons_OS_clearBuffButtons
input:		N/A.
output:		N/A.
descr:		limpia el buffer de botones.
*********************************************************************/
void buttons_OS_clearBuffButtons(void)
{
	memset(pressedButtons, 0, sizeof(pressedButtons));
	iBut = 0;
}

/*******************************************************************
function: 	buttons_OS_keyPress
input:		N/A.
output:		FALSE: success.
descr:		permanece en loop mientras esta presionado el boton y
			registra en el buffer el boton presionado.
*********************************************************************/
uint8_t buttons_OS_keyPress(void)
{
	int8_t i = 0;
	GPIO_TypeDef* port = NULL;
	uint8_t pin		   = 0;
	
	while(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3))
	{
		pressedButtons[iBut] = PE3;
		iBut >= LIMITE_BOTONES ? (iBut = 0): (iBut++);
	}
	while(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_4))
	{
		pressedButtons[iBut] = PE4;
		iBut >= LIMITE_BOTONES ? (iBut = 0): (iBut++);
	}
	while(createdButtons[i] == PE4 || createdButtons[i] == PE3)i++; //TODO: agregar boton K_UP.
	while(createdButtons[i] != 0)
	{
		port = buttons_OS_labelToPort((enum_BUTTONS)createdButtons[i]);
		pin  = buttons_OS_labelToPin((enum_BUTTONS)createdButtons[i]);
		while(!HAL_GPIO_ReadPin(port, pin))
		{
			pressedButtons[iBut] = createdButtons[i];
			iBut >= LIMITE_BOTONES ? (iBut = 0): (iBut++);
		}
		i >= LIMITE_BOTONES ? (i = 0): (i++);
	}
	return 0;
}




/*******************************************************************
function: 	buttons_OS_loopPress
input:		label: etiqueta del boton que se esta presionando.
output:		N/A.
descr:		permanece en un loop hasta que no se presione el boton.
*********************************************************************/
void buttons_OS_loopPress(enum_BUTTONS label)
{
	if(label > 0 && label < 81)
	{
		GPIO_TypeDef* port = buttons_OS_labelToPort(label);
		uint8_t pin		   = buttons_OS_labelToPin(label);
		while(!HAL_GPIO_ReadPin(port, pin));
	}
}


/*******************************************************************
function: 	buttons_OS_readKey
input:		N/A.
output:		etiqueta del boton oprimido
descr:		permanece en un loop hasta que no se presione un boton
			y devuelve la etiqueta del boton oprimido. Limpia el
			buffer.
*********************************************************************/
uint8_t buttons_OS_readKey(void)
{
	uint8_t key = 0;
	buttons_OS_clearBuffButtons();
	buttons_OS_keyPress();
	key = buttons_OS_getKey();
	return key;
}

/*******************************************************************
function: 	buttons_isPress
input:		N/A.
output:		etiqueta del boton oprimido
descr:		devuelve la etiqueta del boton oprimido.
*********************************************************************/
uint8_t buttons_isPress(void)
{
	uint8_t i = 0;
	GPIO_TypeDef* gpio = NULL;
	uint8_t 	   pin = 0;
	
	while(createdButtons[i] != 0)
	{
		gpio = buttons_OS_labelToPort((enum_BUTTONS)createdButtons[i]);
		pin  = buttons_OS_labelToPin((enum_BUTTONS)createdButtons[i]);
		if(!HAL_GPIO_ReadPin(gpio, pin))
		{
			return createdButtons[i];
		}
		i >= LIMITE_BOTONES ? (i = 0): (i++);
	}
	return 0;
}


/*******************************************************************
function: 	buttons_OS_readButt
input:		call: funcion a ser ejecutada si se presiona la tecla 
				  Recibe un parametro key: tecla al ser presionada
output:		N/A
descr:		Ejecuta la funcion call cuando es presionada el boton
			key. Limpia el buffer.
*********************************************************************/
void buttons_OS_readButt(void(call)(uint8_t*))
{
	uint8_t key = 0;
	if( (key = (enum_BUTTONS)buttons_isPress()) != 0)
	{
		call(&key);
	}
}


/*******************************************************************
function: 	buttons_OS_readButtTim
input:		call: funcion a ser ejecutada si se presiona la tecla 
				  Recibe un parametro key: tecla al ser presionada
output:		N/A
descr:		Ejecuta la funcion call cuando es presionada el boton
			key. Permanece en un loop hasta que no se presione un 
			boton. Limpia el buffer.
*********************************************************************/
void buttons_OS_readButtLoop(void(call)(uint8_t*))
{
	uint8_t key = 0;
	if( (key = (enum_BUTTONS)buttons_isPress()) != 0)
	{
		call(&key);
	}
	buttons_OS_loopPress((enum_BUTTONS)key);
}

