/******************************************************************************
  * File Name          : call.c
  * Description        : Herramientas para codificar (debugs, etc).
  * Author			   : Yerih Iturriago.
  ******************************************************************************/
  
#include "call.h"



  
/*******************************************************************
function: 	call_buttonsDebug
input:		key: boton o etiqueta del boton a ser presionado para
			ejecutar esta funcion.
output:		N/A.
descr:		enciende el LED_D2 e imprime en pantalla la etiqueta del
			boton.
*********************************************************************/
void call_buttonsDebug(uint8_t* key)
{
	LED_HIGH_D2;
	utils_printClr("%s ", buttons_OS_labelToStr(*(enum_BUTTONS*)key));
}
  
  
  
/*******************************************************************
function: 	call_playPad
input:		key: boton o etiqueta del boton a ser presionado para
			ejecutar esta funcion.
output:		N/A.
descr:		Reproduce el archivo desde el principio. Resetea el 
			lseek.
			Enciende LED_X dependiendo del boton pulsado.
			imprime el pad correspondiente.
*********************************************************************/
void call_playPad(uint8_t* key)
{
	switch(*key)
	{
#ifndef ONLY_SNARE
		//Se colocan los botones configurados aqui.
#endif
#ifdef WITH_HITHAT 
		case K1:	pad_hithat.actv = 1; pad_hithat.file.lseek = 44; LED_HIGH_D3; utils_printClr("hithat "); break; //Hithat (PE3) 
#endif
#ifdef WITH_KICK
		case K0:	pad_kick.actv   = 1; pad_kick.file.lseek   = 44; LED_HIGH_D3; utils_printClr("kick ");   break; //Kick   (PE4)
#endif
		case K0:	pad_kick.actv   = 1; pad_kick.file.lseek   = 44; LED_HIGH_D3; utils_printClr("kick ");   break; //Kick   (PE4)
		case K1:	pad_hithat.actv = 1; pad_hithat.file.lseek = 44; LED_HIGH_D3; utils_printClr("hithat "); break; //Hithat (PE3) 
		
		//PA
		case PA0:	break;
		case PA1:	pad_snare.actv  = 1; pad_snare.file.lseek  = 44; LED_HIGH_D2; utils_printClr("snare ");  break; //snare	(PA1)
		case PA2:	break;
		case PA3:	break;
		case PA4:	break;
		case PA5:	break;
		case PA6:	break;
		case PA7:	break;
		case PA8:	break;
		case PA9:	break;
		case PA10:	break;
		case PA11:	break;
		case PA12:	break;
		case PA13:	break;
		case PA14:	break;
		case PA15:	break;
		
		//PB
		case PB0:	break;
		case PB1:	break;
		case PB2:	break;
		case PB3:	break;
		case PB4:	break;
		case PB5:	break;
		case PB6:	break;
		case PB7:	break;
		case PB8:	break;
		case PB9:	break;
		case PB10:	break;
		case PB11:	break;
		case PB12:	break;
		case PB13:	break;
		case PB14:	break;
		case PB15:	break;
		
		//PC
		case PC0:	break;
		case PC1:	break;
		case PC2:	break;
		case PC3:	break;
		case PC4:	break;
		case PC5:	break;
		case PC6:	break;
		case PC7:	break;
		case PC8:	break;
		case PC9:	break;
		case PC10:	break;
		case PC11:	break;
		case PC12:	break;
		case PC13:	break;
		case PC14:	break;
		case PC15:	break;
		
		//PD
		case PD0:	break;
		case PD1:	break;
		case PD2:	break;
		case PD3:	break;
		case PD4:	break;
		case PD5:	break;
		case PD6:	break;
		case PD7:	break;
		case PD8:	break;
		case PD9:	break;
		case PD10:	break;
		case PD11:	break;
		case PD12:	break;
		case PD13:	break;
		case PD14:	break;
		case PD15:	break;
		
		//PE
		case PE0:	break;
		case PE1:	break;
		case PE2:	break;
		//case PE3:	break; //K0
		//case PE4:	break; //K1
		case PE5:	break;
		case PE6:	break;
		case PE7:	break;
		case PE8:	break;
		case PE9:	break;
		case PE10:	break;
		case PE11:	break;
		case PE12:	break;
		case PE13:	break;
		case PE14:	break;
		case PE15:	break;
	}
}
	
  
  
  
/*******************************************************************
function: 	call_keyIsPressed
input:		key: boton o etiqueta del boton a ser presionado para
			ejecutar esta funcion.
output:		N/A.
descr:		Reproduce el archivo desde el principio. Resetea el 
			lseek.
			Enciende LED_X dependiendo del boton pulsado.
			imprime el pad correspondiente.
*********************************************************************/
void call_keyIsPressed(uint8_t* key)
{
	switch(*key)
	{
#ifndef ONLY_SNARE
		//Se colocan los botones configurados aqui.
#endif
#ifdef WITH_HITHAT 
		case K1:	pad_hithat.actv = 1; pad_hithat.file.lseek = 44; LED_HIGH_D3; utils_printClr("hithat "); break; //Hithat (PE3) 
#endif
#ifdef WITH_KICK
		case K0:	pad_kick.actv   = 1; pad_kick.file.lseek   = 44; LED_HIGH_D3; utils_printClr("kick ");   break; //Kick   (PE4)
#endif
		case K0:	pad_kick.actv   = 1; pad_kick.file.lseek   = 44; LED_HIGH_D3; utils_printClr("kick ");   break; //Kick   (PE4)
		case K1:	pad_hithat.actv = 1; pad_hithat.file.lseek = 44; LED_HIGH_D3; utils_printClr("hithat "); break; //Hithat (PE3) 
		
		//PA
		case PA0:	break;
		case PA1:	if(!timer_OS_verifyPlayTime(&htim2)) //snare	(PA1)
					{
						LED_HIGH_D3; 
						pad_snare.actv  = 1; pad_snare.file.lseek  = 44; LED_HIGH_D2; utils_printClr("snare ");
					}
					else
					{
						LED_LOW_D3;
					}
					break;
		case PA2:	break;
		case PA3:	break;
		case PA4:	break;
		case PA5:	break;
		case PA6:	break;
		case PA7:	break;
		case PA8:	break;
		case PA9:	break;
		case PA10:	break;
		case PA11:	break;
		case PA12:	break;
		case PA13:	break;
		case PA14:	break;
		case PA15:	break;
		
		//PB
		case PB0:	break;
		case PB1:	break;
		case PB2:	break;
		case PB3:	break;
		case PB4:	break;
		case PB5:	break;
		case PB6:	break;
		case PB7:	break;
		case PB8:	break;
		case PB9:	break;
		case PB10:	break;
		case PB11:	break;
		case PB12:	break;
		case PB13:	break;
		case PB14:	break;
		case PB15:	break;
		
		//PC
		case PC0:	break;
		case PC1:	break;
		case PC2:	break;
		case PC3:	break;
		case PC4:	break;
		case PC5:	break;
		case PC6:	break;
		case PC7:	break;
		case PC8:	break;
		case PC9:	break;
		case PC10:	break;
		case PC11:	break;
		case PC12:	break;
		case PC13:	break;
		case PC14:	break;
		case PC15:	break;
		
		//PD
		case PD0:	break;
		case PD1:	break;
		case PD2:	break;
		case PD3:	break;
		case PD4:	break;
		case PD5:	break;
		case PD6:	break;
		case PD7:	break;
		case PD8:	break;
		case PD9:	break;
		case PD10:	break;
		case PD11:	break;
		case PD12:	break;
		case PD13:	break;
		case PD14:	break;
		case PD15:	break;
		
		//PE
		case PE0:	break;
		case PE1:	break;
		case PE2:	break;
		//case PE3:	break; //K0
		//case PE4:	break; //K1
		case PE5:	break;
		case PE6:	break;
		case PE7:	break;
		case PE8:	break;
		case PE9:	break;
		case PE10:	break;
		case PE11:	break;
		case PE12:	break;
		case PE13:	break;
		case PE14:	break;
		case PE15:	break;
	}
}
  


/*******************************************************************
function: 	call_irqTest
input:		GPIO_Pin: pin de interrupcion.
output:		N/A.
descr:		funcion callback de interrupciones externas.
*********************************************************************/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	UNUSED(GPIO_Pin); //verifica si el pin est� en uso.
	//call_irqTest(GPIO_Pin);
	call_incrementPadCntHit(GPIO_Pin);
}



/*******************************************************************
function: 	call_irqTest
input:		GPIO_Pin: pin de interrupcion.
output:		N/A.
descr:		para testeo de Interrupciones externas. Enciende LED D2,
			imprime texto en pantalla.
*********************************************************************/
void call_irqTest(uint16_t GPIO_Pin)
{
	LED_HIGH_D2;
	utils_printClr("%s \n", utils_getIRQlabel(GPIO_Pin));
	LED_LOW_D2;
}



/*******************************************************************
function: 	call_adcGetPadIntensity
input:		GPIO_Pin: pin de interrupcion.
output:		FALSE:    exito. Delo contrario, ver HAL_RETURN.
descr:		obtiene valor del adc correspondiente al pad.
*********************************************************************/
int call_adcGetPadIntensity(uint16_t GPIO_Pin)
{
	int r = 0;
	
	switch(GPIO_Pin)
	{
		case IRQ_SNARE_Pin:		//SNARE		PA2
								r = call_adcGetIntensity(ADC_SNARE_CHANNEL, &(pad_snare.intensidad));
								utils_printClr("%s %d\n", utils_getIRQlabel(GPIO_Pin), pad_snare.intensidad); 		
								break;

		case IRQ_KICK_Pin:		//KICK		PA3
								r = call_adcGetIntensity(ADC_KICK_CHANNEL, &(pad_kick.intensidad));
								utils_printClr("%s %d\n", utils_getIRQlabel(GPIO_Pin), pad_kick.intensidad); 
								break; 
		
		case IRQ_HITHAT_Pin:	//HITHAT	PA4
//								r = call_adcGetIntensity(ADC_HITHAT_CHANNEL, &(pad_hithat.intensidad));
								utils_printClr("%s %d\n", utils_getIRQlabel(GPIO_Pin), pad_hithat.intensidad); 
								break; 
		
		case IRQ_TOM1_Pin:		//TOM1		PA5
//								r = call_adcGetIntensity(ADC_TOM1_CHANNEL, &(pad_tom1.intensidad));
								utils_printClr("%s %d\n", utils_getIRQlabel(GPIO_Pin), pad_tom1.intensidad);
								break; 
		case IRQ_TOM2_Pin:		//TOM2		PA6
//								r = call_adcGetIntensity(ADC_TOM2_CHANNEL, &(pad_tom2.intensidad));
								utils_printClr("%s %d\n", utils_getIRQlabel(GPIO_Pin), pad_tom2.intensidad);
								break; 
		case IRQ_TOM3_Pin:		//TOM3		PA7
//								r = call_adcGetIntensity(ADC_TOM3_CHANNEL, &(pad_tom3.intensidad));
								utils_printClr("%s %d\n", utils_getIRQlabel(GPIO_Pin), pad_tom3.intensidad);
								break; 
		case IRQ_CYMBAL_R_Pin:	//CYMBAL_R	PE5
//								r = call_adcGetIntensity(ADC_CYMBAL_R_CHANNEL, &(pad_cymbalR.intensidad));
								utils_printClr("%s %d\n", utils_getIRQlabel(GPIO_Pin), pad_cymbalR.intensidad);
								break; 
		case IRQ_CYMBAL_L_Pin:	//CYMBAL_L	PE6
//								r = call_adcGetIntensity(ADC_CYMBAL_L_CHANNEL, &(pad_cymbalL.intensidad));
								utils_printClr("%s %d\n", utils_getIRQlabel(GPIO_Pin), pad_cymbalL.intensidad);
								break; 
		
		default:				break;
	}
	return r;
}



/*******************************************************************
function: 	call_changeADCchannel
input:		adcChannel
output:		retorna el valor del ADC
descr:		cambia el canal del ADC segun el pad y retorna el valor
			leido.
*********************************************************************/
void call_changeADCchannel(uint32_t channel)
{
	ADC_ChannelConfTypeDef sConfig;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
	sConfig.Channel = channel;
	HAL_ADC_ConfigChannel(&hadc1, &sConfig);
}



/***********************************************************************
function: 	call_getADCvalue
input:		value: direccion donde sera almacenado el valor leido
output:		retorna FALSE si es exitoso, de lo contrario ver HAL_RETURN
descr:		cambia el canal del ADC segun el pad y retorna el valor
			leido.
*************************************************************************/
int call_getADCvalue(uint8_t* value)
{
	uint8_t r = 0;
	uint32_t adcValue;
	HAL_ADC_Start(&hadc1);
	if( (r = HAL_ADC_PollForConversion(&hadc1, 1)) == HAL_OK)
	{
		adcValue = HAL_ADC_GetValue(&hadc1);
	}
	*value = (adcValue >> 8);
	HAL_ADC_Stop(&hadc1);
	return r;
}






/*******************************************************************
function: 	call_timerADCmanage
input:		N/A
output:		N/A
descr:		obtiene valor del adc correspondiente al pad y verifica
			el contador de cada pad.
*********************************************************************/
void call_timerADCmanage(void)
{
	if(	(pad_snare.tcnt   ||
#ifdef WITH_KICK
		pad_kick.tcnt    ||
#endif
#ifdef WITH_HITHAT
		pad_hithat.tcnt  
#endif
#ifndef ONLY_SNARE
		pad_kick.tcnt || pad_hithat.tcnt || pad_tom1.tcnt || pad_tom2.tcnt || pad_tom3.tcnt || pad_cymbalL.tcnt || pad_cymbalR.tcnt 
#endif
	) == 0)
	{
		g_tcnt = 0;
		HAL_TIM_Base_Stop_IT(&htim2);
	}
	else
	{
		if(pad_snare.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_SNARE_Pin);
			pad_snare.hit = TRUE;
			pad_snare.tcnt = 0;
		}
#ifdef WITH_KICK
		else if(pad_kick.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_KICK_Pin);
			pad_kick.hit = TRUE;
			pad_kick.tcnt = 0;
		}
#endif
#ifdef WITH_HITHAT
		else if(pad_hithat.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_HITHAT_Pin);
			pad_hithat.hit = TRUE;
			pad_hithat.tcnt = 0;
		}
#endif
#ifndef ONLY_SNARE
		else if(pad_kick.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_KICK_Pin);
			pad_kick.hit = TRUE;
			pad_kick.tcnt = 0;
		}
		else if(pad_hithat.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_HITHAT_Pin);
			pad_hithat.hit = TRUE;
			pad_hithat.tcnt = 0;
		}
		else if(pad_tom1.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_TOM1_Pin);
			pad_tom1.hit = TRUE;
			pad_tom1.tcnt = 0;
		}
		else if(pad_tom2.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_TOM2_Pin);
			pad_tom2.hit = TRUE;
			pad_tom2.tcnt = 0;
		}
		else if(pad_tom3.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_TOM3_Pin);
			pad_tom3.hit = TRUE;
			pad_tom3.tcnt = 0;
		}
		else if(pad_cymbalL.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_CYMBAL_L_Pin);
			pad_cymbalL.hit = TRUE;
			pad_cymbalL.tcnt = 0;
		}
		else if(pad_cymbalR.tcnt == g_tcnt)
		{
			call_adcGetPadIntensity(IRQ_CYMBAL_R_Pin);
			pad_cymbalR.hit = TRUE;
			pad_cymbalR.tcnt = 0;
		}
#endif
		
		HAL_TIM_Base_Start_IT(&htim3);
		g_tcnt++;
	}
}










/***********************************************************************
function: 	call_incrementPadCntHit
input:		GPIO_Pin: pin correspondiente a la interrupcion y al pad.
output:		N/A
descr:		incrementa el contador del pad segun el pin correspondiente
			y setea pad.hit en TRUE.
************************************************************************/
void call_incrementPadCntHit(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin)
	{
		case IRQ_SNARE_Pin:		if(pad_snare.tcnt == 0) 			//SNARE	PA9
									pad_snare.tcnt = 2 + g_tcnt;
								break;
#ifdef WITH_KICK 
		case IRQ_KICK_Pin:		if(pad_kick.tcnt == 0) 				//KICK	PD12
									pad_kick.tcnt = 2 + g_tcnt;
								break; 
#endif		
#ifdef WITH_HITHAT
		case IRQ_HITHAT_Pin:	if(pad_hithat.tcnt == 0)
									pad_hithat.tcnt = 2 + g_tcnt;
								break; 
#endif
#ifndef ONLY_SNARE
		case IRQ_KICK_Pin:		if(pad_kick.tcnt == 0) 				//KICK	PD12
									pad_kick.tcnt = 2 + g_tcnt;
								break; 
		case IRQ_HITHAT_Pin:	if(pad_hithat.tcnt == 0)			//HITHAT 
									pad_hithat.tcnt = 2 + g_tcnt;
								break; 
		case IRQ_TOM1_Pin:		if(pad_tom1.tcnt == 0)				//TOM1	PB10
									pad_tom1.tcnt = 2 + g_tcnt;
								break;
		case IRQ_TOM2_Pin:		if(pad_tom2.tcnt == 0)				//TOM2	PE0
									pad_tom2.tcnt = 2 + g_tcnt;
								break;
		case IRQ_TOM3_Pin:		if(pad_tom3.tcnt == 0) 				//TOM3	PE1
									pad_tom3.tcnt = 2 + g_tcnt;
								break; 
		case IRQ_CYMBAL_R_Pin:	if(pad_cymbalR.tcnt == 0)			//CYMBAL_R	PE5
									pad_cymbalR.tcnt = 2 + g_tcnt;
								break; 
		case IRQ_CYMBAL_L_Pin:	if(pad_cymbalL.tcnt == 0)			//CYMBAL_L	PE6
									pad_cymbalL.tcnt = 2 + g_tcnt;
								break; 
#endif
		default:				break;
	}
	
	if(g_tcnt == 0)
	{
		g_tcnt++;
		HAL_TIM_Base_Start_IT(&htim3);//TODO: activar timer.
	}
}








/*******************************************************************************
function: 	call_adcGetIntensity
input:		channel: canal del pad correspondiente.
			value:   direccion donde sera almacenado el valor del ADC.
output:		FALSE si es exitoso, de lo contrario ver HAL_RETURN.
descr:		obtiene el valor del adc segun el canal en que se encuentre el pad.
********************************************************************************/
int call_adcGetIntensity(uint32_t channel, uint8_t* value)
{
	call_changeADCchannel(channel);
	return call_getADCvalue(value);
}











