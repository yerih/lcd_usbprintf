/***************************************************************************
Archivo: DMA_I2S_OS.c
Autor  : Yerih Iturriago
Descrip: libreria para control de protocolo I2S a traves del DMA.
****************************************************************************/

#include "dma_OS.h"


/* ------------------------------------------------ Globales ----------------------------------------------------------- */

//extern DMA_HandleTypeDef hdma_memtomem_dma2_stream0;

/* ------------------------------------------------ Funciones ----------------------------------------------------------- */

/**
  * @brief  Set the DMA Transfer parameter.
  * @param  hdma       pointer to a DMA_HandleTypeDef structure that contains
  *                     the configuration information for the specified DMA Stream.  
  * @param  SrcAddress The source memory Buffer address
  * @param  DstAddress The destination memory Buffer address
  * @param  DataLength The length of data to be transferred from source to destination
  * @retval HAL status
  */
void DMA_MultiBufferSetConfig(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength)
{  
  /* Configure DMA Stream data length */
  hdma->Instance->NDTR = DataLength;
  
  /* Peripheral to Memory */
  if((hdma->Init.Direction) == DMA_MEMORY_TO_PERIPH)
  {   
    /* Configure DMA Stream destination address */
    hdma->Instance->PAR = DstAddress;
    
    /* Configure DMA Stream source address */
    hdma->Instance->M0AR = SrcAddress;
  }
  /* Memory to Peripheral */
  else
  {
    /* Configure DMA Stream source address */
    hdma->Instance->PAR = SrcAddress;
    
    /* Configure DMA Stream destination address */
    hdma->Instance->M0AR = DstAddress;
  }
}



/******************************************************************
function: 	DMA_OS_dmaI2S_init
input	:	N/A.
output	:	N/A.
descr	:	inicializa y configura el DMA para I2S.
NOTA	:	
			- Solo para 24 y 32 bits.
			
*******************************************************************/
void DMA_OS_dmaI2S_init(void)
{	
	hi2s2.ErrorCode   = HAL_I2S_ERROR_NONE;
    hi2s2.State       = HAL_I2S_STATE_BUSY_TX;	
	hi2s2.TxXferSize  = (DMA_I2S_BUFFER_SIZE << 1U);
    hi2s2.TxXferCount = (DMA_I2S_BUFFER_SIZE << 1U);
	
	hi2s2.hdmatx->XferAbortCallback		 = NULL; //---------------------------------------------------------------------
	hi2s2.hdmatx->XferErrorCallback		 = DMA_I2S_ErrorCallback;
	hi2s2.hdmatx->XferHalfCpltCallback 	 = DMA_I2S_TxHalfCpltCallback;
	hi2s2.hdmatx->XferM1HalfCpltCallback = DMA_I2S_TxHalfCpltCallback;
	hi2s2.hdmatx->XferCpltCallback       = DMA_OS_MultibufferCallbackM0M1;
	hi2s2.hdmatx->XferM1CpltCallback     = DMA_OS_MultibufferCallbackM0M1;
	
	if (hi2s2.hdmatx->Init.Direction == DMA_MEMORY_TO_MEMORY)
		hi2s2.hdmatx->ErrorCode = HAL_DMA_ERROR_NOT_SUPPORTED;
	else
		hi2s2.hdmatx->ErrorCode = HAL_DMA_ERROR_NONE;
	
    hi2s2.hdmatx->State = HAL_DMA_STATE_BUSY;
    
	
	if (( (hi2s2.hdmatx->Instance->CR) & 0x40000 ) == 0)
	{
		hi2s2.hdmatx->Instance->CR |= (uint32_t)DMA_SxCR_DBM;
	}
	
	(hi2s2.hdmatx->Instance->M1AR) = (uint32_t)DMA_buffer_M1;
	
	DMA_MultiBufferSetConfig(hi2s2.hdmatx, (uint32_t)DMA_buffer_M0, (uint32_t)&hi2s2.Instance->DR, DMA_I2S_BUFFER_SIZE*1/*DMA_I2S_MULTIPLIER*/);
	
	/* Clear all flags */
    __HAL_DMA_CLEAR_FLAG (hi2s2.hdmatx, __HAL_DMA_GET_TC_FLAG_INDEX(hi2s2.hdmatx));
    __HAL_DMA_CLEAR_FLAG (hi2s2.hdmatx, __HAL_DMA_GET_HT_FLAG_INDEX(hi2s2.hdmatx));
    __HAL_DMA_CLEAR_FLAG (hi2s2.hdmatx, __HAL_DMA_GET_TE_FLAG_INDEX(hi2s2.hdmatx));
    __HAL_DMA_CLEAR_FLAG (hi2s2.hdmatx, __HAL_DMA_GET_DME_FLAG_INDEX(hi2s2.hdmatx));
    __HAL_DMA_CLEAR_FLAG (hi2s2.hdmatx, __HAL_DMA_GET_FE_FLAG_INDEX(hi2s2.hdmatx));
	
	
	/* Enable Common interrupts*/
	hi2s2.hdmatx->Instance->CR  |= DMA_IT_TC | DMA_IT_TE | DMA_IT_DME;
    hi2s2.hdmatx->Instance->FCR |= DMA_IT_FE;
    if((hi2s2.hdmatx->XferHalfCpltCallback != NULL) || (hi2s2.hdmatx->XferM1HalfCpltCallback != NULL))
		hi2s2.hdmatx->Instance->CR  |= DMA_IT_HT;
	
	/* Enable the peripheral */
    __HAL_DMA_ENABLE((hi2s2.hdmatx));
	
	/* Check if the I2S is already enabled */
    if((hi2s2.Instance->I2SCFGR &SPI_I2SCFGR_I2SE) != SPI_I2SCFGR_I2SE)
    {
      __HAL_I2S_ENABLE(&hi2s2); //Enable I2S peripheral
    }
	
	/* Check if the I2S Tx request is already enabled */
    if((hi2s2.Instance->CR2 & SPI_CR2_TXDMAEN) != SPI_CR2_TXDMAEN)
    {
      SET_BIT(hi2s2.Instance->CR2, SPI_CR2_TXDMAEN);					//Enable Tx DMA Request
    }
	//utils_debug("SxCR = 0x%x\n", hi2s2.hdmatx->Instance->CR);
	
	/* Este fragmento se coloca al final de esta funcion */
	//HAL_I2S_Transmit_DMA(&hi2s2, DMA_buffer_M0, 512);
	//HAL_DMAEx_MultiBufferStart_IT(hi2s2.hdmatx, (uint32_t)DMA_buffer_M0,(uint32_t)&hi2s2.Instance->DR, (uint32_t)DMA_buffer_M1,hi2s2.TxXferSize);
}

/******************************************************************
function: 	DMA_OS_MultibufferCallbackM0M1
input	:	hdma: estructura que contiene la config. del dma.
output	:	N/A.
descr	:	Indica que memoria se esta usando actualmente (M0 o M1).
*******************************************************************/
void DMA_OS_MultibufferCallbackM0M1(struct __DMA_HandleTypeDef *hdma)
{
	uint8_t  flag_writeMEM = 0;
	
	if( ((hdma->Instance->CR)&0x80000) == 0)
		flag_writeMEM = 1;
	
	//colocar aqui la flag global que se usar�
	st_flag.dma_M0M1 = flag_writeMEM;
}


/******************************************************************
function: 	TxHalfCpltCallback
input	:	hi2s pointer to a I2S_HandleTypeDef structure that 
			contains the configuration information for I2S module.
output	:	N/A.
descr	:	Tx Transfer Half completed callbacks
*******************************************************************/
void DMA_I2S_TxHalfCpltCallback(struct __DMA_HandleTypeDef *hdma)
{
	I2S_HandleTypeDef* hi2s = (I2S_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;
	UNUSED(hi2s);
}

/******************************************************************
function: 	DMA_I2S_TxCpltCallback
input	:	hdma pointer to a __DMA_HandleTypeDef structure that 
			contains the configuration information for I2S module.
output	:	N/A.
descr	:	Tx Transfer Half completed callbacks
*******************************************************************/
void DMA_I2S_TxCpltCallback(struct __DMA_HandleTypeDef *hdma)
{
	I2S_HandleTypeDef* hi2s = (I2S_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

	if((hdma->Instance->CR & DMA_SxCR_CIRC) == 0U)
	{
		/* Disable Tx DMA Request */
		CLEAR_BIT(hi2s->Instance->CR2,SPI_CR2_TXDMAEN);

		hi2s->TxXferCount = 0U;
		hi2s->State       = HAL_I2S_STATE_READY;
	}
	UNUSED(hi2s);
}



/******************************************************************
function: 	DMA_I2S_ErrorCallback
input	:	hdma pointer to a __DMA_HandleTypeDef structure that 
			contains the configuration information for I2S module.
output	:	N/A.
descr	:	registra en el apuntador si hubo algun error.
*******************************************************************/
void DMA_I2S_ErrorCallback(DMA_HandleTypeDef *hdma)
{
  I2S_HandleTypeDef* hi2s = (I2S_HandleTypeDef*)((DMA_HandleTypeDef*)hdma)->Parent;

  /* Disable Rx and Tx DMA Request */
  CLEAR_BIT(hi2s->Instance->CR2,(SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN));
  hi2s->TxXferCount = 0U;
  hi2s->RxXferCount = 0U;

  hi2s->State= HAL_I2S_STATE_READY;

  SET_BIT(hi2s->ErrorCode,HAL_I2S_ERROR_DMA);
  UNUSED(hi2s);
}


/******************************************************************
function: 	DMA_I2S_printInfoDMAconfig
input	:	N/A.
output	:	N/A.
descr	:	Imprime en pantalla la info del DMA I2S.
*******************************************************************/
void DMA_I2S_printInfoDMAconfig(void)
{
	utils_debug("       Que empiece el juego\n");
	lcd_printf("BUFFER_SIZE    = %d samples\n", DMA_I2S_BUFFER_SIZE);
	lcd_printf("FORMAT SAMPLES = %d bit/sample\n", SAMPLE_BIT);
	lcd_printf("SIZEOFBUFFER   = %d bytes\n", sizeof(DMA_buffer_M0));
}

/******************************************************************
function: 	DMA_I2S_clnBuff
input	:	N/A.
output	:	N/A.
descr	:	Imprime en pantalla la info del DMA I2S.
*******************************************************************/
void DMA_I2S_clnBuff(void)
{
	//limpia buffers M0 y M1
	memset(DMA_buffer_M0, 0, sizeof(DMA_buffer_M0));
    memset(DMA_buffer_M1, 0, sizeof(DMA_buffer_M1));
}


/******************************************************************
function: 	call_DMA2_OS_memIncEnable
input	:	hdma: stream usado en DMA2.
output	:	N/A.
descr	:	Reestablece la configuracion del stream DMA2 en modo 
			m2m.
*******************************************************************/
void call_DMA2_OS_memIncEnable(DMA_HandleTypeDef* hdma)
{
	hdma->Instance->CR |= DMA_PINC_ENABLE;
	hdma->Instance->CR &= (uint32_t)~(DMA_SxCR_TEIE | DMA_SxCR_DMEIE);//(0x2) | (0x4));
}


/******************************************************************
function: 	DMA2_OS_memcpy
input	:	SrcAddress: memoria fuente.
			DstAddress: memoria destino.
			len:		tmno de los datos a enviar.
output	:	N/A.
descr	:	Transfiere datos de memoria a memoria con interrupcion.
*******************************************************************/
void DMA2_OS_memcpy(void* DstAddress, void* SrcAddress, uint32_t len)
{
	while(hdma_memtomem_dma2_stream0.State > HAL_DMA_STATE_READY);  //Espera a que el DMA este disponible.
	hdma_memtomem_dma2_stream0.XferCpltCallback = call_DMA2_OS_memIncEnable;
	HAL_DMA_Start_IT(&hdma_memtomem_dma2_stream0, (uint32_t)SrcAddress, (uint32_t)DstAddress, len);
	hdma_memtomem_dma2_stream0.Instance->CR |= DMA_PDATAALIGN_BYTE;
	hdma_memtomem_dma2_stream0.Instance->CR |= DMA_MDATAALIGN_BYTE;
}

/******************************************************************
function: 	DMA2_OS_memset
input	:	SrcAddress: memoria fuente.
			DstAddress: memoria destino.
			szElmnt:	tmno de cada elemento.
			sz:			tmno de los datos a enviar.
output	:	N/A.
descr	:	Transfiere datos de memoria a memoria con interrupcion.
*******************************************************************/
void DMA2_OS_memset(void* dst, uint32_t val, uint8_t szElmnt, uint32_t sz)
{
	while(hdma_memtomem_dma2_stream0.State > HAL_DMA_STATE_READY);  //Espera a que el DMA este disponible.
	hdma_memtomem_dma2_stream0.Instance->CR &= (uint32_t)~(0x7800); //La transferencia sera de bytes por bytes.
	hdma_memtomem_dma2_stream0.Instance->CR &= (uint32_t)~(0x200); 	//Se limpia el bit PINC. Leer manual reference pag. 327.
	hdma_memtomem_dma2_stream0.XferCpltCallback = call_DMA2_OS_memIncEnable;
	HAL_DMA_Start_IT(&hdma_memtomem_dma2_stream0, (uint32_t)&val, (uint32_t)dst, szElmnt*sz);
}






