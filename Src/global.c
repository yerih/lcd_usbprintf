/***************************************************************************
Archivo: global.c
Autor  : Yerih Iturriago
Descrip: libreria de sistema operativo que contiene variables globales.
****************************************************************************/

#include "global.h"


/*-----------------------------------------------  LCD  ---------------------------------------------------------*/
/**********************************************************
					font_7x10
descripcion: fuente de tmno 7x10.

					font_11x18
descripcion: fuente de tmno 11x18.

					font_16x26
descripcion: fuente de tmno 16x26.

					st_fontDefault
descripcion: variable del tipo "st_fonDef_t" que contiene
			 el tipo de fuente LCD.
**********************************************************/
st_fontDef_t st_fontDefault;

/**********************************************************
					st_LCD
					
descripcion: variable del tipo "st_LCD_setup" que controla
			 el cursor de la LCD, fuente, colorForeGr, 
			 colorText, ancho y alto de la LCD.
**********************************************************/
st_LCD_setup st_LCD;


/*----------------------------------------------  Imagen  -------------------------------------------------------*/
/**********************************************************
					st_fileImgVar
					
descripcion: variable del tipo st_fileImg para tomar
			 los datos de una imagen. Tmno, dimensiones.
**********************************************************/
st_fileImg st_fileImgVar;


/*--------------------------------------------- MEM W25QXX ------------------------------------------------------*/
/*****************************************************
					mem_Available

descripcion: arreglo con informacion de espacio
			 disponible en memoria.
						
mem_Available[BLOCK]  : bloques disponibles.
mem_Available[SECTOR] : sector disponibles.
mem_Available[PAG]    : pag disponibles.
mem_Available[BYTES]  : bytes disponibles.
mem_Available[ADDRESS]: address disponible. valor por 
						default 0xffFFffFF;
******************************************************/
uint32_t mem_Available[5];
uint32_t **g_filePtr = NULL;



/*-------------------------------------------------  SD  ---------------------------------------------------------*/
uint8_t g_SDmntState = 0; //SD mount State. 1: montaje de SD. 0: desmontaje.



/*----------------------------------------------  Generales  ---------------------------------------------------------*/
uint8_t g_showDebug = 0; // muestra debugs.
uint8_t g_dmai2sStop = 0; //realiza stop si es > 0; 
st_flagGestor st_flag;

//------------------------------------------------------------- Directorio
DIR SDDir;

/*------------------------------------------------- Audio ------------------------------------------------------------*/

//------------------------------------------------------------- Generales
uint8_t g_demoAudio = 0;		//Para el seteo de configuraciones demo o default.

//------------------------------------------------------------- buffers DMA I2S


//------------------------------------------------------------- Archivo
//AUDIO_file_st_typedef audioFile;


//------------------------------------------------------------- Timers
uint8_t g_tcnt = 0;
