
/********************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  *******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "global.h"


#ifndef STM32F407xx_H_
#define STM32F407xx_H_
#include "stm32f4xx_hal.h"
#endif


#ifndef STM32F407xx_hal_dma_H_
#define STM32F407xx_hal_dma_H_
#include "stm32f4xx_hal_dma.h"
#endif

//#include "carnivoro_logo.h"

/* Global Variables ------------------------------------------------------------------*/

// LCD
extern st_fontDef_t st_fontDefault;
extern st_LCD_setup st_LCD;

// FlashMEM
extern uint32_t mem_Available[5];
extern st_fileImg st_fileImgVar;
extern uint32_t **g_filePtr;


//-------------------------------------------------------------------------- Audio 

//Archivos
extern AUDIO_file_st_typedef audioFile;

// AUDIO arrays
extern DMA_I2S_FORMATBIT sinewave[DMA_I2S_BUFFER_SIZE];


void SystemClock_Config(void);

int main(void)
{ 
	HAL_Init();
	SystemClock_Config();

	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USB_DEVICE_Init();
	MX_FSMC_Init();
	MX_RNG_Init();
	MX_SPI1_Init();
	MX_TIM3_Init();
	MX_TIM2_Init();
	MX_SDIO_SD_Init();
	MX_FATFS_Init();
	MX_I2S2_Init();
	MX_ADC1_Init();
	utils_LED_init();
	ILI9341_init();
	st_LCD_init();
	utils_LEDsequence(0, LED_D2);
	
	memset(&st_flag, 0, sizeof(st_flag));
	g_showDebug = 0;
	
	SD_mount();
	buttons_initAvailButtons();
	//buttons_OS_printAvailButtons(availButtons);
	g_demoAudio = 1;
	audioPlay_initPAD(0);
	DMA_OS_dmaI2S_init();
	//audioPlay_actvPads();
	utils_printClr("Init\n");
	
	g_demoAudio = 1;
	
	//audioPlay_actvPads();
	
	pad_snare.actv = 1;
	pad_snare.vol  = 0.5;
	while(1)
	{
		pad_snare.actv = 1;
		buttons_OS_readButt(call_keyIsPressed);
		LED_LOW_D2;
		audioPlay_loadMainBuff();
	}
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2S;
  PeriphClkInitStruct.PLLI2S.PLLI2SN = 192;
  PeriphClkInitStruct.PLLI2S.PLLI2SR = 2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
	utils_debug("Error. File %s, %d\n Press Reset button", file, line);
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
