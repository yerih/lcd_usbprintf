/***************************************************************************
Archivo: tdd.c
Autor  : Yerih Iturriago
Descrip: funciones para pruebas unitarias de funciones.
****************************************************************************/


#include "tdd.h"




/*************************************************************************
function: 	tdd_audioPlay_setDirAndFile
input	:	padLabel: etiqueta correspondiente al pad.
			demo:	  setea el tipo de configuracion. TRUE: setea
			los directorios en default. FALSE: directorios segun 
			configuracion de usuario. Usa g_demoAudio.
output	:	N/A
descr	:	observa los directorios segun la configuracion en demo y user.
**************************************************************************/
void tdd_audioPlay_setDirAndFile(uint8_t padLabel, uint8_t demo)
{
	st_PAD* pad = utils_padPointer(padLabel);
	uint8_t old_intensity = pad->intensidad;
	demo ? (g_demoAudio = 1):(g_demoAudio = 0);
	
	utils_hitPad(padLabel);
	pad->intensidad = 100;
	
	audioPlay_setDirAndFile();
	utils_printClr("     tdd test line %d\n", __LINE__);
	utils_printClr("pathDir: %s\n", pad->file.pathDir);
	utils_printClr("path: %s\n", pad->file.path);
	
	
	pad->intensidad = 1;
	audioPlay_setDirAndFile();
	utils_printClr("pathDir: %s\n", pad->file.pathDir);
	utils_printClr("path: %s\n", pad->file.path);
	
	pad->intensidad = old_intensity;
	if(g_demoAudio)
		g_demoAudio = 0;
}





/*************************************************************************
function: 	tdd_SD_testPathFile
input	:	path:	string con la ruta del archivo a testear.
output	:	N/A
descr	:	imprime en pantalla el resultado o msj de error del archivo de
			la ruta.
**************************************************************************/
void tdd_SD_testPathFile(char* path)
{
	uint8_t ret;
	ret = f_open(&SDFile, path, FA_READ);
	char* str = utils_fresult(ret);
	utils_printClr("open: %s\n", str);
}





