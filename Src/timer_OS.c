/******************************************************************************
  * File Name          : timer_OS.c
  * Autor			   : Yerih Iturriago.
  * Description        : operaciones con timers.
  ******************************************************************************/


#include "timer_OS.h"

/*******************************************************************
function: 	HAL_TIM_PeriodElapsedCallback
input:		htim: timer vinculado.
output:		N/A.
descr:		funcion de interrupcion o callback. 
			Desabilita el timer. 
			Apaga el LED D3.
*********************************************************************/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	UNUSED(htim);
	if(htim == &htim2)
	{
		LED_LOW_D3;  
		__HAL_TIM_DISABLE(htim);
		htim->State = HAL_TIM_STATE_READY;
	}
	else if(htim == &htim3)
	{
		__HAL_TIM_DISABLE(htim);
		htim->State = HAL_TIM_STATE_READY;
		call_timerADCmanage();
	}
	//utils_printClr("v%d ", htim->State);
}


/*******************************************************************
function: 	timer_OS_enableTimerMs
input:		htim: timer vinculado al pad o boton.
			ms  : milisegundos.
output:		N/A.
descr:		setea el timer indicado para el conteo de ms.
			El calculo se hace en con: 
			   clkSys = 84MHz.
			   
*********************************************************************/
void timer_OS_enableTimerMs(TIM_HandleTypeDef *htim, uint32_t ms)
{
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 42001; 				//Para 1ms, Prescaler -> 42001, Period -> 1. Para 1 seg, Prescaler -> 42001, Period -> 1999. Para 1 us, Prescaler -> 43, Period -> 1.
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim->Init.Period = ((ms*84*1000)/(42001 - 1)) - 1; //(uint32_t)
	HAL_TIM_Base_Init(htim);
	__HAL_TIM_ENABLE(htim);
	htim->State = HAL_TIM_STATE_BUSY;
}





/*******************************************************************
function: 	timer_OS_padPlayTime
input:		key : etiqueta del boton asociado.
output:		TRUE, si no es el momento de reproducir.
descr:		Verifica si el timer vinculado a la tecla termino el 
			conteo y lo vuelve a activar si es afirmativo.
*********************************************************************/
uint8_t timer_OS_verifyPlayTime(TIM_HandleTypeDef *htim) //TODO:(st_PAD* pad)
{
	uint8_t busy = 0;
	if(htim == &htim2)
	{
		if(htim->State == HAL_TIM_STATE_BUSY)
			busy = 1;
		else
		{
			timer_OS_enableTimerMs(htim, 100);
		}
	}
	return busy;
}
	


