/******************************************************************************
  * File Name          : utils.c
  * Autor			   : Yerih Iturriago.
  * Description        : Herramientas para codificar (debugs, etc).
  ******************************************************************************/


#include "utils.h"





/* Global Variables ------------------------------------------------------------------*/
extern st_fontDef_t font_7x10;
extern st_fontDef_t st_fontDefault;
extern st_LCD_setup st_LCD;
extern uint32_t mem_Available[5];
extern st_fileImg st_fileImgVar;
extern uint32_t **g_filePtr;

/*****************************************************************
function: 	utils_LED_init
input:		N/A
output:		N/A
descr:		Inicializa LEDs.
*******************************************************************/
void utils_LED_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	__HAL_RCC_GPIOA_CLK_ENABLE();
	//LED D2 (PA6 datasheet)
	GPIO_InitStruct.Pin = GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP/*GPIO_MODE_OUTPUT_OD*/;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	  
	//LED D3 (PA7 datasheet)
	/*Configure GPIO pin : PA7 */
	GPIO_InitStruct.Pin = GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	LED_LOW_D2;
	LED_LOW_D3;
}

/*****************************************************************
function: 	utils_USB_printf
input:		formato: string conteniendo formato (%s, %d, etc)
			(...)  : lista de variables.
output:		retorna USBD_OK = 0, USBD_BUSY = 1, USBD_FAIL = 2.
descr:		es como un printf via usb.
*******************************************************************/
uint8_t utils_USB_printf(const char *formato, ...)
{
	char buff[200];
	//memset(buff, 0, sizeof(buff));
	va_list ap;
	va_start(ap, formato);
	vsprintf(buff, formato, ap);
	va_end(ap);	
	return CDC_Transmit_FS((uint8_t*)buff, strlen(formato));
}


/*****************************************************************
function: 	utils_LEDsequence
input:		seq: numero de secuencia.
output:		N/A
descr:		Hace una seq de blinks con el LED
*******************************************************************/
void utils_LEDsequence(uint8_t seq, uint8_t led)
{
	//Inicio. Progresion de LED D3
	if(led == LED_D3)
	{
		LED_TOGGLE_D3;
		HAL_Delay(300);
		LED_TOGGLE_D3;
		HAL_Delay(300);
		LED_TOGGLE_D3;
		HAL_Delay(300);
		LED_TOGGLE_D3;
		HAL_Delay(300);
	}
	else
	{
		LED_TOGGLE_D2;
		HAL_Delay(300);
		LED_TOGGLE_D2;
		HAL_Delay(300);
		LED_TOGGLE_D2;
		HAL_Delay(300);
		LED_TOGGLE_D2;
		HAL_Delay(300);
	}
}





/**************************************************
function: 	utils_debug
input	:	fmt: formato de caracteres.
			...: lista de variables.
output	:	N/A
descr	:	hace un lcd_clr, y un lcd_printf para
			debuguear.
			No cambia el tipo de letra.
***************************************************/
void utils_debug(char *fmt, ...)
{
	char buff[200];
	
	LCD_clr();
	//st_LCD_setup st_LCD;
	st_LCD.x = 2;
	st_LCD.y = 2;
	st_LCD.background = WHITE;
	st_LCD.foreground = BLACK;
	st_LCD.fontBackground_flag = 0;
	st_LCD.font = font_7x10;
	
	memset(buff, 0, sizeof(200));
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buff, fmt, ap);
	va_end(ap);
	lcd_printf(buff);
	lcd_printf("\n--------------- presione tecla -------------\n"); //44 caracteres en total (sin contar el salto de linea).
	while(!buttons_OS_isPress());
	LCD_clr();
	st_LCD.x = 2;
	st_LCD.y = 2;
}


/*****************************************************************
function: 	utils_w25q16_printStruct
input	:	N/A
output	:	N/A
descr	:	imprime el valor de los campos de la estructura que
			controla la flash mem w25q16.
*****************************************************************/
void utils_w25q16_printStruct(void)
{
	st_LCD.x = 10;
	st_LCD.y = 10;
	LCD_clr();
	lcd_printf("Estructura w25qxx: expresado en bytes\n\n");
	lcd_printf("UniqID = %d\n", w25qxx.UniqID);
	lcd_printf("ID = %d\n", w25qxx.ID);
	lcd_printf("Capacity = %d\n", w25qxx.PageSize*8192);
	lcd_printf("CapacityInKiloByte = %d\n", w25qxx.CapacityInKiloByte);
	lcd_printf("PageSize = %d\n", w25qxx.PageSize);
	lcd_printf("SectorSize = %d\n", w25qxx.SectorSize);
	lcd_printf("BlockSize = %d\n", w25qxx.BlockSize);
	lcd_printf("PageCount = %d\n", w25qxx.PageCount);
	lcd_printf("SectorCount = %d\n", w25qxx.SectorCount);
	lcd_printf("BlockCount = %d\n", w25qxx.BlockCount);
	lcd_printf("Lock = %d\n", w25qxx.Lock);
	lcd_printf("StatusRegister1 = %d\n", w25qxx.StatusRegister1);
	lcd_printf("StatusRegister2 = %d\n", w25qxx.StatusRegister2);
	lcd_printf("StatusRegister3 = %d\n", w25qxx.StatusRegister3);
}

/*****************************************************************
function: 	utils_MEMdistribution
input	:	fsize : tmno de arreglo o archivo
			*array: arreglo que contiene el numero de
					estructura de bloques, sectores, pag y bytes
					a ser escritos en la flash mem.
output	:	N/A
descr	:	Calcula la distribucion de la memoria en estructuras 
			de memoria. Retorna la cantidad de cada estructuras
			en un arreglo. 
			array[0] = nro de bloques
			array[1] = nro de sectores
			array[2] = nro de paginas
			array[3] = nro de bytes
*****************************************************************/
void utils_MEMdistribution(uint32_t fsize, uint32_t *array)
{
    uint32_t blocks = 65536;
    int sect = 4096;
    int pag = 256;
	uint8_t nsb = 16; 								//numero de sectores por bloque N.S.B.
	uint8_t nps = 16;								//numero de paginas por sector N.P.S.
    uint32_t nb = fsize / blocks;					//numero de bloques
    uint32_t ns = fsize / sect;						//numero de sectores
    uint32_t np = fsize / pag;						//numero de paginas
    
    array[BLOCK] = nb;
    array[SECT] = (ns - nsb*nb);//*sect;
    array[PAG] = (np - nps*ns);//*pag;
    array[BYTES] = fsize - array[BLOCK]*blocks - array[SECT]*sect - array[PAG]*pag;
	//utils_debug("BLOCK = %d\nSECT = %d\nPAG = %d\nBYTES = %d\n", array[BLOCK], array[SECT], array[PAG], array[BYTES]);
}




/******************************************************************
function: 	st_memAvailable_init
input	:	N/A
output	:	N/A
descr	:	inicializa memoria
*******************************************************************/
void st_memAvailable_init(void)
{
	memset(mem_Available, 0, sizeof(mem_Available));
}


/******************************************************************
function: 	utils_w25q16_scanMEM
input	:	show = 1 (TRUE) muestra mensajes en pantalla
			show = 0 (FALSE) es obvio.
output	:	N/A
descr	:	escanea la memoria flash y detecta el byte disponible
			o vacio (0xff) y guarda esa direccion de memoria.
*******************************************************************/
void utils_w25q16_scanMEM(uint8_t show)
{
	uint8_t buffer[3] = {0, 0, 0};
	uint32_t address = 0;
	uint32_t disponible = 0;
	uint32_t size = 0;
	uint32_t fileCount = 0;
	
	
	HAL_GPIO_WritePin(_W25QXX_CS_GPIO,_W25QXX_CS_PIN,GPIO_PIN_RESET);
	W25qxx_Spi(0x0B);
	W25qxx_Spi((0 & 0xFF0000) >> 16);
	W25qxx_Spi((0 & 0xFF00) >> 8);
	W25qxx_Spi( 0 & 0xFF);
	W25qxx_Spi(0);
	
	if(show == 1)
	{
		LCD_clr();
		st_LCD.x = 2;
		st_LCD.y = 50;
		lcd_printf("escaneando memoria, espere un momento...\n");
	}
	
	for(address = 0; address < 2097152; address += 2)
	{
		buffer[0] = W25qxx_Spi(W25QXX_DUMMY_BYTE);
		buffer[1] = W25qxx_Spi(W25QXX_DUMMY_BYTE);
		
		if( (buffer[0] == '#')&&(buffer[1] == '|') ) 						  //Si se consigue un archivo
		{
			++fileCount;
			W25qxx_Spi(W25QXX_DUMMY_BYTE);
			W25qxx_Spi(W25QXX_DUMMY_BYTE);
			W25qxx_Spi(W25QXX_DUMMY_BYTE);
			size = ((W25qxx_Spi(W25QXX_DUMMY_BYTE)) << 16) | ((W25qxx_Spi(W25QXX_DUMMY_BYTE)) << 8) | (W25qxx_Spi(W25QXX_DUMMY_BYTE));
			HAL_GPIO_WritePin(_W25QXX_CS_GPIO,_W25QXX_CS_PIN,GPIO_PIN_SET);   //detiene comando de lectura.
			utils_w25q16_IDfileList(fileCount, address, size);
			
			address += size + 8; 											  //5 bytes: inicio de archivo y tmno. +3 bytes: de ID del archivo. 
			W25qxx_ReadBytes(buffer, address, 2);							  //+2 bytes de final de archivo.
			address += 1;
			
			HAL_GPIO_WritePin(_W25QXX_CS_GPIO,_W25QXX_CS_PIN,GPIO_PIN_RESET); //Se envia de nuevo el comando Fast Read a la memoria con la nueva direccion a leer
			W25qxx_Spi(0x0B);
			W25qxx_Spi((address & 0xFF0000) >> 16);
			W25qxx_Spi((address & 0xFF00) >> 8);
			W25qxx_Spi( address & 0xFF);
			//W25qxx_Spi(0);
		}
			
		if(buffer[0] == 0xff)
		{
			++disponible;
		}
		
		if(buffer[1] == 0xff)
		{
			++disponible;
		}
	}
	
	HAL_GPIO_WritePin(_W25QXX_CS_GPIO,_W25QXX_CS_PIN,GPIO_PIN_SET); 					//finaliza comando
	
	
	utils_MEMdistribution(disponible, mem_Available);
	size = mem_Available[BLOCK]*65536+mem_Available[SECT]*4096+mem_Available[PAG]*256+mem_Available[BYTES];
	mem_Available[ADDRESS] = 2097152 - disponible;
	
	if(show == 1)
	{			
		LCD_clr();
		st_LCD.x = 2;
		st_LCD.y = 50;
		lcd_printf("\nMemoria disponible:\n");
		lcd_printf("block = %d\nsect  = %d\npag   = %d\nbytes = %d\n", mem_Available[BLOCK], mem_Available[SECT], mem_Available[PAG], mem_Available[BYTES]);
		lcd_printf("Total = %d bytes\n", size);
		lcd_printf("Address aval. = %d\n", mem_Available[ADDRESS]);
		lcd_printf("MEM - total = %d\n", 2097152 - size);
	}
}


/******************************************************************
function: 	utils_w25q16_saveImgArray
input	:	*st_fileImgVar: direccion de la estructura que contiene
							la informacion de la imagen.
output	:	N/A
descr	:	guarda un dato en forma de archivo. Se le agrega un
			HEADER o encabezado.
			'#' y '|': 2 bytes, para comienzo de archivo.
			'ID'	 : 3 bytes, contiene la direcci�n en memoria.
			'SIZE'	 : 3 bytes, contiene el tamno del archivo.
			DATA	 : n bytes, la informacion del archivo.
			'|' y '#': 2 bytes, para final de archivo.
*******************************************************************/
void utils_w25q16_saveImgArray(st_fileImg *st_fileImgVar)
{	
	uint32_t dstrb[4] = {BLOCK, SECT, PAG, BYTES};
	uint32_t imgAddress;
	uint8_t  i = 0;
	uint32_t structAddress;
	
	//averiguar address de memoria usada
	utils_w25q16_scanMEM(0);
	imgAddress = mem_Available[ADDRESS];
	
	W25qxx_WriteByte(st_fileImgVar->headerFooter[0], mem_Available[ADDRESS]);
	W25qxx_WriteByte(st_fileImgVar->headerFooter[1], ++mem_Available[ADDRESS]);
	
	W25qxx_WriteByte((uint8_t)(imgAddress>>16), ++mem_Available[ADDRESS]);
	W25qxx_WriteByte((uint8_t)(imgAddress>>8), ++mem_Available[ADDRESS]);
	W25qxx_WriteByte((uint8_t)(imgAddress), ++mem_Available[ADDRESS]);
	
	//se envia tamano de archivo
	W25qxx_WriteByte((uint8_t)((st_fileImgVar->size)>>16), ++mem_Available[ADDRESS]);
	W25qxx_WriteByte((uint8_t)((st_fileImgVar->size)>>8), ++mem_Available[ADDRESS]);
	W25qxx_WriteByte((uint8_t)(st_fileImgVar->size), ++mem_Available[ADDRESS]);
	++mem_Available[ADDRESS];
	
	utils_MEMdistribution((st_fileImgVar->size), dstrb);
	//utils_debug("BLOCK = %d\nSECT = %d\nPAG = %d\nBYTES = %d\n", dstrb[BLOCK], dstrb[SECT], dstrb[PAG], dstrb[BYTES]);
	imgAddress = 0;
	
	structAddress = mem_Available[ADDRESS];
	for(i = 0; i < dstrb[BLOCK]; ++i)
	{
		//mandar bloques
		W25qxx_WriteBlock((uint8_t*)((st_fileImgVar->imgInArray)+imgAddress), structAddress + i, 0, 65536);
		mem_Available[ADDRESS] += w25qxx.BlockSize;
		imgAddress += w25qxx.BlockSize;
	}
	
	structAddress = mem_Available[ADDRESS];
	for(i = 0; i < dstrb[SECT]; ++i)
	{
		//mandar sect
		W25qxx_WriteSector((uint8_t*)((st_fileImgVar->imgInArray)+imgAddress), structAddress + i, 0, 4096);
		mem_Available[ADDRESS] += w25qxx.SectorSize;
		imgAddress += w25qxx.SectorSize;
	}
	
	structAddress = mem_Available[ADDRESS];
	for(i = 0; i < dstrb[PAG]; ++i)
	{
		//mandar pag
		W25qxx_WritePage((uint8_t*)((st_fileImgVar->imgInArray)+imgAddress), structAddress + i, 0, 256);
		mem_Available[ADDRESS] += w25qxx.PageSize;
		imgAddress += w25qxx.PageSize;
	}
	
	for(i = 0; i < dstrb[BYTES]; ++i)
	{
		//mandar bytes
		W25qxx_WriteByte((uint8_t)(*((st_fileImgVar->imgInArray)+imgAddress)), mem_Available[ADDRESS]);
		++mem_Available[ADDRESS];
		++imgAddress;
	}
	
	W25qxx_WriteByte(st_fileImgVar->headerFooter[1], mem_Available[ADDRESS]);
	W25qxx_WriteByte(st_fileImgVar->headerFooter[0], ++mem_Available[ADDRESS]);
	++mem_Available[ADDRESS];
}

/******************************************************************
function: 	utils_flashMEM_init
input	:	erase: >0 : borrar. 0 : no borra.
output	:	TRUE : hubo un error.
			FALSE: proceso exitoso.
descr	:	
*******************************************************************/
uint8_t utils_flashMEM_init(uint8_t erase)
{
	if(!W25qxx_Init())
	{
		lcd_printf("Falla al iniciar flash MEM\n");
		return 1;
	}
	else
	{
		if(g_showDebug)
			utils_w25q16_printStruct();
		if(erase)
			W25qxx_EraseChip();
		mem_Available[ADDRESS] = 0xFFffFFff;
	}
	return 0;
}

/******************************************************************
function: 	utils_flashMEM_init
input	:	N/A
output	:	TRUE : hubo un error.
			FALSE: proceso exitoso.
descr	:	
*******************************************************************/
void utils_dumpMemory(uint8_t *buffer, uint32_t size)
{
	LCD_clr();
	st_LCD.x = 2;
	st_LCD.y = 2;
	for(int i = 0; i < size; ++i)
		lcd_printf("0x%x ", buffer[i]);
}

/******************************************************************
function: 	utils_writeMEMnTimes
input	:	N/A
output	:	N/A
descr	:	escribe en la memoria 0x56, n veces.
*******************************************************************/
void utils_writeMEMnTimes(uint32_t n)
{
	for(uint32_t i = 0; i < n; ++i)
	{
		W25qxx_WriteByte(0x56, i);
	}
}


/******************************************************************
function: 	utils_w25q16_slowScanMEM
input	:	show = 1 (TRUE) muestra mensajes en pantalla
			show = 0 (FALSE) es obvio.
output	:	N/A
descr	:	escanea la memoria flash y detecta el byte disponible
			o vacio (0xff) y guarda esa direccion de memoria.
*******************************************************************/
void utils_w25q16_slowScanMEM(uint8_t show)
{
	uint8_t buffer[3] = {0, 0, 0};
	uint32_t address = 0;
	uint32_t disponible = 0;
	uint32_t total = 0;
	
	
	HAL_GPIO_WritePin(_W25QXX_CS_GPIO,_W25QXX_CS_PIN,GPIO_PIN_RESET);
	W25qxx_Spi(0x0B);
	W25qxx_Spi((0 & 0xFF0000) >> 16);
	W25qxx_Spi((0 & 0xFF00) >> 8);
	W25qxx_Spi( 0 & 0xFF);
	W25qxx_Spi(0);
	
	if(show == 1)
	{
		LCD_clr();
		st_LCD.x = 2;
		st_LCD.y = 50;
		lcd_printf("slow scan MEM, espere un momento...\n");
	}
	
	for(address = 0; address < 2097152; ++address)
	{
		buffer[0] = W25qxx_Spi(W25QXX_DUMMY_BYTE);	
		if(buffer[0] == 0xff)
		{
			++disponible;
			if(mem_Available[ADDRESS] == MEM_VACIA)
			{
				mem_Available[ADDRESS] = address;
			}
		}
		
	}

	HAL_GPIO_WritePin(_W25QXX_CS_GPIO,_W25QXX_CS_PIN,GPIO_PIN_SET); //finaliza comando	
	utils_MEMdistribution(disponible, mem_Available);
	
	
	utils_debug("disp = %d\nmem_Avail = %d", disponible, mem_Available[ADDRESS]);
	HAL_Delay(10000);
	
	total = mem_Available[BLOCK]*65536+mem_Available[SECT]*4096+mem_Available[PAG]*256+mem_Available[BYTES];
	
	//debug
	mem_Available[ADDRESS] = 2097152 - disponible;
	
	if(show == 1)
	{			
		LCD_clr();
		st_LCD.x = 2;
		st_LCD.y = 50;
		lcd_printf("\nMemoria disponible:\n");
		lcd_printf("block = %d\nsect  = %d\npag   = %d\nbytes = %d\n", mem_Available[BLOCK], mem_Available[SECT], mem_Available[PAG], mem_Available[BYTES]);
		lcd_printf("Total = %d bytes\n", total);
		lcd_printf("Address aval. = %d\n", ++mem_Available[ADDRESS]);
		lcd_printf("MEM - total = %d\n", 2097152 - total);
	}
}


/******************************************************************
function: 	utils_w25q16_IDfileList
input	:	fileCount: contador de archivos. Debe ser > 0.
			id       : ID o direccion del archivo.
			size     : tmno del archivo.

output	:	N/A
descr	:	guarda en un doble apuntador (g_filePtr) un lista de 
			direcciones o ID y tmno de los archivos contenidos en 
			la flashsMEM. La primera fila del apuntador contiene
			g_filePtr[0][0] = contador de archivos encontrados.	
			g_filePtr[0][1] = peso en memoria.
*******************************************************************/
void utils_w25q16_IDfileList(uint32_t fileCount, uint32_t id, uint32_t size)
{
	if(fileCount > 0)
	{
		if(g_filePtr == NULL)
		{		
			g_filePtr = (uint32_t**)malloc(2*sizeof(uint32_t*));
			g_filePtr[0] = (uint32_t*)malloc(2*sizeof(uint32_t));
			g_filePtr[fileCount] = (uint32_t*)malloc(2*sizeof(uint32_t));
			g_filePtr[0][1] = 0;
		}
		else
		{
			g_filePtr = (uint32_t**)realloc(g_filePtr, 2*sizeof(uint32_t));
			g_filePtr[fileCount] = (uint32_t*)malloc(2*sizeof(uint32_t));
		}
		
		g_filePtr[0][0] = fileCount;
		g_filePtr[0][1] += size;
		g_filePtr[fileCount][0] = id;
		g_filePtr[fileCount][1] = size;
	}
}



/******************************************************************
function: 	utils_w25q16_IDfileList
input	:	fileCount: contador de archivos. Debe ser > 0.
			id       : ID o direccion del archivo.
			size     : tmno del archivo.

output	:	N/A
descr	:	guarda en un doble apuntador (g_filePtr) un lista de 
			direcciones o ID y tmno de los archivos contenidos en 
			la flashsMEM. La primera fila del apuntador contiene
			g_filePtr[0][0] = contador de archivos encontrados.	
			g_filePtr[0][1] = peso en memoria.
*******************************************************************/
void utils_w25q16_IDfilePrint(void)
{
	LCD_clr();
	st_LCD.x = 2;
	st_LCD.y = 50;
	
	lcd_printf("Lista de archivos: \ncantidad %d, \npeso total %d\n\n", g_filePtr[0][0], g_filePtr[0][1]);
	for(int i = 1; i <= g_filePtr[0][0]; ++i)
	{
		lcd_printf("ID: %d, SIZE: %d\n", g_filePtr[i][0], g_filePtr[i][1]);
	}
	
}


/******************************************************************
function: 	utils_w25q16_saveFileInBytes
input	:	fileCount: contador de archivos. Debe ser > 0.
			id       : ID o direccion del archivo.
			size     : tmno del archivo.

output	:	N/A
descr	:	escribe en la flashMEM el archivo de memoria sin 
			utilizar estructuras de memoria; solo bytes.
*******************************************************************/
void utils_w25q16_saveFileInBytes(st_fileImg *st_fileImgVar)
{
	uint32_t imgAddress;
	
	//averiguar address de memoria usada
	utils_w25q16_scanMEM(0);
	imgAddress = mem_Available[ADDRESS];
	
	//Escribe inicio de archivo '#' y '|'
	W25qxx_WriteByte(st_fileImgVar->headerFooter[0], mem_Available[ADDRESS]);
	W25qxx_WriteByte(st_fileImgVar->headerFooter[1], ++mem_Available[ADDRESS]);
	
	//Se guarda el address o ID del archivo
	W25qxx_WriteByte((uint8_t)(imgAddress>>16), ++mem_Available[ADDRESS]);
	W25qxx_WriteByte((uint8_t)(imgAddress>>8), ++mem_Available[ADDRESS]);
	W25qxx_WriteByte((uint8_t)(imgAddress), ++mem_Available[ADDRESS]);
	
	//se envia tamano de archivo
	W25qxx_WriteByte((uint8_t)((st_fileImgVar->size)>>16), ++mem_Available[ADDRESS]);
	W25qxx_WriteByte((uint8_t)((st_fileImgVar->size)>>8), ++mem_Available[ADDRESS]);
	W25qxx_WriteByte((uint8_t)(st_fileImgVar->size), ++mem_Available[ADDRESS]);
	++mem_Available[ADDRESS];
	
	//Se guarda data del archivo
	for(int32_t i = 0; i < st_fileImgVar->size; ++i, ++mem_Available[ADDRESS])
	{
		W25qxx_WriteByte((uint8_t)(st_fileImgVar)->imgInArray[i], mem_Available[ADDRESS]);
	}
	
	W25qxx_WriteByte(st_fileImgVar->headerFooter[1], mem_Available[ADDRESS]);
	W25qxx_WriteByte(st_fileImgVar->headerFooter[0], ++mem_Available[ADDRESS]);
	++mem_Available[ADDRESS];
}

/******************************************************************
function: 	memset32
input	:	dest : direccion de destino.
			value: valor a ser seteado.
			size : tmno del paquete (bytes).

output	:	N/A
descr	:	memset de 32 bit.
*******************************************************************/
void memset32(void * dest, uint32_t value, uintptr_t size)
{
	/*
    uintptr_t i;
    for( i = 0; i < (size & (~7)); i+=8 )
    {
        memcpy( ((char*)dest) + i, &value, 8);
    }  
    
    for(i = 0; i < size; i++ )
    {
        ((char*)dest)[i] = ((char*)&value)[i&7];
    }
	*/
}

/******************************************************************
function: 	utils_changeByteOrder32
input	:	buffer: buffer a modificar.
			size  : tmno del paquete medido en unidad de 32bit 
					o 4bytes.
output	:	N/A
descr	:	cambia el orden de los bytes. Ejemplo: a = 0xaabbccdd
			al invocar funcion a = ddccbbaa. Valido solo para 32bit
*******************************************************************/
void utils_changeByteOrder32(uint32_t *buffer, uint32_t size)
{
    uint32_t aux;
    uintptr_t i;
    for(i = 0; i < size; ++i)
    {
        aux = buffer[i];
        buffer[i] = (aux << 24) | ((aux & 0xff00) << 8) | ((aux & 0xff0000) >> 8) | ((aux & 0xff000000) >> 24);
        //a = (aux << 24) | ((aux & 0xff00) << 8) | ((aux & 0xff0000) >> 8) | ((aux & 0xff000000) >> 24);
        //lcd_printf("a   = 0x%x\n", a[i]);
    }
    //lcd_printf("aux = 0x%x\n", aux);
}



/******************************************************************
function: 	utils_printClr
input	:	fmt: string de texto.
			va_list: lista de variables.
output	:	N/A
descr	:	imprime texto y cuando llega al final de la pantalla
			la limpia y continua escribiendo.
*******************************************************************/
void utils_printClr(char* fmt, ...)
{
	char buff[200];
	memset(buff, 0, sizeof(200));
	
	if(st_LCD.y >= 215)
	{
		LCD_clr();
		st_LCD.x = 2;
		st_LCD.y = 2;
	}
	//st_LCD_setup st_LCD;
	st_LCD.background = WHITE;
	st_LCD.foreground = BLACK;
	st_LCD.fontBackground_flag = 0;
	st_LCD.font = font_7x10;
	
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buff, fmt, ap);
	va_end(ap);
	lcd_printf(buff);
}



/******************************************************************
function: 	utils_printArray
input	:	a:			 arreglo a imprimir.
			sizeElement: tmno de un elemento.
			len:		 tmno total del arreglo.
output	:	N/A
descr	:	imprime el arreglo en enteros.
*******************************************************************/
void utils_printArray(void* a, uint32_t sizeElement, uint32_t len)
{
	uint32_t i = 0;
	lcd_printf("array = {");
	while(i < len - 1)
	{
		lcd_printf(" %d,", *((int16_t*)a+i++) );
		//i++;
	}
	lcd_printf(" %d}\n", *((int16_t*)a+i) );
	/*
	uint32_t i = 0;
	lcd_printf("array = {");
	while(i < len/sizeElement - 1)
	{
		lcd_printf(" %d,", *((uint32_t*)a+i++) );
		//i++;
	}
	lcd_printf(" %d}\n", *((uint32_t*)a+i) );
	*/
}





















/********************************************************************
function: 	utils_printArray
input	:	GPIO_Pin: pin solicitado.
output	:	string con el nombre del pad
descr	:	retorna el string con el nombre del pad asociado al pin.
*********************************************************************/
char* utils_getIRQlabel(uint16_t GPIO_Pin)
{
	char* str;
	switch(GPIO_Pin)
	{
		case IRQ_SNARE_Pin:		str = "IRQ_SNARE"; 	  break; //SNARE	PA9
		case IRQ_KICK_Pin:		str = "IRQ_KICK"; 	  break; //KICK		PD12
		case IRQ_HITHAT_Pin:	str = "IRQ_HITHAT";   break; //HITHAT	PB2
		case IRQ_TOM1_Pin:		str = "IRQ_TOM1"; 	  break; //TOM1		PB10
		case IRQ_TOM2_Pin:		str = "IRQ_TOM2"; 	  break; //TOM2		PE0
		case IRQ_TOM3_Pin:		str = "IRQ_TOM3"; 	  break; //TOM3		PE1
		case IRQ_CYMBAL_R_Pin:	str = "IRQ_CYMBAL_R"; break; //CYMBAL_R	PE5
		case IRQ_CYMBAL_L_Pin:	str = "IRQ_CYMBAL_L"; break; //CYMBAL_L	PE6
		
		default:			str = "IRQ_INVALIDA"; break;
	}
	return str;	
}



/********************************************************************
function: 	utils_printArray
input	:	GPIO_Pin: pin solicitado.
output	:	string con el nombre del pad
descr	:	retorna el puerto del pad asociado al pin.
*********************************************************************/
GPIO_TypeDef* utils_getIRQPort(uint16_t GPIO_Pin)
{
	GPIO_TypeDef* port;
	
	switch(GPIO_Pin)
	{
		case IRQ_SNARE_Pin:		port = IRQ_SNARE_GPIO_Port;		break; //SNARE	PA9
		case IRQ_KICK_Pin:		port = IRQ_KICK_GPIO_Port; 		break; //KICK		PD12
		case IRQ_HITHAT_Pin:	port = IRQ_HITHAT_GPIO_Port;   	break; //HITHAT	PB2
		case IRQ_TOM1_Pin:		port = IRQ_TOM1_GPIO_Port;		break; //TOM1		PB10
		case IRQ_TOM2_Pin:		port = IRQ_TOM2_GPIO_Port;		break; //TOM2		PE0
		case IRQ_TOM3_Pin:		port = IRQ_TOM3_GPIO_Port;		break; //TOM3		PE1
		case IRQ_CYMBAL_R_Pin:	port = IRQ_CYMBAL_R_GPIO_Port; 	break; //CYMBAL_R	PE5
		case IRQ_CYMBAL_L_Pin:	port = IRQ_CYMBAL_L_GPIO_Port;	break; //CYMBAL_L	PE6
		
		default:				port = NULL; break;
	}
	return port;	
}


/******************************************************************
function: 	utils_padPointer
input	:	padLabel: etiqueta correspondiente al pad.
output	:	apuntador al pad correspodiente a la etiqueta.
descr	:	apunta al pad correspondiente a padLabel.
*******************************************************************/
st_PAD* utils_padPointer(uint8_t padLabel)
{
	st_PAD* p = NULL;
	switch(padLabel)
	{
		case SNARE:		p = &pad_snare;   break;
		case KICK:		p = &pad_kick;    break;
		case HITHAT:	p = &pad_hithat;  break;
		case TOM1:		p = &pad_tom1;    break;
		case TOM2:		p = &pad_tom2;    break;
		case TOM3:		p = &pad_tom3; 	  break;
		case CYMBAL_L:  p = &pad_cymbalL; break;
		case CYMBAL_R:  p = &pad_cymbalR; break;
		default: 		break;
	}
	return p;
}




/******************************************************************
function: 	utils_hitPad
input	:	show: TRUE: muestra msj en pantalla
output	:	TRUE: si hubo algun error.
descr	:	realiza montaje de SD. Se debe llamar a SD_unmount().
			despues de realizar todas operaciones con la SD.
*******************************************************************/
void utils_hitPad(uint8_t padLabel)
{
	switch(padLabel)
	{
		case SNARE:		pad_snare.hit   = 1; break;
		case KICK:		pad_kick.hit    = 1; break;
		case HITHAT:	pad_hithat.hit  = 1; break;
		case TOM1:		pad_tom1.hit    = 1; break;
		case TOM2:		pad_tom2.hit	= 1; break;
		case TOM3:		pad_tom3.hit	= 1; break;
		case CYMBAL_L:  pad_cymbalL.hit = 1; break;
		case CYMBAL_R:  pad_cymbalR.hit = 1; break;
		default: 		break;
	}
}


/******************************************************************
function: 	utils_hitPadReset
input	:	padLabel: etiqueta correspondiente al pad.
output	:	N/A
descr	:	resetea el campo hit de la estructura st_PAD.
*******************************************************************/
void utils_hitPadReset(uint8_t padLabel)
{
	switch(padLabel)
	{
		case SNARE:		pad_snare.hit   = 0; break;
		case KICK:		pad_kick.hit    = 0; break;
		case HITHAT:	pad_hithat.hit  = 0; break;
		case TOM1:		pad_tom1.hit    = 0; break;
		case TOM2:		pad_tom2.hit	= 0; break;
		case TOM3:		pad_tom3.hit	= 0; break;
		case CYMBAL_L:  pad_cymbalL.hit = 0; break;
		case CYMBAL_R:  pad_cymbalR.hit = 0; break;
		default: 		break;
	}
}





/******************************************************************
function: 	utils_fresult
input	:	frLabel: referido al enum FRESULT.
output	:	string de datos
descr	:	retorna el string del error segun frLabel.
*******************************************************************/
char* utils_fresult(uint8_t frLabel)
{
	char str[50];
	memset(str, 0, sizeof(50));
	char* str1;
	switch(frLabel)
	{
		case FR_OK:					 sprintf(str, "%s", "Success");														break;
		case FR_DISK_ERR:			 sprintf(str, "%s", "Error in the low level disk I/O layer");							break;
		case FR_INT_ERR:			 sprintf(str, "%s", "Assertion failed");												break;
		case FR_NOT_READY:			 sprintf(str, "%s", "The physical drive cannot work");									break;
		case FR_NO_FILE:			 sprintf(str, "%s", "Could not find the file");											break;
		case FR_NO_PATH:			 sprintf(str, "%s", "Could not find the path");											break;
		case FR_INVALID_NAME:		 sprintf(str, "%s", "The path name format is invalid");									break;
		case FR_DENIED:				 sprintf(str, "%s", "Access denied or directory full");									break;
		case FR_EXIST:				 sprintf(str, "%s", "Access denied due to prohibited access");							break;
		case FR_INVALID_OBJECT:		 sprintf(str, "%s", "The file/directory object is invalid");							break;
		case FR_WRITE_PROTECTED:	 sprintf(str, "%s", "The physical drive is write protected");							break;
		case FR_INVALID_DRIVE:		 sprintf(str, "%s", "The logical drive number is invalid");								break;
		case FR_NOT_ENABLED:		 sprintf(str, "%s", "The volume has no work area");										break;
		case FR_NO_FILESYSTEM:		 sprintf(str, "%s", "There is no valid FAT volume");									break;
		case FR_MKFS_ABORTED:		 sprintf(str, "%s", "The f_mkfs() aborted due to any problem");							break;
		case FR_TIMEOUT:			 sprintf(str, "%s", "Couldn't access the volume by Timeout");							break;
		case FR_LOCKED:				 sprintf(str, "%s", "Rejected according to the sharing policy");						break;
		case FR_NOT_ENOUGH_CORE:	 sprintf(str, "%s", "LFN working buffer could not be allocated");						break;
		case FR_TOO_MANY_OPEN_FILES: sprintf(str, "%s", "Number of open files > _FS_LOCK");									break;
		case FR_INVALID_PARAMETER:	 sprintf(str, "%s", "Given parameter is invalid");										break;
		default:					 sprintf(str, "%s", "FRsult doesn't exist");											break;
	}
	str1 = str;
	return str1;
}



